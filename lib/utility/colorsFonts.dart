import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppColors {
  static const primaryColor = Color(0xFFa62626);
}

class AppFonts {
  double? fontSize = 17.sp;
  static double font17 = 17;
  static double font20 = 20;
  static double font15 = 15;
  static double font13 = 13;
}

class buttons {
  static bool? isButtonClick1;
  static bool? isButtonClick2;
  static String? dropdownValue;

  void getData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    dropdownValue = sp.getString('volume') ?? 'Feet / Inch';
    isButtonClick1 = dropdownValue == 'Meter / CM' ? false : true;
    isButtonClick2 = dropdownValue == 'Meter / CM' ? true : false;
  }

  static String? currencyType;
  static String? dropdownValue2;

  void getCurrencyType() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    dropdownValue2 = sp.getString('currency') ?? 'INR (\u{20B9})';
    currencyType = dropdownValue2 == 'INR (\u{20B9})' ? '\u{20B9}' : '\$';
  }
}

class AppBarColor {
  Widget getAppBarColor() {
    return Container(
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          gradient: LinearGradient(
            colors: [Color(0xff5d5d5d), Color(0xff2e2e2e)],
            stops: [0, 0.6],
            begin: Alignment.bottomRight,
            end: Alignment.topLeft,
          )),
    );
  }
}

class ChartData {
  ChartData(this.x, this.y, this.color);

  final String x;
  final double y;
  final Color color;
}