import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/about_us_page.dart';
import 'package:civil_quantity_estimator/pages/01_construction_cost_page.dart';
import 'package:civil_quantity_estimator/pages/02_carpet_area_page.dart';
import 'package:civil_quantity_estimator/pages/03_cement_concrete_page.dart';
import 'package:civil_quantity_estimator/pages/04_plaster_cal_page.dart';
import 'package:civil_quantity_estimator/pages/05_brick_masonary_page.dart';
import 'package:civil_quantity_estimator/pages/06_concrete_block_page.dart';
import 'package:civil_quantity_estimator/pages/07_boundry_wall_page.dart';
import 'package:civil_quantity_estimator/pages/08_flooring_cal_page.dart';
import 'package:civil_quantity_estimator/pages/09_tank_volume_page.dart';
import 'package:civil_quantity_estimator/pages/10_air_conditioner_page.dart';
import 'package:civil_quantity_estimator/pages/11_solar_rooftop_page.dart';
import 'package:civil_quantity_estimator/pages/12_solar_waterHeater_page.dart';
import 'package:civil_quantity_estimator/pages/13_paint_work_page.dart';
import 'package:civil_quantity_estimator/pages/14_excavation_calculator_page.dart';
import 'package:civil_quantity_estimator/pages/15_wood_farming_page.dart';
import 'package:civil_quantity_estimator/pages/16_plyWood_sheets_page.dart';
import 'package:civil_quantity_estimator/pages/17_anti_termite_page.dart';
import 'package:civil_quantity_estimator/pages/18_round_column_page.dart';
import 'package:civil_quantity_estimator/pages/19_stair_case_page.dart';
import 'package:civil_quantity_estimator/pages/20_top_soil_page.dart';
import 'package:civil_quantity_estimator/pages/21_steel_weight_page.dart';
import 'package:civil_quantity_estimator/pages/22_concrete_pipe_page.dart';
import 'package:civil_quantity_estimator/setting_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class Home_page extends StatefulWidget {
  const Home_page({Key? key}) : super(key: key);

  @override
  State<Home_page> createState() => _Home_pageState();
}

class _Home_pageState extends State<Home_page> {
  @override
  void initState() {
    super.initState();
    buttons().getData();
    MyDatabase().copyPasteAssetFileToRoot();
  }

  @override
  Widget build(BuildContext context) {
    DateTime timeBackPressed = DateTime.now();
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height * 0.1;
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: AppBarColor().getAppBarColor(),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15))),
          // backgroundColor: AppColors.primaryColor,
          title: const Text(
            "Civil Quantity Estimator",
          ),
          actions: [
            PopupMenuButton(
              itemBuilder: (context) {
                return [
                  const PopupMenuItem<int>(
                    value: 0,
                    child: Text("Share"),
                  ),
                  const PopupMenuItem<int>(
                    value: 1,
                    child: Text("Settings"),
                  ),
                  const PopupMenuItem<int>(
                    value: 2,
                    child: Text("About Us"),
                  ),
                ];
              },
              onSelected: (value) {
                if (value == 0) {
                  // void onShare(BuildContext context) async {
                  final box = context.findRenderObject() as RenderBox?;
                  String content =
                      "Download App of Civil Quantity Estimator from Play Store";
                  Share.share(
                    "$content: https://play.google.com/store/apps/details?id=com.aswdc_civilquantityestimator",
                    sharePositionOrigin:
                        box!.localToGlobal(Offset.zero) & box.size,
                  );
                  // }
                } else if (value == 1) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return const SettingsPage();
                      },
                    ),
                  );
                } else if (value == 2) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return const AboutUsPage();
                      },
                    ),
                  );
                }
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
                child: GridView(
                  physics: const NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 13,
                      mainAxisSpacing: 13),
                  children: [
                    getCalculationCard(
                      context,
                      "Construction\nCost",
                      const BuildUpCalculationPage(),
                      'assets/icons/ConstructionCost.png',
                    ),
                    getCalculationCard(
                      context,
                      "Carpet\nArea",
                      const CarpetAreaPage(),
                      'assets/icons/carpetArea.png',
                    ),
                    getCalculationCard(
                      context,
                      "Cement\nConcrete",
                      const CementConcretePage(),
                      'assets/icons/concrete-mixer.png',
                    ),
                    getCalculationCard(
                      context,
                      "Plaster\nCalculator",
                      const PlasterCalculationPage(),
                      'assets/icons/plasterCalculator.png',
                    ),
                    getCalculationCard(
                      context,
                      "Brick\nMasonary",
                      const BrickMasonaryPage(),
                      'assets/icons/BrickMasonary.png',
                    ),
                    getCalculationCard(
                      context,
                      "Concrete\nBlock",
                      const ConcreteBlockPage(),
                      'assets/icons/concreteBlock.png',
                    ),
                    getCalculationCard(
                      context,
                      "Boundry\nWall",
                      const BoundryWallPage(),
                      'assets/icons/boundryWall.png',
                    ),
                    getCalculationCard(
                      context,
                      "Flooring\nCalculator",
                      const FlooringCalculationPage(),
                      'assets/icons/flooringCalculator.png',
                    ),
                    getCalculationCard(
                      context,
                      "Tank\nVolume",
                      const TankVolumePage(),
                      'assets/icons/tankVolume.png',
                    ),
                    getCalculationCard(
                      context,
                      "Air\nConditioner",
                      const AirConditionerPage(),
                      'assets/icons/airConditioner.png',
                    ),
                    getCalculationCard(
                      context,
                      "Solar\nRooftop",
                      const SolarRoofTopPage(),
                      'assets/icons/solarRoofTop.png',
                    ),
                    getCalculationCard(
                      context,
                      "Solar\nWater Heater",
                      const SolarWaterHeaterPage(),
                      'assets/icons/solarWaterHeater.png',
                    ),
                    getCalculationCard(
                      context,
                      "Paint\nWork",
                      const PaintWorkPage(),
                      'assets/icons/paintWork.png',
                    ),
                    getCalculationCard(
                      context,
                      "Excavation\nCalculator",
                      const ExcavationCalculationPage(),
                      'assets/icons/excavationCalculator.png',
                    ),
                    getCalculationCard(
                      context,
                      "Wood\nFarming",
                      const WoodFarmingPage(),
                      'assets/icons/woodFarming.png',
                    ),
                    getCalculationCard(
                      context,
                      "PlyWood\nSheets",
                      const PlyWoodSheets(),
                      'assets/icons/plyWoodSheets.png',
                    ),
                    getCalculationCard(
                      context,
                      "Anti\nTermite",
                      const AntiTermitePage(),
                      'assets/icons/antiTermite.png',
                    ),
                    getCalculationCard(
                      context,
                      "Round\nColumn",
                      const RoundColumnPage(),
                      'assets/icons/roundColumn.png',
                    ),
                    getCalculationCard(
                      context,
                      "Stair\nCase",
                      const StairCasePage(),
                      'assets/icons/stairCase.png',
                    ),
                    getCalculationCard(
                      context,
                      "Top\nSoil",
                      const TopSoilPage(),
                      'assets/icons/topSoil.png',
                    ),
                    getCalculationCard(
                      context,
                      "Steel\nWeight",
                      const SteelWeightPage(),
                      'assets/icons/steelWeight3.png',
                    ),
                    getCalculationCard(
                      context,
                      "Concrete\nPipe",
                      const ConcretePipePage(),
                      'assets/icons/concretePipe.png',
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () => launchUrl(
                          Uri.parse(
                              'https://www.civil-engineering-calculators.com/'),
                          mode: LaunchMode.externalApplication,
                        ),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(5),
                                topLeft: Radius.circular(5),
                              ),
                              gradient: LinearGradient(
                                colors: [Color(0xff5d5d5d), Color(0xff2e2e2e)],
                                stops: [0, 0.6],
                                begin: Alignment.bottomRight,
                                end: Alignment.topLeft,
                              )),
                          child: const Padding(
                            padding: EdgeInsets.all(6.0),
                            child: Text(
                              'www.civil-engineering-calculators.com',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      onWillPop: () async {
        final differeance = DateTime.now().difference(timeBackPressed);
        timeBackPressed = DateTime.now();
        if (differeance >= const Duration(seconds: 2)) {
          const String msg = 'Tap Back Again to Exit';
          Fluttertoast.showToast(
            msg: msg,
          );
          return false;
        } else {
          Fluttertoast.cancel();
          SystemNavigator.pop();
          return true;
        }
      },
    );
  }

  Widget getCalculationCard(context, fieldName, navigate, icon) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: ((context) {
              return navigate;
            }),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            gradient: const LinearGradient(
              colors: [Color(0xffe8e8e8), Color(0xffe5e9f0)],
              stops: [0, 1],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 0),
          child: Column(
            children: [
              Expanded(
                child: Image.asset(icon, fit: BoxFit.fill),
              ),
              Expanded(
                child: Text(
                  fieldName,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.grey.shade800,
                      fontSize: 12),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}