import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDatabase {

  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'calculators.db');
    return await openDatabase(
      databasePath,
      version: 2,
    );
  }

  Future<bool> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "calculators.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data =
      await rootBundle.load(join('assets/dataBase', 'calculators.db'));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes);
      return true;
    }
    return false;
  }

  Future<List<Map<String, dynamic>>> getData(String tableName) async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> history = await db.rawQuery(
        "Select * from $tableName");
    return history;
  }

  Future<void> deleteData(String tableName) async {
    Database db = await initDatabase();
    await db.rawQuery('delete from $tableName');
  }

  Future<void> insertData(String tableName,
      Map<String, dynamic> history) async {
    Database db = await initDatabase();
    await db.insert(tableName, history);
  }
}