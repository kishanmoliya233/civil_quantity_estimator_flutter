import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/history_display_design.dart';
import 'package:flutter/material.dart';

class HistoryBuildUpCalculationPage extends StatefulWidget {
  const HistoryBuildUpCalculationPage({super.key});

  @override
  State<HistoryBuildUpCalculationPage> createState() =>
      _HistoryBuildUpCalculationPageState();
}

class _HistoryBuildUpCalculationPageState
    extends State<HistoryBuildUpCalculationPage> {
  List<Map<String, dynamic>> history = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() {
    MyDatabase().copyPasteAssetFileToRoot().then((value) {
      MyDatabase().getData('Construction_Cost').then(
            (value) => setState(
              () {
                history = value;
              },
            ),
          );
    });
  }

  void deleteData() {
    MyDatabase().deleteData('Construction_Cost').then((value) => getData());
  }

  dynamic builtup;
  dynamic cost;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: const Text(
          'Construction Cost Calculator',
          style: TextStyle(fontSize: 18),
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text("Clear History"),
                ),
              ];
            },
            onSelected: (value) {
              if (value == 0) {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return AlertDialog(
                      backgroundColor: Colors.grey.shade200,
                      title: Row(children: const [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Text("  Alert")
                      ]),
                      content:
                          const Text('Are you sure want to clear all logs ?'),
                      actions: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey.shade300),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                          color: Colors.grey.shade300),
                                    ),
                                  ),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text(
                                      'No',
                                      style: TextStyle(
                                          fontSize: 17, color: Colors.black87),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    deleteData();
                                  },
                                  child: const Text(
                                    'Yes',
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.black87),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                );
              }
            },
          ),
        ],
      ),
      body: FutureBuilder(
        future: MyDatabase().getData('Construction_Cost'),
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.data!.isNotEmpty &&
              snapshot.connectionState != ConnectionState.waiting) {
            return ListView.builder(
              itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      elevation: 3,
                      margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pop([
                            builtup = history[index]["BuiltUpArea"],
                            cost = history[index]["Cost"]
                          ]);
                        },
                        child: Column(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              color: Colors.grey.shade300,
                              padding: const EdgeInsets.all(8.0),
                              child: const Text('Tap to Recalculate'),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        HistoryDisplay("BuiltUp Area : ", "${history[index]["BuiltUpArea"]} Sq. ft.", isDivider: true),
                                        HistoryDisplay("Approx Cost Per Square Feet : ", "${history[index]["Cost"]} ${buttons.currencyType}", isDivider: false),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
              },
              itemCount: history.length,
            );
          } else {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return const Center(
                child: Text(
                  'No History Found',
                  style: TextStyle(fontSize: 30, color: Colors.red),
                ),
              );
            }
          }
        },
      ),
    );
  }
}
