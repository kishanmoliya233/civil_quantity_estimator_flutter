import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/history_display_design.dart';
import 'package:flutter/material.dart';

class HistoryBoundryWallPage extends StatefulWidget {
  const HistoryBoundryWallPage({super.key});

  @override
  State<HistoryBoundryWallPage> createState() =>
      _HistoryBoundryWallPageState();
}

class _HistoryBoundryWallPageState
    extends State<HistoryBoundryWallPage> {
  List<Map<String, dynamic>> history = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() {
    MyDatabase().copyPasteAssetFileToRoot().then((value) {
      MyDatabase().getData('Boundry_Wall').then(
            (value) => setState(
              () {
            history = value;
          },
        ),
      );
    });
  }

  void deleteData() {
    MyDatabase().deleteData('Boundry_Wall').then((value) => getData());
  }

  dynamic lengthArea;
  dynamic lengthArea2;
  dynamic heightArea;
  dynamic heightArea2;
  dynamic lengthBar;
  dynamic lengthBar2;
  dynamic heightBar;
  dynamic heightBar2;
  dynamic unit;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: const Text(
          'Boundry Wall Calculator',
          style: TextStyle(fontSize: 18),
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text("Clear History"),
                ),
              ];
            },
            onSelected: (value) {
              if (value == 0) {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return AlertDialog(
                      backgroundColor: Colors.grey.shade200,
                      title: Row(children: const [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Text("  Alert")
                      ]),
                      content:
                      const Text('Are you sure want to clear all logs ?'),
                      actions: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey.shade300),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                          color: Colors.grey.shade300),
                                    ),
                                  ),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text(
                                      'No',
                                      style: TextStyle(
                                          fontSize: 17, color: Colors.black87),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    deleteData();
                                  },
                                  child: const Text(
                                    'Yes',
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.black87),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                );
              }
            },
          ),
        ],
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.data!.isNotEmpty &&
              snapshot.connectionState != ConnectionState.waiting) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 3,
                    margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop([
                          lengthArea = history[index]["LengthArea"],
                          lengthArea2 = history[index]["LengthArea2"],
                          heightArea = history[index]["HeightArea"],
                          heightArea2 = history[index]["HeightArea2"],
                          lengthBar = history[index]["LengthBar"],
                          lengthBar2 = history[index]["LengthBar2"],
                          heightBar = history[index]["HeightBar"],
                          heightBar2 = history[index]["HeightBar2"],
                          unit = history[index]["Unit"]
                        ]);
                      },
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            color: Colors.grey.shade300,
                            padding: const EdgeInsets.all(8.0),
                            child: const Text('Tap to Recalculate'),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      HistoryDisplay("Length of Area : ", "${history[index]["LengthArea"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'}  ${history[index]["LengthArea2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Height Of Area : ", "${history[index]["HeightArea"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'} ${history[index]["HeightArea2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Length Of Bar : ", "${history[index]["LengthBar"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'} ${history[index]["LengthBar2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Height Of Bar : ", "${history[index]["HeightBar"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'} ${history[index]["HeightBar2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: false),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: history.length,
            );
          } else {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return const Center(
                child: Text(
                  'No History Found',
                  style: TextStyle(fontSize: 30, color: Colors.red),
                ),
              );
            }
          }
        },
        future: MyDatabase().getData('Boundry_Wall'),
      ),
    );
  }
}
