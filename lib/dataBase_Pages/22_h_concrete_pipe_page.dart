import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/history_display_design.dart';
import 'package:flutter/material.dart';

class HistoryConcretePipePage extends StatefulWidget {
  const HistoryConcretePipePage({super.key});

  @override
  State<HistoryConcretePipePage> createState() =>
      _HistoryConcretePipePageState();
}

class _HistoryConcretePipePageState
    extends State<HistoryConcretePipePage> {
  List<Map<String, dynamic>> history = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() {
    MyDatabase().copyPasteAssetFileToRoot().then((value) {
      MyDatabase().getData('Concrete_Pipe').then(
            (value) => setState(
              () {
            history = value;
          },
        ),
      );
    });
  }

  void deleteData() {
    MyDatabase().deleteData('Concrete_Pipe').then((value) => getData());
  }

  dynamic gradeOfConcrete;
  dynamic innerDiameter;
  dynamic innerDiameter2;
  dynamic outerDiameter;
  dynamic outerDiameter2;
  dynamic height;
  dynamic height2;
  dynamic noOfPipes;
  dynamic unit;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: const Text(
          'Concrete Pipe Calculator',
          style: TextStyle(fontSize: 18),
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text("Clear History"),
                ),
              ];
            },
            onSelected: (value) {
              if (value == 0) {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return AlertDialog(
                      backgroundColor: Colors.grey.shade200,
                      title: Row(children: const [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Text("  Alert")
                      ]),
                      content:
                      const Text('Are you sure want to clear all logs ?'),
                      actions: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey.shade300),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                          color: Colors.grey.shade300),
                                    ),
                                  ),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text(
                                      'No',
                                      style: TextStyle(
                                          fontSize: 17, color: Colors.black87),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    deleteData();
                                  },
                                  child: const Text(
                                    'Yes',
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.black87),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                );
              }
            },
          ),
        ],
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.data!.isNotEmpty &&
              snapshot.connectionState != ConnectionState.waiting) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 3,
                    margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop([
                          gradeOfConcrete = history[index]["GradeOfConcrete"],
                          innerDiameter = history[index]["InnerDiameter"],
                          innerDiameter2 = history[index]["InnerDiameter2"],
                          outerDiameter = history[index]["OuterDiameter"],
                          outerDiameter2 = history[index]["OuterDiameter2"],
                          height = history[index]["Height"],
                          height = history[index]["Height2"],
                          noOfPipes = history[index]["NoOfPipes"],
                          unit = history[index]["Unit"]
                        ]);
                      },
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            color: Colors.grey.shade300,
                            padding: const EdgeInsets.all(8.0),
                            child: const Text('Tap to Recalculate'),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      HistoryDisplay("Inner Diameter : ", "${history[index]["InnerDiameter"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'}  ${history[index]["InnerDiameter2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Outer Diameter : ", "${history[index]["OuterDiameter"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'} ${history[index]["OuterDiameter2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Outer Diameter : ", "${history[index]["Height"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'} ${history[index]["Height2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("No. of Pipes : ", "${history[index]["NoOfPipes"]}", isDivider: true),
                                      HistoryDisplay("Grade of Concrete : ", "${history[index]["GradeOfConcrete"]}", isDivider: false),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: history.length,
            );
          } else {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return const Center(
                child: Text(
                  'No History Found',
                  style: TextStyle(fontSize: 30, color: Colors.red),
                ),
              );
            }
          }
        },
        future: MyDatabase().getData('Concrete_Pipe'),
      ),
    );
  }
}
