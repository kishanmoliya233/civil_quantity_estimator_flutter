import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/history_display_design.dart';
import 'package:flutter/material.dart';

class HistoryStairCasePage extends StatefulWidget {
  const HistoryStairCasePage({super.key});

  @override
  State<HistoryStairCasePage> createState() =>
      _HistoryStairCasePageState();
}

class _HistoryStairCasePageState
    extends State<HistoryStairCasePage> {
  List<Map<String, dynamic>> history = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() {
    MyDatabase().copyPasteAssetFileToRoot().then((value) {
      MyDatabase().getData('Stair_Case').then(
            (value) => setState(
              () {
            history = value;
          },
        ),
      );
    });
  }

  void deleteData() {
    MyDatabase().deleteData('Stair_Case').then((value) => getData());
  }

  dynamic riser;
  dynamic riser2;
  dynamic tread;
  dynamic tread2;
  dynamic stairWidth;
  dynamic stairHeight;
  dynamic thicknessOfSlab;
  dynamic gradeOfConcrete;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: const Text(
          'Stair Case Calculator',
          style: TextStyle(fontSize: 18),
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text("Clear History"),
                ),
              ];
            },
            onSelected: (value) {
              if (value == 0) {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return AlertDialog(
                      backgroundColor: Colors.grey.shade200,
                      title: Row(children: const [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Text("  Alert")
                      ]),
                      content:
                      const Text('Are you sure want to clear all logs ?'),
                      actions: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey.shade300),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                          color: Colors.grey.shade300),
                                    ),
                                  ),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text(
                                      'No',
                                      style: TextStyle(
                                          fontSize: 17, color: Colors.black87),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    deleteData();
                                  },
                                  child: const Text(
                                    'Yes',
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.black87),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                );
              }
            },
          ),
        ],
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.data!.isNotEmpty &&
              snapshot.connectionState != ConnectionState.waiting) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 3,
                    margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop([
                          riser = history[index]["Riser"],
                          riser2 = history[index]["Riser2"],
                          tread = history[index]["Tread"],
                          tread2 = history[index]["Tread2"],
                          stairWidth = history[index]["StairWidth"],
                          stairHeight = history[index]["StairHeight"],
                          thicknessOfSlab = history[index]["ThicknessOfSlab"],
                          gradeOfConcrete = history[index]["GradeOfConcrete"]
                        ]);
                      },
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            color: Colors.grey.shade300,
                            padding: const EdgeInsets.all(8.0),
                            child: const Text('Tap to Recalculate'),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      HistoryDisplay("Riser Feet : ", "${history[index]["Riser"]} Feet", isDivider: true),
                                      HistoryDisplay("Riser Inch : ", "${history[index]["Riser2"]} Inch", isDivider: true),
                                      HistoryDisplay("Tread Feet : ", "${history[index]["Tread"]} Feet", isDivider: true),
                                      HistoryDisplay("Tread Inch : ", "${history[index]["Tread2"]} Inch", isDivider: true),
                                      HistoryDisplay("Width of stair : ", "${history[index]["StairWidth"]} Feet", isDivider: true),
                                      HistoryDisplay("Height of stair : ", "${history[index]["StairHeight"]} Feet", isDivider: true),
                                      HistoryDisplay("Thickness of waist Slab : ", "${history[index]["ThicknessOfSlab"]} Inch", isDivider: true),
                                      HistoryDisplay("Grade of Concrete : ", "${history[index]["GradeOfConcrete"]}", isDivider: false),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: history.length,
            );
          } else {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return const Center(
                child: Text(
                  'No History Found',
                  style: TextStyle(fontSize: 30, color: Colors.red),
                ),
              );
            }
          }
        },
        future: MyDatabase().getData('Stair_Case'),
      ),
    );
  }
}
