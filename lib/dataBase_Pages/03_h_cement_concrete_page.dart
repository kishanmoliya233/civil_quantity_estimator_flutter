import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/history_display_design.dart';
import 'package:flutter/material.dart';

class HistoryCementConcretePage extends StatefulWidget {
  const HistoryCementConcretePage({super.key});

  @override
  State<HistoryCementConcretePage> createState() =>
      _HistoryCementConcretePageState();
}

class _HistoryCementConcretePageState
    extends State<HistoryCementConcretePage> {
  List<Map<String, dynamic>> history = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() {
    MyDatabase().copyPasteAssetFileToRoot().then((value) {
      MyDatabase().getData('Cement_Concrete').then(
            (value) => setState(
              () {
            history = value;
          },
        ),
      );
    });
  }

  void deleteData() {
    MyDatabase().deleteData('Cement_Concrete').then((value) => getData());
  }

  dynamic length;
  dynamic length2;
  dynamic width;
  dynamic width2;
  dynamic depth;
  dynamic depth2;
  dynamic gradeOfConcrete;
  dynamic unite;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: const Text(
          'Cement Concrete Calculator',
          style: TextStyle(fontSize: 18),
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) {
              return [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text("Clear History"),
                ),
              ];
            },
            onSelected: (value) {
              if (value == 0) {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return AlertDialog(
                      backgroundColor: Colors.grey.shade200,
                      title: Row(children: const [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Text("  Alert")
                      ]),
                      content:
                      const Text('Are you sure want to clear all logs ?'),
                      actions: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey.shade300),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border(
                                      right: BorderSide(
                                          color: Colors.grey.shade300),
                                    ),
                                  ),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text(
                                      'No',
                                      style: TextStyle(
                                          fontSize: 17, color: Colors.black87),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    deleteData();
                                  },
                                  child: const Text(
                                    'Yes',
                                    style: TextStyle(
                                        fontSize: 17, color: Colors.black87),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                );
              }
            },
          ),
        ],
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.hasData &&
              snapshot.data!.isNotEmpty &&
              snapshot.connectionState != ConnectionState.waiting) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 3,
                    margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop([
                          gradeOfConcrete = history[index]["GradeOfConcrete"],
                          length = history[index]["Length"],
                          length2 = history[index]["Length2"],
                          width = history[index]["Width"],
                          width2 = history[index]["Width2"],
                          depth = history[index]["Depth"],
                          depth2 = history[index]["Depth2"],
                          unite = history[index]["Unit"]
                        ]);
                      },
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            color: Colors.grey.shade300,
                            padding: const EdgeInsets.all(8.0),
                            child: const Text('Tap to Recalculate'),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      HistoryDisplay("Length : ", "${history[index]["Length"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'}  ${history[index]["Length2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Width : ", "${history[index]["Width"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'} ${history[index]["Width2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Depth : ", "${history[index]["Depth"]} ${history[index]["Unit"] == '1' ? 'Feet' : 'Meter'} ${history[index]["Depth2"]} ${history[index]["Unit"] == '1' ? 'Inch' : 'Cm'}", isDivider: true),
                                      HistoryDisplay("Grade of Concrete : ", "${history[index]["GradeOfConcrete"]}", isDivider: false),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: history.length,
            );
          } else {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return const Center(
                child: Text(
                  'No History Found',
                  style: TextStyle(fontSize: 30, color: Colors.red),
                ),
              );
            }
          }
        },
        future: MyDatabase().getData('Cement_Concrete'),
      ),
    );
  }
}
