import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUsPage extends StatefulWidget {
  const AboutUsPage({Key? key}) : super(key: key);

  @override
  State<AboutUsPage> createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  double space = 5.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: Row(
          children: const [
            Text(
              'About Us',
              style: TextStyle(fontSize: 18),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
      ),
      body: Scrollbar(
        thickness: 5,
        radius: const Radius.circular(20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //region LOGO AND VERSION...
              Container(
                padding: EdgeInsets.all(space * 2),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Image.asset('assets/images/Logo_CivilEngineeringCalculators.png',scale: 3.3,),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Civil Quantity Estimator 2023 (version)",
                        style: TextStyle(
                          fontSize: 13,
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //endregion

              //region MEET OUR TEAM...
              getCustomLabelBox(title: 'Meet Our Team'),
              getCustomContainer(
                widget: Column(
                  children: [
                    getCustomInformationContainer(
                      label: 'Developed by ',
                      text: 'Moliya Kishan (21010101128),\nSongara Utsav H.(190543107022),\nChaudhri Pooja S.(170540107183)',
                    ),
                    getCustomInformationContainer(
                      label: 'Mentored by',
                      text: 'Dr. Pradyumansinh Jadeja, Computer Engineering Department',
                    ),
                    getCustomInformationContainer(
                      label: 'Explored by',
                      text: 'ASWDC, School of Computer Science',
                    ),
                    getCustomInformationContainer(
                      label: 'Eulogized by',
                      text: 'Darshan Institute of Engineering & Technology, Rajkot, Gujarat - INDIA',
                    ),
                  ],
                ),
              ),
              //endregion

              //region ABOUT ASWDC...
              getCustomLabelBox(title: 'About ASWDC'),
              getCustomContainer(
                widget: Column(
                  children: [
                    Container(
                      color: Colors.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          getCustomImageContainer(imgPath: "assets/images/logo_du.png"),
                          getCustomImageContainer(imgPath: "assets/images/logo_aswdc.png",),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: space * 2),
                      child: getCustomTextContainer(
                        text: "ASWDC is Application, Software and website Development Center @ Darshan University run by Students and Staff of School of Computer Science.",
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: space * 2),
                      child: getCustomTextContainer(
                        text: "Sole purpose of ASWDC is to bridge gap between university curriculum & industry demands. Students learn cutting edge technologies, develop real world application & experiences professional environment @ ASWDC under guidance of industry experts & faculty members.",
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
              //endregion

              //region CONTACT US...
              getCustomLabelBox(title: 'Contact Us'),
              getCustomContainer(
                widget: Column(
                  children: [
                    getCustomContactContainer(
                      icon: 'assets/icons/ic_email.png',
                      text: 'aswdc@darshan.ac.in',
                      url: 'mailto:aswdc@darshan.ac.in',
                    ),
                    getCustomContactContainer(
                        icon: 'assets/icons/ic_phone.png',
                        text: '+91-9727747317',
                        url: 'tel:9727747317'),
                    getCustomContactContainer(
                      icon: 'assets/icons/ic_website.png',
                      text: 'www.darshan.ac.in',
                      url: 'https:www.darshan.ac.in',
                    ),
                    getCustomContactContainer(
                      icon: 'assets/icons/ic_share.png',
                      text: 'Share app',
                      isShare: true,
                    ),
                    getCustomContactContainer(
                      icon: 'assets/icons/ic_android.png',
                      text: 'More Apps',
                      url:
                      'https://play.google.com/store/apps/developer?id=Darshan+University',
                    ),
                    getCustomContactContainer(
                      icon: 'assets/icons/ic_star.png',
                      text: 'Rate Us',
                      url:
                      'https://play.google.com/store/apps/details?id=com.aswdc_civilquantityestimator',
                    ),
                    getCustomContactContainer(
                      icon: 'assets/icons/ic_update.png',
                      text: 'Check for Updates',
                      url:
                      'https://play.google.com/store/apps/details?id=com.aswdc_civilquantityestimator',
                    ),
                  ],
                ),
              ),
              //endregion

              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "\u00a9 2023 Darshan University",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "All Rights Reversed - ",
                        style: TextStyle(fontSize: 12),
                      ),
                      InkWell(
                        onTap: () {
                          launchUrl(
                            Uri.parse(
                                "http://www.darshan.ac.in/DIET/ASWDC-Mobile-Apps/Privacy-Policy-General"),
                            mode: LaunchMode.externalApplication,
                          );
                        },
                        child: const Text(
                          "Privacy Policy",
                          style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: 12,
                          ),
                        ),
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: space),
                    child: const Text(
                      "Made with ♥ in India ",
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getCustomLabelBox({title}) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(space),
            topRight: Radius.circular(space)),
        color: AppColors.primaryColor,
      ),
      margin: EdgeInsets.symmetric(horizontal: space * 4),
      padding: EdgeInsets.symmetric(
          horizontal: space * 2, vertical: space),
      child: Text(
        title,
        style: const TextStyle(
          fontSize: 17,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget getCustomContainer ({widget}){
    return Card(
      elevation:  4,
      margin: EdgeInsets.fromLTRB(space, 0, space, space * 5),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: space * 3, vertical: space),
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.primaryColor),
          borderRadius: BorderRadius.circular(space),
          color: Colors.white,
        ),
        child: widget,
      ),
    );
  }

  Widget getCustomInformationContainer({label, text}) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 7),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 2,
            child: getCustomTextContainer(text: label, color: AppColors.primaryColor),
          ),
          Expanded(
            child: getCustomTextContainer(text: '\t\t:', color: AppColors.primaryColor),
          ),
          Expanded(
            flex: 5,
            child: getCustomTextContainer(text: text,),
          ),
        ],
      ),
    );
  }

  Widget getCustomTextContainer({text, color, textAlign}) {
    return Text(
      text,
      style: TextStyle(
        color: (color == null) ? Colors.blueGrey : AppColors.primaryColor,
        fontWeight: FontWeight.bold,
        fontSize: 13,
      ),
      textAlign: textAlign,
    );
  }

  Widget getCustomImageContainer ({imgPath}){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: space * 2),
      child: Image.asset(
        imgPath,
        filterQuality: FilterQuality.high,
        scale: 5.5,
      ),
    );
  }

  Widget getCustomContactContainer ({icon, text, isDatabaseUpdate = false, url, isShare = false}){
    return InkWell(
      onTap: () async {
        if (isShare){
          final box = context.findRenderObject() as RenderBox?;
          String content =
              "Download App Civil Quantity Estimator - 2023 from Play Store";
          await Share.share(
            "$content : https://play.google.com/store/apps/details?id=com.aswdc_civilquantityestimator",
            sharePositionOrigin:
            box!.localToGlobal(Offset.zero) & box.size,
          );
        }else{
          launchUrl(
            Uri.parse(url),
            mode: LaunchMode.externalApplication,
          );
        }
      },
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: space, vertical: space * 1.5),
            child: Image.asset(
              icon,
              color: AppColors.primaryColor,
              scale: space * 6,
              filterQuality: FilterQuality.high,
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: space * 2),
              child: getCustomTextContainer(text: text),
            ),
          ),
        ],
      ),
    );
  }
}
