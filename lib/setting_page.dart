import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  void initState() {
    super.initState();
    getData();
  }

  static String dropdownValue = 'Feet / Inch';
  static String dropdownValue2 = 'INR (\u{20B9})';
  String Key_dropdownValue = 'volume';
  String Key_dropdownValue2 = 'currency';

  void getData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    dropdownValue = sp.getString(Key_dropdownValue) ?? 'Meter / CM';
    dropdownValue2 = sp.getString(Key_dropdownValue2) ?? 'INR (\u{20B9})';
    buttons.isButtonClick1 = dropdownValue == 'Meter / CM' ? false : true;
    buttons.isButtonClick2 = dropdownValue == 'Meter / CM' ? true : false;
  }

  setVolume(value) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString(Key_dropdownValue, value);
    getData();
  }

  setCurrency(value) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString(Key_dropdownValue2, value);
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: Row(
          children: const [
            Text(
              'Settings',
              style: TextStyle(fontSize: 18),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              elevation: 3,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InputDecorator(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Default Unit'),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton2(
                          value: dropdownValue,
                          isExpanded: true,
                          isDense: true,
                          onChanged: (String? newValue) {
                            setState(() {
                              dropdownValue = newValue!;
                            });
                          },
                          items: <String>['Feet / Inch', 'Meter / CM']
                              .map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: const TextStyle(
                                      fontSize: 16,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }).toList(),
                          buttonHeight: 15,
                          itemHeight: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InputDecorator(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Default Currency'),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton2(
                          value: dropdownValue2,
                          isExpanded: true,
                          isDense: true,
                          onChanged: (String? newValue) {
                            setState(() {
                              dropdownValue2 = newValue!;
                            });
                          },
                          items: <String>['INR (\u{20B9})', 'USD (\$)']
                              .map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: const TextStyle(
                                      fontSize: 16,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }).toList(),
                          buttonHeight: 18,
                          itemHeight: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              FocusManager.instance.primaryFocus?.unfocus();
                              setState(() {
                                setVolume(dropdownValue);
                                setCurrency(dropdownValue2);
                                showToastMessage("Setting saved Successfully");
                                Navigator.pop(context);
                              });
                            },
                            child: Container(
                              decoration: const BoxDecoration(
                                color: AppColors.primaryColor,
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(5),
                                    bottomRight: Radius.circular(5)),
                              ),
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(8),
                              child: const Text(
                                'Save',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  void showToastMessage(String message){
    Fluttertoast.showToast(
        msg: message, //message to show toast
       // toastLength: Toast.LENGTH_LONG, //duration for message to show
      //  gravity: ToastGravity.BOTTOM, //where you want to show, top, bottom
        timeInSecForIosWeb: 1, //for iOS only
      //  backgroundColor: Colors.red, //background Color for message
        //textColor: Colors.white, //message text color
        fontSize: 16.0 //message font size
    );
  }
}
