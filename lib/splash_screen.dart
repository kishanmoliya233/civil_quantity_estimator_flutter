import 'dart:async';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:flutter/material.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    Timer(
      const Duration(seconds: 4),
      () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const Home_page(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Image.asset(
                    'assets/images/bg3.jpg',
                    fit: BoxFit.fill,
                  ),
                  Container(
                    alignment: Alignment.topCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Container(
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(bottomRight: Radius.circular(70),bottomLeft: Radius.circular(70),)
                             , color: Colors.white24,
                            ),
                            child: customImageContainer(
                              imgPath:
                                  "assets/images/Logo_CivilEngineeringCalculators.png",
                              imgHeight: screenHeight * 0.08,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      color: Colors.white60,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                              child: customImageContainer(
                            imgPath: "assets/images/logo_du.png",
                            imgHeight: screenHeight * 0.08,
                          )),
                          Expanded(
                              child: customImageContainer(
                            imgPath: "assets/images/logo_aswdc.png",
                            imgHeight: screenHeight * 0.1,
                          )),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //region CUSTOM IMAGE CONTAINER...
  Widget customImageContainer({imgPath, imgHeight}) {
    return Container(
      margin: const EdgeInsets.all(5),
      child: Image.asset(
        imgPath,
        height: imgHeight,
        filterQuality: FilterQuality.high,
      ),
    );
  }
//endregion
}
