import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:flutter/material.dart';

class HistoryDisplay extends StatelessWidget {
  String txt;
  String value;
  bool? isDivider;
  HistoryDisplay(this.txt, this.value, {super.key, required bool this.isDivider});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Text(txt),
              Text(
                value,
                style: const TextStyle(
                    fontWeight:
                    FontWeight.bold),
              ),
            ],
          ),
        ),
        isDivider! ?  const Dividers(): Container(),
      ],
    );
  }
}
