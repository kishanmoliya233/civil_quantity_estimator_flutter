import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:flutter/material.dart';

class VolumeButtonType extends StatefulWidget {
  const VolumeButtonType({Key? key}) : super(key: key);

  @override
  State<VolumeButtonType> createState() => _VolumeButtonTypeState();
}

class _VolumeButtonTypeState extends State<VolumeButtonType> {
  static bool isButtonClick1 = true;
  static bool isButtonClick2 = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(8, 10, 8, 8),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 60,
              padding: const EdgeInsets.only(left: 0, top: 8, bottom: 8),
              child: Form(
                //key: _formKey,
                  child: TextButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: isButtonClick1 ? AppColors.primaryColor : Colors.white,
                      shape: const RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey),
                          borderRadius: BorderRadius.horizontal(
                            left: Radius.circular(10),
                          )),
                    ),
                    onPressed: () {
                      setState(() {
                        isButtonClick1 = true;
                        isButtonClick2 = false;
                      });
                    },
                    child: Text('Feet / Inch',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            color: isButtonClick2 ? Colors.black : Colors.white,
                            fontWeight: FontWeight.bold)),
                  )),
            ),
          ),
          Expanded(
            child: Container(
              height: 60,
              padding: const EdgeInsets.only(right: 0, top: 8, bottom: 8),
              child: Form(
                //  key: _formKey1,
                child: TextButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: isButtonClick2 ? AppColors.primaryColor : Colors.white,
                    shape: const RoundedRectangleBorder(
                        side: BorderSide(color: Colors.grey),
                        borderRadius: BorderRadius.horizontal(
                          right: Radius.circular(10),
                        )),
                  ),
                  onPressed: () {
                    setState(() {
                      isButtonClick2 = true;
                      isButtonClick1 = false;
                    });
                  },
                  child: Text('Meter / CM',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          color: isButtonClick2 ? Colors.white : Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
