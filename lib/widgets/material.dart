import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:flutter/material.dart';

class GetMaterial extends StatelessWidget {
  String image;
  String name;
  String contity;
  String materialType;
  bool? isDivider;
  GetMaterial(this.image, this.name, this.contity, this.materialType, {super.key, required bool this.isDivider});

  @override
  Widget build(BuildContext context) {
    double fonts13 = AppFonts.font13;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.all(8),
                  width: 28,
                  height: 28,
                  child: Image.asset(
                    image,
                    bundle: null,
                  ),
                ),
                Text(
                  name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts13,
                      color: Colors.black),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  contity,
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: fonts13,
                      color: Colors.black),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(materialType,
                      style: TextStyle(
                          fontSize: fonts13,
                          color: Colors.black87)),
                ),
              ],
            ),
          ],
        ),
        isDivider! ?  const Dividers(): Container(),
      ],
    );
  }
}
