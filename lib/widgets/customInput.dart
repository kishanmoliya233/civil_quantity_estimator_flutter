import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomInput extends StatefulWidget {
  final String label;
  dynamic controller;
  final String errMessage;
  dynamic formkey;
  bool? validate;

  CustomInput(
      {Key? key,
      required this.label,
      this.controller,
      required this.errMessage,
      this.formkey,
      this.validate});

  @override
  State<CustomInput> createState() => _CustomInputState();
}

class _CustomInputState extends State<CustomInput> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formkey,
      child: TextFormField(
        controller: widget.controller,
        cursorColor: AppColors.primaryColor,
        inputFormatters: [
          LengthLimitingTextInputFormatter(7),
        ],
        onChanged: (value) {
          setState(() {});
        },
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          labelText: widget.label,
          labelStyle: const TextStyle(color: Colors.black),
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.primaryColor)),
          border: const OutlineInputBorder(),
          contentPadding: const EdgeInsets.all(8),
        ),
        validator: (value) {
          if (value != null && value.isEmpty && widget.validate == null) {
            return widget.errMessage;
          }

          if(value!.isNotEmpty){
            if (widget.validate == true && buttons.isButtonClick1! == true) {
              if (int.parse(value) < 0 || int.parse(value) > 11) {
                return 'Enter between 0 to 11';
              }
            } else if (widget.validate == true &&
                buttons.isButtonClick2! == true) {
              if (int.parse(value) < 0 || int.parse(value) >= 100) {
                return 'Enter between 0 to 99';
              }
            }
          }
          return null;
        },
      ),
    );
  }
}
