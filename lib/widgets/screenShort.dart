import 'dart:typed_data';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:share_plus/share_plus.dart';

class ScreenShort{
  void captureScreenShort(controller) async {
    controller
        .capture(delay: const Duration(milliseconds: 10))
        .then((capturedImage) async {
      if (capturedImage != null) {
        Uint8List pngint8 = capturedImage.buffer.asUint8List();

        await ImageGallerySaver.saveImage(Uint8List.fromList(pngint8),
            quality: 100, name: 'screenShot-${DateTime.now()}');

        final image =
        await ImagePicker().pickImage(source: (ImageSource.gallery));
        if (image == null) return;
        await Share.shareFiles([image.path]);
      }
    }).catchError((onError) {
      print(onError);
    });
  }
}