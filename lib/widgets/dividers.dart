import 'package:flutter/material.dart';

class Dividers extends StatefulWidget {
  const Dividers({super.key});
  @override
  State<Dividers> createState() => _DividersState();
}

class _DividersState extends State<Dividers> {
  @override
  Widget build(BuildContext context) {
    return const Divider(
      color: Colors.black,
      thickness: 0.1,
      height: 1,
      indent: 10,
      endIndent: 10,
    );
  }
}
