import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/14_h_excavation_calculator_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class ExcavationCalculationPage extends StatefulWidget {
  const ExcavationCalculationPage({super.key});

  @override
  State<ExcavationCalculationPage> createState() =>
      _ExcavationCalculationPageState();
}

class _ExcavationCalculationPageState extends State<ExcavationCalculationPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double length = 0;
  double width = 0;
  double depth = 0;

  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  String? length2Label;
  String? lengthLabel;
  String? Width2Label;
  String? WidthLabel;
  String? depthLabel;
  String? depth2Label;

  var excavationLength = TextEditingController();
  var excavationLength2 = TextEditingController();
  var excavationWidth = TextEditingController();
  var excavationWidth2 = TextEditingController();
  var excavationDepth = TextEditingController();
  var excavationDepth2 = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey8 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey9 = GlobalKey<FormState>();
  late double _ans = 0;
  late double _ansFinal = 0;

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String Width2Label =
        buttons.isButtonClick2! ? 'Width (cm)' : 'Width (inch)';
    String WidthLabel =
        buttons.isButtonClick1! ? 'Width (feet)' : 'Width (meter)';
    String depth2Label =
        buttons.isButtonClick1! ? 'Depth (cm)' : 'Depth (inch)';
    String depthLabel =
        buttons.isButtonClick2! ? 'Depth (feet)' : 'Depth (meter)';
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: ((context) {
              return const Home_page();
            }),
          ),
        );
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: AppBarColor().getAppBarColor(),
          titleSpacing: 0,
          title: Row(
            children: [
              Image.asset(
                'assets/icons/excavationCalculator.png',
                width: 35,
                height: 35,
                color: Colors.white,
              ),
              Expanded(
                child: Text(
                  ' Excavation Calculator',
                  style: TextStyle(fontSize: fonts17),
                ),
              ),
            ],
          ),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15))),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: ((context) {
                        return HistoryExcavationCalculatorPage();
                      }),
                    ),
                  ).then(
                    (value) {
                      if (value != null) {
                        isDataVisible = false;
                        excavationLength.clear();
                        excavationLength2.clear();
                        excavationWidth.clear();
                        excavationWidth2.clear();
                        excavationDepth.clear();
                        excavationDepth2.clear();

                        excavationLength =
                            TextEditingController(text: value[0]);
                        excavationLength2 =
                            TextEditingController(text: value[1]);
                        excavationWidth = TextEditingController(text: value[2]);
                        excavationWidth2 =
                            TextEditingController(text: value[3]);
                        excavationDepth = TextEditingController(text: value[4]);
                        excavationDepth2 =
                            TextEditingController(text: value[5]);
                        buttons.isButtonClick1 = value[6] == '1' ? true : false;
                        buttons.isButtonClick2 = value[6] == '1' ? false : true;
                        _calculateVolume();
                      }
                    },
                  );
                },
                child: const Icon(Icons.history),
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Screenshot(
            controller: screenshotController,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Form(child: getVolumeType1()),
                      ),
                      Expanded(
                        child: Form(
                          child: getVolumeType2(),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                  child: Card(
                    elevation: 4,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                  label: lengthLabel,
                                  controller: excavationLength,
                                  errMessage: 'Enter $lengthLabel',
                                  formkey: formKey,
                                  // validate: _validate,
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                    label: length2Label,
                                    controller: excavationLength2,
                                    errMessage: 'Enter $length2Label',
                                    formkey: formKey1,
                                    validate: true),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                  label: WidthLabel,
                                  controller: excavationWidth,
                                  errMessage: 'Enter $WidthLabel',
                                  formkey: formKey2,
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                    label: Width2Label,
                                    controller: excavationWidth2,
                                    errMessage: 'Enter $Width2Label',
                                    formkey: formKey3,
                                    validate: true),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                  label: depthLabel,
                                  controller: excavationDepth,
                                  errMessage: 'Enter $depthLabel',
                                  formkey: formKey4,
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                    label: depth2Label,
                                    controller: excavationDepth2,
                                    errMessage: 'Enter $depth2Label',
                                    formkey: formKey5,
                                    validate: true),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                    setState(() {
                                      if (formKey.currentState!.validate() &&
                                          formKey2.currentState!.validate() &&
                                          formKey1.currentState!.validate() &&
                                          formKey3.currentState!.validate() &&
                                          formKey5.currentState!.validate() &&
                                          formKey4.currentState!.validate()) {
                                        _calculateVolume();

                                        //add data in database
                                        Map<String, dynamic> history = {};
                                        history['Length'] =
                                            excavationLength.text;
                                        history['Length2'] =
                                            excavationLength2.text == ''
                                                ? 0
                                                : excavationLength2.text;
                                        history['Width'] = excavationWidth.text;
                                        history['Width2'] =
                                            excavationWidth2.text == ''
                                                ? 0
                                                : excavationWidth2.text;
                                        history['Depth'] = excavationDepth.text;
                                        history['Depth2'] =
                                            excavationDepth2.text == ''
                                                ? 0
                                                : excavationDepth2.text;
                                        history['Unit'] =
                                            buttons.isButtonClick1;
                                        MyDatabase().insertData(
                                            'Excavation_Cal', history);
                                      }
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(5)),
                                    ),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(right: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Calculate',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      isDataVisible = false;
                                      excavationLength.clear();
                                      excavationLength2.clear();
                                      excavationWidth.clear();
                                      excavationWidth2.clear();
                                      excavationDepth.clear();
                                      excavationDepth2.clear();
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(5))),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(left: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Reset',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                if (isDataVisible) getVolume(),
              ],
            ),
          ),
        ),
        floatingActionButton: isDataVisible
            ? FloatingActionButton(
                onPressed: () {
                  ScreenShort().captureScreenShort(screenshotController);
                },
                backgroundColor: AppColors.primaryColor,
                child: const Icon(Icons.share),
              )
            : Container(),
      ),
    );
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    excavationLength2.text.isEmpty
        ? excavationLength2 = TextEditingController(text: '0')
        : excavationLength2;
    excavationWidth2.text.isEmpty
        ? excavationWidth2 = TextEditingController(text: '0')
        : excavationWidth2;
    excavationDepth2.text.isEmpty
        ? excavationDepth2 = TextEditingController(text: '0')
        : excavationDepth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    length = double.parse('${excavationLength.text}.${excavationLength2.text}');
    width = double.parse('${excavationWidth.text}.${excavationWidth2.text}');
    depth = double.parse('${excavationDepth.text}.${excavationDepth2.text}');
    if ((buttons.isButtonClick1!)) {
      setState(() {
        _ansFinal = (((length * width * depth) / 35.3147) * fac).round() / fac;
        _ans = (((length * width * depth)) * fac).round() / fac;
      });
    } else {
      setState(() {
        _ans = (((length * width * depth) * 35.3147) * fac).round() / fac;
        _ansFinal = (((length * width * depth)) * fac).round() / fac;
      });
    }
    excavationLength2.text != '0'
        ? excavationLength2 = excavationLength2
        : excavationLength2.clear();
    excavationWidth2.text != '0'
        ? excavationWidth2 = excavationWidth2
        : excavationWidth2.clear();
    excavationDepth2.text != '0'
        ? excavationDepth2 = excavationDepth2
        : excavationDepth2.clear();
  }

  Widget getVolume() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Total Volume of Excavation',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: fonts17)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$_ansFinal m',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$_ans ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
