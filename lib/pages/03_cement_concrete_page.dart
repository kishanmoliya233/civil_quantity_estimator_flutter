import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/03_h_cement_concrete_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class CementConcretePage extends StatefulWidget {
  const CementConcretePage({super.key});

  @override
  State<CementConcretePage> createState() => _CementConcretePageState();
}

class _CementConcretePageState extends State<CementConcretePage> {
  ScreenshotController screenshotController = ScreenshotController();
  bool isDataVisible = false;

  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  String dropdownValue = 'M20 (1:1.5:3)';

  var length = TextEditingController();
  var length2 = TextEditingController();
  var width = TextEditingController();
  var width2 = TextEditingController();
  var depth = TextEditingController();
  var depth2 = TextEditingController();
  var formKey1 = GlobalKey<FormState>();
  var formKey2 = GlobalKey<FormState>();
  var formKey3 = GlobalKey<FormState>();
  var formKey4 = GlobalKey<FormState>();
  var formKey5 = GlobalKey<FormState>();
  var formKey6 = GlobalKey<FormState>();

  double finalVolumeFeet = 0;
  double finalVolumeMeter = 0;
  double _cementBag = 0;
  double _sandTon = 0;
  double _aggTon = 0;

  String? length2Label;
  String? lengthLabel;
  String? width2Label;
  String? widthLabel;
  String? depth2Label;
  String? depthLabel;

  bool isChangeValue = true;

  List<ChartData> chartData = [];
  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    chartData = [
      ChartData('Cement\n$_cementBag Bags', _cementBag * 50, Colors.blue),
      ChartData('Sand\n$_sandTon Ton', _sandTon * 1000, Colors.red),
      ChartData(
          'Aggregate\n$_aggTon Ton', _aggTon * 1000, Colors.yellow),
    ];

    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String width2Label =
        buttons.isButtonClick2! ? 'Width (cm)' : 'Width (inch)';
    String widthLabel =
        buttons.isButtonClick1! ? 'Width (feet)' : 'Width (meter)';
    String depth2Label =
        buttons.isButtonClick2! ? 'Depth (cm)' : 'Depth (inch)';
    String depthLabel =
        buttons.isButtonClick1! ? 'Depth (feet)' : 'Depth (meter)';
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: ((context) {
              return const Home_page();
            }),
          ),
        );
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: AppBarColor().getAppBarColor(),
          titleSpacing: 0,
          title: Row(
            children: [
              Image.asset(
                'assets/icons/concrete-mixer.png',
                width: 45,
                height: 45,
                color: Colors.white,
              ),
              Expanded(
                child: Text(
                  ' Cement Concrete Calculator',
                  style: TextStyle(fontSize: fonts17),
                ),
              ),
            ],
          ),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15))),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: ((context) {
                        return HistoryCementConcretePage();
                      }),
                    ),
                  ).then(
                    (value) {
                      setState(() {
                        if (value != null) {
                          isDataVisible = false;
                          length.clear();
                          length2.clear();
                          width.clear();
                          width2.clear();
                          depth.clear();
                          depth2.clear();
                          dropdownValue = 'M20 (1:1.5:3)';

                          dropdownValue = value[0];
                          length = TextEditingController(text: value[1]);
                          length2 = TextEditingController(text: value[2]);
                          width = TextEditingController(text: value[3]);
                          width2 = TextEditingController(text: value[4]);
                          depth = TextEditingController(text: value[5]);
                          depth2 = TextEditingController(text: value[6]);
                          buttons.isButtonClick1 =
                              value[7] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[7] == '1' ? false : true;
                          _calculateVolume();
                        }
                      });
                    },
                  );
                },
                child: const Icon(Icons.history),
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Screenshot(
            controller: screenshotController,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Form(child: getVolumeType1()),
                      ),
                      Expanded(
                        child: Form(
                          child: getVolumeType2(),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                  child: Card(
                    elevation: 4,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                          child: SizedBox(
                              child: InputDecorator(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Grade of Concrete'),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                value: dropdownValue,
                                isExpanded: true,
                                isDense: true,
                                icon:
                                    const Icon(Icons.arrow_drop_down_outlined),
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    dropdownValue = newValue!;
                                  });
                                },
                                items: <String>[
                                  'M20 (1:1.5:3)',
                                  'M15 (1:2:4)',
                                  'M10 (1:3:6)',
                                  'M7.5 (1:4:8)'
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                buttonHeight: 18,
                                itemHeight: 40,
                              ),
                            ),
                          )),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                  label: lengthLabel,
                                  controller: length,
                                  errMessage: 'Enter $lengthLabel',
                                  formkey: formKey1,
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                  label: length2Label,
                                  controller: length2,
                                  errMessage: 'Enter $length2Label',
                                  formkey: formKey2,
                                  validate: true,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                    label: widthLabel,
                                    controller: width,
                                    errMessage: 'Enter $widthLabel',
                                    formkey: formKey3),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                  label: width2Label,
                                  controller: width2,
                                  errMessage: 'Enter $width2Label',
                                  formkey: formKey4,
                                  validate: true,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                    label: depthLabel,
                                    controller: depth,
                                    errMessage: 'Enter $depthLabel',
                                    formkey: formKey5),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                  label: depth2Label,
                                  controller: depth2,
                                  errMessage: 'Enter $depth2Label',
                                  formkey: formKey6,
                                  validate: true,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                    setState(() {
                                      if (formKey1.currentState!.validate() &&
                                          formKey2.currentState!.validate() &&
                                          formKey3.currentState!.validate() &&
                                          formKey4.currentState!.validate() &&
                                          formKey5.currentState!.validate() &&
                                          formKey6.currentState!.validate()) {
                                        _calculateVolume();

                                        //add data in database
                                        Map<String, dynamic> history = {};
                                        history['GradeOfConcrete'] =
                                            dropdownValue;
                                        history['Length'] = length.text;
                                        history['Length2'] = length2.text == ''
                                            ? 0
                                            : length2.text;
                                        history['Width'] = width.text;
                                        history['Width2'] =
                                            width2.text == '' ? 0 : width2.text;
                                        history['Depth'] = depth.text;
                                        history['Depth2'] =
                                            depth2.text == '' ? 0 : depth2.text;
                                        history['Unit'] =
                                            buttons.isButtonClick1;
                                        MyDatabase().insertData(
                                            'Cement_Concrete', history);
                                      }
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(5)),
                                    ),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(right: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Calculate',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      isDataVisible = false;
                                      length.clear();
                                      length2.clear();
                                      width.clear();
                                      width2.clear();
                                      depth.clear();
                                      depth2.clear();
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(5))),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(left: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Reset',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                if (isDataVisible) getVolume(),
              ],
            ),
          ),
        ),
        floatingActionButton: isDataVisible
            ? FloatingActionButton(
                onPressed: () {
                  ScreenShort().captureScreenShort(screenshotController);
                },
                backgroundColor: AppColors.primaryColor,
                child: const Icon(Icons.share),
              )
            : Container(),
      ),
    );
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    length2 = length2.text.isEmpty ? TextEditingController(text: '0') : length2;
    width2 = width2.text.isEmpty ? TextEditingController(text: '0') : width2;
    depth2 = depth2.text.isEmpty ? TextEditingController(text: '0') : depth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    late double _wetVolumeFinal =
        ((finalVolumeMeter + (finalVolumeMeter * 52.4 / 100)) * fac).round() /
            fac;
    if (dropdownValue == 'M20 (1:1.5:3)') {
      if (buttons.isButtonClick1!) {
        setState(() {
          double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 12)) * fac).round() /
                  fac;
          finalVolumeMeter =
              (((_length * _breadth * _height) / 35.3147) * fac).round() / fac;
          finalVolumeFeet =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 5.5);
          late double _sandVolume = _wetVolumeFinal * (1.5 / 5.5);
          late double _aggVolume = _wetVolumeFinal * (3 / 5.5);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          late double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 100)) * fac)
                      .round() /
                  fac;
          finalVolumeFeet =
              (((_length * _breadth * _height) * 35.3147) * fac).round() / fac;
          finalVolumeMeter =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 5.5);
          late double _sandVolume = _wetVolumeFinal * (1.5 / 5.5);
          late double _aggVolume = _wetVolumeFinal * (3 / 5.5);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == 'M15 (1:2:4)') {
      if (buttons.isButtonClick1!) {
        setState(() {
          late double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 12)) * fac).round() /
                  fac;
          finalVolumeMeter =
              (((_length * _breadth * _height) / 35.3147) * fac).round() / fac;
          finalVolumeFeet =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 7);
          late double _sandVolume = _wetVolumeFinal * (2 / 7);
          late double _aggVolume = _wetVolumeFinal * (4 / 7);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          int decimals = 2;
          final num fac = pow(10, decimals);
          late double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 100)) * fac)
                      .round() /
                  fac;
          finalVolumeFeet =
              (((_length * _breadth * _height) * 35.3147) * fac).round() / fac;
          finalVolumeMeter =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 7);
          late double _sandVolume = _wetVolumeFinal * (2 / 7);
          late double _aggVolume = _wetVolumeFinal * (4 / 7);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == 'M10 (1:3:6)') {
      if (buttons.isButtonClick1!) {
        setState(() {
          int decimals = 2;
          final num fac = pow(10, decimals);
          late double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 12)) * fac).round() /
                  fac;
          finalVolumeMeter =
              (((_length * _breadth * _height) / 35.3147) * fac).round() / fac;
          finalVolumeFeet =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 10);
          late double _sandVolume = _wetVolumeFinal * (3 / 10);
          late double _aggVolume = _wetVolumeFinal * (6 / 10);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          int decimals = 2;
          final num fac = pow(10, decimals);
          late double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 100)) * fac)
                      .round() /
                  fac;
          finalVolumeFeet =
              (((_length * _breadth * _height) * 35.3147) * fac).round() / fac;
          finalVolumeMeter =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 10);
          late double _sandVolume = _wetVolumeFinal * (3 / 10);
          late double _aggVolume = _wetVolumeFinal * (6 / 10);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    } else {
      if (buttons.isButtonClick1!) {
        setState(() {
          int decimals = 2;
          final num fac = pow(10, decimals);
          late double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 12)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 12)) * fac).round() /
                  fac;
          finalVolumeMeter =
              (((_length * _breadth * _height) / 35.3147) * fac).round() / fac;
          finalVolumeFeet =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 13);
          late double _sandVolume = _wetVolumeFinal * (4 / 13);
          late double _aggVolume = _wetVolumeFinal * (8 / 13);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          int decimals = 2;
          final num fac = pow(10, decimals);
          late double _lengthFinal = double.parse(length2.text);
          late double _length =
              ((double.parse(length.text) + (_lengthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _breadthFinal = double.parse(width2.text);
          late double _breadth =
              ((double.parse(width.text) + (_breadthFinal / 100)) * fac)
                      .round() /
                  fac;
          late double _heightFinal = double.parse(depth2.text);
          late double _height =
              ((double.parse(depth.text) + (_heightFinal / 100)) * fac)
                      .round() /
                  fac;
          finalVolumeFeet =
              (((_length * _breadth * _height) * 35.3147) * fac).round() / fac;
          finalVolumeMeter =
              ((_length * _breadth * _height) * fac).round() / fac;
          late double _cementVolume = _wetVolumeFinal * (1 / 13);
          late double _sandVolume = _wetVolumeFinal * (4 / 13);
          late double _aggVolume = _wetVolumeFinal * (8 / 13);
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    }
    length2.text != '0' ? length2 = length2 : length2.clear();
    width2.text != '0' ? width2 = width2 : width2.clear();
    depth2.text != '0' ? depth2 = depth2 : depth2.clear();
  }

  Widget getVolume() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        children: [
          Card(
            elevation: 4,
            child: Column(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 2),
                    child: Text('Total Value of Cement Concrete',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.black)),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: Text(
                        '$finalVolumeMeter m',
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: fonts17,
                            color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        '3',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: fonts13,
                          color: Colors.red,
                          fontFeatures: const <FontFeature>[
                            FontFeature.superscripts(),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: Text(
                        ' or ',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: Text(
                        '$finalVolumeFeet ft',
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: fonts17,
                            color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        '3',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: fonts13,
                          color: Colors.red,
                          fontFeatures: const <FontFeature>[
                            FontFeature.superscripts(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        height: 40,
                        padding: const EdgeInsets.only(right: 70, top: 10),
                        decoration:
                            const BoxDecoration(color: AppColors.primaryColor),
                        child: Text('Material',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: fonts17,
                                color: Colors.white)),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 40,
                        padding: const EdgeInsets.only(left: 70, top: 10),
                        decoration:
                            const BoxDecoration(color: AppColors.primaryColor),
                        child: Text('Quantity',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: fonts17,
                                color: Colors.white)),
                      ),
                    )
                  ],
                ),
                GetMaterial(
                    'assets/icons/cement.png', 'Cement', '$_cementBag', 'Bags',
                    isDivider: true),
                GetMaterial('assets/icons/sand.png', 'Sand', '$_sandTon', 'Ton',
                    isDivider: true),
                GetMaterial('assets/icons/aggregate.png', 'Aggregate',
                    '$_aggTon', 'Ton',
                    isDivider: false),
                _cementBag != 0 ? SfCircularChart(
                  margin: EdgeInsets.zero,
                  series: <CircularSeries>[
                    PieSeries<ChartData, String>(
                      dataSource: chartData,
                      radius: '80',
                      pointColorMapper: (ChartData data, _) => data.color,
                      xValueMapper: (ChartData data, _) => data.x,
                      yValueMapper: (ChartData data, _) => data.y,
                      dataLabelMapper: (ChartData data, _) => data.x,
                      dataLabelSettings: const DataLabelSettings(
                        isVisible: true,
                        labelPosition: ChartDataLabelPosition.outside,
                      ),
                      enableTooltip: true,
                    )
                  ],
                ) : Container(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
