import 'dart:math';
import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/11_h_solar_rooftop_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class SolarRoofTopPage extends StatefulWidget {
  const SolarRoofTopPage({super.key});

  @override
  State<SolarRoofTopPage> createState() => _SolarRoofTopPageState();
}

class _SolarRoofTopPageState extends State<SolarRoofTopPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var solarUnits = TextEditingController();
  String dropdownValue = 'Monthly Unit Consumption';
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  late double _unit = 0;
  late double _dailyUnitConsumption = 0;
  late double _capacity = 0;
  late double _panel = 0;
  late double _area = 0;
  late double _areaFinal = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: Image.asset(
                'assets/icons/solarRoofTop.png',
                width: 45,
                height: 45,
                color: Colors.white,
              ),
            ),
            Expanded(
              child: Text(
                ' Solar Rooftop Calculator',
                style: TextStyle(fontSize: fonts17),
              ),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: ((context) {
                      return HistorySolarRoofTopPage();
                    }),
                  ),
                ).then(
                  (value) {
                    if (value != null) {
                      isDataVisible = false;
                      solarUnits.clear();
                      dropdownValue = 'Monthly Unit Consumption';

                      dropdownValue = value[0];
                      solarUnits = TextEditingController(text: value[1]);
                      _calculateUnit();
                    }
                  },
                );
              },
              child: const Icon(Icons.history),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Screenshot(
          controller: screenshotController,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 4,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: InputDecorator(
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Consumption Type'),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton2(
                              value: dropdownValue,
                              isExpanded: true,
                              isDense: true,
                              style:
                                  const TextStyle(color: Colors.black, fontSize: 15),
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownValue = newValue!;
                                });
                              },
                              items: <String>[
                                'Monthly Unit Consumption',
                                'Yearly Unit Consumption',
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              buttonHeight: 16,
                              itemHeight: 40,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: CustomInput(
                            label: 'Units',
                            controller: solarUnits,
                            errMessage: 'Enter Units',
                            formkey: formKey),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  setState(() {
                                    if (formKey.currentState!.validate()) {
                                      _calculateUnit();

                                      //add data in database
                                      Map<String, dynamic> history = {};
                                      history['ConsuptionType'] = dropdownValue;
                                      history['Units'] = solarUnits.text;
                                      MyDatabase()
                                          .insertData('Solar_RoofTop', history);
                                    }
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: AppColors.primaryColor,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(5)),
                                  ),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(right: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Calculate',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isDataVisible = false;
                                    solarUnits.clear();
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(5))),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(left: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Reset',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (isDataVisible) getCal(),
            ],
          ),
        ),
      ),
      floatingActionButton: isDataVisible
          ? FloatingActionButton(
              onPressed: () {
                ScreenShort().captureScreenShort(screenshotController);
              },
              backgroundColor: AppColors.primaryColor,
              child: const Icon(Icons.share),
            )
          : Container(),
    );
  }

  void _calculateUnit() {
    isDataVisible = true;
    int decimals = 2;
    final num fac = pow(10, decimals);
    dropdownValue == 'Monthly Unit Consumption'
        ? _unit = double.parse(solarUnits.text)
        : _unit = double.parse(solarUnits.text) / 12;

    setState(() {
      _dailyUnitConsumption = ((_unit / 30) * fac).round() / fac;
      _capacity = ((_dailyUnitConsumption / 4.5) * fac).round() / fac;
      _panel = (_capacity * 3).round().toDouble();
      //sq meter
      _area = ((_capacity * 95) * fac).round() / fac;
      //sq feet
      _areaFinal = ((_area / 10.764) * fac).round() / fac;
    });
  }

  Widget getCal() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            getRow('assets/icons/solar.png', "No of Solar Panels",
                "$_panel Panels", ''),
            Dividers(),
            getRow('assets/icons/meter.png', "Kw System Recommended",
                "$_capacity Kw", ''),
            Dividers(),
            getRow('assets/icons/unit.png', "Daily Unit Consumption",
                "$_dailyUnitConsumption units/day", ''),
            Dividers(),
            getRow('assets/icons/building.png', "Area For RoofTop",
                "$_area Sq ft", "$_areaFinal Sq m"),
          ],
        ),
      ),
    );
  }

  Widget getRow(img, text, calculation, [calculation2]) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            child: Center(
                child: Image.asset(img,
                    width: 70, height: 70, alignment: Alignment.center)),
          ),
          Expanded(
            child: Column(
              children: [
                Text(
                  text,
                  style: const TextStyle(fontWeight: FontWeight.w500),
                ),
                Text(
                  calculation,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts15,
                      color: Colors.red),
                ),
                Text(
                  calculation2,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts15,
                      color: Colors.red),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
