import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/06_h_concrete_block_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ConcreteBlockPage extends StatefulWidget {
  const ConcreteBlockPage({super.key});

  @override
  State<ConcreteBlockPage> createState() => _ConcreteBlockPageState();
}

class _ConcreteBlockPageState extends State<ConcreteBlockPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var concreteLength = TextEditingController();
  var concreteLength2 = TextEditingController();
  var concreteHeight = TextEditingController();
  var concreteHeight2 = TextEditingController();
  var blockLength = TextEditingController(text: '9');
  var blockWidth = TextEditingController(text: '4');
  var blockHeight = TextEditingController(text: '3');
  var otherPortion = TextEditingController(text: '1');

  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey8 = GlobalKey<FormState>();

  String dropdownValue = '23 CM Wall';
  String dropdownValue2 = 'C.M 1:6';

  late int noOfBrick = 0;
  late double cement = 0;
  late double sand = 0;
  late double brickMasonrym = 0;
  late double brickMasonryft = 0;

  String? length2Label;
  String? lengthLabel;
  String? height2Label;
  String? heightLabel;

  bool isChangeValue = true;
  List<ChartData> chartData = [];
  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    chartData = [
      ChartData('Cement\n$cement Bags', cement * 50, Colors.blue),
      ChartData('Sand\n$sand Ton', sand * 1000, Colors.red),
      ChartData(
          'No of Bricks\n$noOfBrick', noOfBrick*1, Colors.yellow),
    ];
    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String height2Label =
        buttons.isButtonClick2! ? 'Height/Depth (cm)' : 'Height/Depth (inch)';
    String heightLabel = buttons.isButtonClick1!
        ? 'Height/Depth (feet)'
        : 'Height/Depth (meter)';
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: ((context) {
              return const Home_page();
            }),
          ),
        );
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: AppBarColor().getAppBarColor(),
          titleSpacing: 0,
          title: Row(
            children: [
              Image.asset(
                'assets/icons/concreteBlock.png',
                width: 35,
                height: 35,
                color: Colors.white,
              ),
              Expanded(
                child: Text(
                  ' Concrete Block Calculator',
                  style: TextStyle(fontSize: fonts17),
                ),
              ),
            ],
          ),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15))),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: ((context) {
                        return HistoryConcreteBlockPage();
                      }),
                    ),
                  ).then(
                    (value) {
                      if (value != null) {
                        isDataVisible = false;
                        concreteLength.clear();
                        concreteLength2.clear();
                        concreteHeight.clear();
                        concreteHeight2.clear();
                        blockLength = TextEditingController(text: '19');
                        blockWidth = TextEditingController(text: '9');
                        blockHeight = TextEditingController(text: '9');
                        otherPortion.clear();
                        dropdownValue = '23 CM Wall';
                        dropdownValue2 = 'C.M 1:6';

                        concreteLength = TextEditingController(text: value[0]);
                        concreteLength2 = TextEditingController(text: value[1]);
                        concreteHeight = TextEditingController(text: value[2]);
                        concreteHeight2 = TextEditingController(text: value[3]);
                        dropdownValue = value[4];
                        dropdownValue2 = value[5];
                        blockLength = TextEditingController(text: value[6]);
                        blockWidth = TextEditingController(text: value[7]);
                        blockHeight = TextEditingController(text: value[8]);
                        buttons.isButtonClick1 = value[9] == '1' ? true : false;
                        buttons.isButtonClick2 = value[9] == '1' ? false : true;
                        _calculateVolume();
                      }
                    },
                  );
                },
                child: const Icon(Icons.history),
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Screenshot(
            controller: screenshotController,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Form(child: getVolumeType1()),
                      ),
                      Expanded(
                        child: Form(
                          child: getVolumeType2(),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                  child: Card(
                    elevation: 4,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                    label: lengthLabel,
                                    controller: concreteLength,
                                    errMessage: 'Enter $lengthLabel',
                                    formkey: formKey1),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                    label: length2Label,
                                    controller: concreteLength2,
                                    errMessage: 'Enter $length2Label',
                                    formkey: formKey2,
                                    validate: true),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                    label: heightLabel,
                                    controller: concreteHeight,
                                    errMessage: 'Enter $heightLabel',
                                    formkey: formKey3),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                    label: height2Label,
                                    controller: concreteHeight2,
                                    errMessage: 'Enter $height2Label',
                                    formkey: formKey4,
                                    validate: true),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: InputDecorator(
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Wall Thickness'),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  value: dropdownValue,
                                  isExpanded: true,
                                  isDense: true,
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 15),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValue = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    '23 CM Wall',
                                    '10 CM Wall',
                                    'Other Partition'
                                  ].map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  buttonHeight: 14,
                                  itemHeight: 40,
                                ),
                              ),
                            )),
                        dropdownValue == 'Other Partition'
                            ? Padding(
                                padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                                child: Row(
                                  children: [
                                    const Expanded(
                                      child: Text("Partition (cm)",
                                          textAlign: TextAlign.center),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Expanded(
                                        child: Form(
                                      key: formKey8,
                                      child: TextFormField(
                                        controller: otherPortion,
                                        cursorColor: AppColors.primaryColor,
                                        inputFormatters: [
                                          LengthLimitingTextInputFormatter(2),
                                        ],
                                        onChanged: (value) {
                                          setState(() {});
                                        },
                                        keyboardType: TextInputType.number,
                                        textInputAction: TextInputAction.next,
                                        decoration: const InputDecoration(
                                          labelText: 'Partition (cm)',
                                          labelStyle: TextStyle(color: Colors.black),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color:
                                                      AppColors.primaryColor)),
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                    ))
                                  ],
                                ),
                              )
                            : Container(),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: InputDecorator(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Ratio'),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                value: dropdownValue2,
                                isExpanded: true,
                                isDense: true,
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 15),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    dropdownValue2 = newValue!;
                                  });
                                },
                                items: <String>[
                                  'C.M 1:3',
                                  'C.M 1:4',
                                  'C.M 1:5',
                                  'C.M 1:6',
                                  'C.M 1:7',
                                  'C.M 1:8',
                                  'C.M 1:9'
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                buttonHeight: 14,
                                itemHeight: 40,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(
                              left: 13, bottom: 3, top: 8),
                          alignment: Alignment.centerLeft,
                          child: const Text(
                            'Size of Block',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomInput(
                                    label: 'Length (cm)',
                                    controller: blockLength,
                                    errMessage: 'Enter Length in cm',
                                    formkey: formKey5),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                    label: 'Width',
                                    controller: blockWidth,
                                    errMessage: 'Enter Length in cm',
                                    formkey: formKey6),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: CustomInput(
                                    label: 'Height',
                                    controller: blockHeight,
                                    errMessage: 'Enter Height in cm',
                                    formkey: formKey7),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                    setState(() {
                                      if (formKey1.currentState!.validate() &&
                                          formKey3.currentState!.validate() &&
                                          formKey2.currentState!.validate() &&
                                          formKey4.currentState!.validate() &&
                                          formKey5.currentState!.validate() &&
                                          formKey6.currentState!.validate() &&
                                          formKey7.currentState!.validate()) {
                                        _calculateVolume();

                                        //add data in database
                                        Map<String, dynamic> history = {};
                                        history['Length'] = concreteLength.text;
                                        history['Length2'] =
                                            concreteLength2.text == ''
                                                ? 0
                                                : concreteLength2.text;
                                        history['HeightDepth'] =
                                            concreteHeight.text;
                                        history['HeightDepth2'] =
                                            concreteHeight2.text == ''
                                                ? 0
                                                : concreteHeight2.text;
                                        history['WallThickness'] =
                                            dropdownValue;
                                        history['Ratio'] = dropdownValue2;
                                        history['BlockLength'] =
                                            blockLength.text;
                                        history['BlockWidth'] = blockWidth.text;
                                        history['otherPortion'] =
                                            blockHeight.text;
                                        history['Unit'] =
                                            buttons.isButtonClick1;
                                        MyDatabase().insertData(
                                            'Concrete_Block', history);
                                      }
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(5)),
                                    ),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(right: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Calculate',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      isDataVisible = false;
                                      concreteLength.clear();
                                      concreteLength2.clear();
                                      concreteHeight.clear();
                                      concreteHeight2.clear();
                                      otherPortion.clear();
                                      blockLength.clear();
                                      blockWidth.clear();
                                      blockHeight.clear();
                                      dropdownValue = '23 CM Wall';
                                      dropdownValue2 = 'C.M 1:6';
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(5))),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(left: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Reset',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                if (isDataVisible) getBlocks(),
              ],
            ),
          ),
        ),
        floatingActionButton: isDataVisible
            ? FloatingActionButton(
                onPressed: () {
                  ScreenShort().captureScreenShort(screenshotController);
                },
                backgroundColor: AppColors.primaryColor,
                child: const Icon(Icons.share),
              )
            : Container(),
      ),
    );
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    concreteLength2.text.isEmpty
        ? concreteLength2 = TextEditingController(text: '0')
        : concreteLength2;
    concreteHeight2.text.isEmpty
        ? concreteHeight2 = TextEditingController(text: '0')
        : concreteHeight2;

    double wallCal = 0.1;
    double cementCal = 1 / 4;
    double sandCal = 3 / 4;

    if (dropdownValue == '10 CM Wall') {
      wallCal = 0.1;
    } else if (dropdownValue == '23 CM Wall') {
      wallCal = 0.23;
    } else {
      wallCal = double.parse(otherPortion.text) * 0.01;
    }

    if (dropdownValue2 == 'C.M 1:3') {
      cementCal = 1 / 4;
      sandCal = 3 / 4;
    } else if (dropdownValue2 == 'C.M 1:4') {
      cementCal = 1 / 5;
      sandCal = 4 / 5;
    } else if (dropdownValue2 == 'C.M 1:5') {
      cementCal = 1 / 6;
      sandCal = 5 / 6;
    } else if (dropdownValue2 == 'C.M 1:6') {
      cementCal = 1 / 7;
      sandCal = 6 / 7;
    } else if (dropdownValue2 == 'C.M 1:7') {
      cementCal = 1 / 8;
      sandCal = 7 / 8;
    } else if (dropdownValue2 == 'C.M 1:8') {
      cementCal = 1 / 9;
      sandCal = 8 / 9;
    } else {
      cementCal = 1 / 10;
      sandCal = 9 / 10;
    }
    int decimals = 2;
    final num fac = pow(10, decimals);
    double length = buttons.isButtonClick1!
        ? (double.parse(concreteLength.text) * 0.3048) +
            (double.parse(concreteLength2.text) * 0.0254)
        : (double.parse(concreteLength.text)) +
            (double.parse(concreteLength2.text) * 0.01);
    double depth = buttons.isButtonClick1!
        ? (double.parse(concreteHeight.text) * 0.3048) +
            (double.parse(concreteHeight2.text) * 0.0254)
        : (double.parse(concreteHeight.text)) +
            (double.parse(concreteHeight2.text) * 0.01);

    double brickMasonry = length * depth * wallCal;
    brickMasonrym = (length * depth * wallCal * fac).round() / fac;
    brickMasonryft = (length * depth * wallCal * 35.3147 * fac).round() / fac;
    double brickWithMortar =
        ((double.parse(blockLength.text) * 0.0254 + 0.015) *
            (double.parse(blockWidth.text) * 0.0254 + 0.015) *
            (double.parse(blockHeight.text) * 0.0254 + 0.015));

    double noOfBrick2 = brickMasonry / brickWithMortar;
    noOfBrick = buttons.isButtonClick1!
        ? (brickMasonry / brickWithMortar).floor()
        : (brickMasonry / brickWithMortar).ceil();

    double volumeBrick = ((double.parse(blockLength.text) * 0.0254) *
        (double.parse(blockWidth.text) * 0.0254) *
        (double.parse(blockHeight.text) * 0.0254));

    double brickMortar = noOfBrick2 * volumeBrick;
    double mortar = brickMasonry - brickMortar;

    double mortar15 = mortar + (mortar * (15 / 100));
    double mortar25 = mortar15 + (mortar15 * (25 / 100));

    cement = ((cementCal * mortar25) / 0.035).ceilToDouble();
    double temp = ((sandCal * mortar25) * fac).round() / fac;
    sand = ((temp * 1550) / 1000 * fac).round() / fac;
    concreteLength2.text != '0'
        ? concreteLength2 = concreteLength2
        : concreteLength2.clear();
    concreteHeight2.text != '0'
        ? concreteHeight2 = concreteHeight2
        : concreteHeight2.clear();
  }

  Widget getBlocks() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Total Concrete Blocks Required',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts17)),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  '$noOfBrick',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: fonts17,
                      color: Colors.red),
                ),
              ),
            ),
            Dividers(),
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Volume',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts17)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$brickMasonrym m',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$brickMasonryft ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(right: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: Text('Material',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.white)),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(left: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: Text('Quantity',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.white)),
                  ),
                )
              ],
            ),
            GetMaterial('assets/icons/concreteBlock.png', 'Blocks',
                '$noOfBrick', 'Nos.',
                isDivider: true),
            GetMaterial('assets/icons/cement.png', 'Cement', '$cement', 'Bags',
                isDivider: true),
            GetMaterial('assets/icons/sand.png', 'Sand', '$sand', 'Ton',
                isDivider: false),
            cement != 0 ? SfCircularChart(
              margin: EdgeInsets.zero,
              series: <CircularSeries>[
                PieSeries<ChartData, String>(
                  dataSource: chartData,
                  radius: '80',
                  pointColorMapper: (ChartData data, _) => data.color,
                  xValueMapper: (ChartData data, _) => data.x,
                  yValueMapper: (ChartData data, _) => data.y,
                  dataLabelMapper: (ChartData data, _) => data.x,
                  dataLabelSettings: const DataLabelSettings(
                    isVisible: true,
                    labelPosition: ChartDataLabelPosition.outside,
                  ),
                  enableTooltip: true,
                )
              ],
            ) : Container(),
          ],
        ),
      ),
    );
  }
}
