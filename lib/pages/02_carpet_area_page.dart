import 'dart:math';

import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:screenshot/screenshot.dart';

class CarpetAreaPage extends StatefulWidget {
  const CarpetAreaPage({super.key});

  @override
  State<CarpetAreaPage> createState() => _CarpetAreaPageState();
}

class _CarpetAreaPageState extends State<CarpetAreaPage> {
  ScreenshotController screenshotController = ScreenshotController();

  bool isDataVisible = false;
  bool isCarpetSuperBuildupArea = false;

  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  final length = TextEditingController();
  var length2 = TextEditingController();
  final breadth = TextEditingController();
  var breadth2 = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey3 = GlobalKey<FormState>();

  dynamic _superBuiltupArea = 0;
  dynamic _builtupArea = 0;
  dynamic _carpetArea = 0;
  String? _carpetArea1;
  String? _carpetArea2;

  double _scarpetArea = 0;
  double _ssuperBuiltupArea = 0;
  double _sbuiltupArea = 0;

  String dropdownValue = 'Select Type';
  final items = [
    'Select Type',
    'Bedroom',
    'Living',
    'Balcony',
    'Dining',
    'Kitchen',
    'Passage',
    'Duct',
    'Garden',
    'Bathroom',
    'Lobby',
    'Lift',
    'Gym',
    'Swimming Pool',
    'Terrace',
    'Staircase'
  ];

  List<Map<String, dynamic>> myList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: Row(
          children: [
            Image.asset(
              'assets/icons/carpetArea.png',
              width: 35,
              height: 35,
              color: Colors.white,
            ),
            Expanded(
              child: Text(
                ' Carpet Area Calculator',
                style: TextStyle(fontSize: fonts17),
              ),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
      ),
      body: SingleChildScrollView(
        child: Screenshot(
          controller: screenshotController,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 4,
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                      child: InputDecorator(
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Name of Room/Passage',
                            labelStyle: TextStyle(fontSize: 15)),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            value: dropdownValue,
                            isDense: true,
                            style: const TextStyle(
                                color: Colors.black, fontSize: 15),
                            onChanged: (String? newValue) {
                              setState(() {
                                dropdownValue = newValue!;
                              });
                            },
                            items: items
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            buttonHeight: 18,
                            itemHeight: 40,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                      child: Row(
                        children: [
                          Expanded(
                            child: CustomInput(
                              label: 'Length (feet)',
                              controller: length,
                              errMessage: 'Enter Length in feet',
                              formkey: _formKey,
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Expanded(
                              child: Form(
                            key: _formKey1,
                            child: TextFormField(
                                controller: length2,
                                cursorColor: AppColors.primaryColor,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(7),
                                ],
                                onChanged: (value) {
                                  setState(() {});
                                },
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.next,
                                decoration: const InputDecoration(
                                  labelText: 'Length (inch)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.primaryColor)),
                                  border: OutlineInputBorder(),
                                  contentPadding: EdgeInsets.all(8),
                                ),
                                validator: (value) {
                                  if (value != '') {
                                    if (int.parse(value!) < 0 ||
                                        int.parse(value) > 11) {
                                      return 'Enter inch between 0 to 11';
                                    }
                                  }
                                  return null;
                                }),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                      child: Row(
                        children: [
                          Expanded(
                            child: CustomInput(
                              label: 'Breadth (feet)',
                              controller: breadth,
                              errMessage: 'Enter breadth in inch',
                              formkey: _formKey2,
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Expanded(
                              child: Form(
                            key: _formKey3,
                            child: TextFormField(
                                cursorColor: AppColors.primaryColor,
                                controller: breadth2,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(7),
                                ],
                                onChanged: (value) {
                                  setState(() {});
                                },
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.next,
                                decoration: const InputDecoration(
                                  labelText: 'Breadth (inch)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColors.primaryColor)),
                                  border: OutlineInputBorder(),
                                  contentPadding: EdgeInsets.all(8),
                                ),
                                validator: (value) {
                                  if (value != '') {
                                    if (int.parse(value!) < 0 ||
                                        int.parse(value) > 11) {
                                      return 'Enter inch between 0 to 11';
                                    }
                                  }
                                  return null;
                                }),
                          ))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                FocusManager.instance.primaryFocus?.unfocus();
                                setState(() {
                                  if (_formKey.currentState!.validate() &&
                                      _formKey1.currentState!.validate() &&
                                      _formKey3.currentState!.validate() &&
                                      _formKey2.currentState!.validate()) {
                                    _add();

                                    Map<String, dynamic> map = {};
                                    map['carpetArea1'] = _carpetArea1;
                                    map['carpetArea2'] = _carpetArea2;
                                    map['carpetArea'] = _carpetArea;
                                    map['builtupArea'] = _builtupArea;
                                    map['superBuiltupArea'] = _superBuiltupArea;
                                    map['isCarpetSuperBuildupArea'] =
                                        isCarpetSuperBuildupArea;
                                    map['area'] = dropdownValue;
                                    myList.add(map);
                                    length.clear();
                                    length2.clear();
                                    breadth.clear();
                                    breadth2.clear();
                                    dropdownValue = 'Select Type';
                                  }
                                });
                              },
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: AppColors.primaryColor,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(5),
                                      bottomRight: Radius.circular(5)),
                                ),
                                alignment: Alignment.center,
                                padding: const EdgeInsets.all(8),
                                child: Text(
                                  'Add',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: fonts15),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              if (isDataVisible) getCarperArea(),
              if (isDataVisible) getAreas(),
              if (isDataVisible) getCal(),
            ],
          ),
        ),
      ),
      floatingActionButton: isDataVisible
          ? FloatingActionButton(
              onPressed: () {
                ScreenShort().captureScreenShort(screenshotController);
              },
              backgroundColor: AppColors.primaryColor,
              child: const Icon(Icons.share),
            )
          : Container(),
    );
  }

  void _add() {
    isDataVisible = true;
    length2.text.isEmpty ? length2 = TextEditingController(text: '0') : length2;
    breadth2.text.isEmpty ? breadth2 = TextEditingController(text: '0') : breadth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    if ((length2.text == '0' || length2.text.isEmpty) &&
        (breadth2.text == '0' || breadth2.text.isEmpty)) {
      if (dropdownValue == 'Bedroom' ||
          dropdownValue == 'Living' ||
          dropdownValue == 'Dining' ||
          dropdownValue == 'Kitchen' ||
          dropdownValue == 'Bathroom' ||
          dropdownValue == 'Select Type') {
        setState(() {
          _carpetArea1 = length.text;
          _carpetArea2 = breadth.text;
          _carpetArea =
              (double.parse(length.text) * double.parse(breadth.text) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea;
          _scarpetArea += (_carpetArea * fac).round() / fac;
          _sbuiltupArea += (_builtupArea * fac).round() / fac;
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      } else if (dropdownValue == 'Balcony') {
        setState(() {
          _carpetArea1 = length.text;
          _carpetArea2 = breadth.text;
          _carpetArea =
              (double.parse(length.text) * double.parse(breadth.text) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea + _carpetArea;
          _scarpetArea += (_carpetArea * fac).round() / fac;
          _sbuiltupArea += (_builtupArea * fac).round() / fac;
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      } else {
        setState(() {
          isCarpetSuperBuildupArea = true;
          _carpetArea1 = length.text;
          _carpetArea2 = breadth.text;
          _carpetArea = 0;
          _builtupArea = _carpetArea + ((_carpetArea * 20) / 100);
          _superBuiltupArea =
              (double.parse(length.text) * double.parse(breadth.text));
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      }
    } else if ((length2.text == '0' || length2.text.isEmpty) &&
        breadth2.text.isNotEmpty) {
      if (dropdownValue == 'Bedroom' ||
          dropdownValue == 'Living' ||
          dropdownValue == 'Dining' ||
          dropdownValue == 'Kitchen' ||
          dropdownValue == 'Bathroom' ||
          dropdownValue == 'Select Type') {
        setState(() {
          int carpetArea2c = (int.parse(breadth2.text)* 83.5).floor();
          _carpetArea1 = length.text;
          _carpetArea2 = '${breadth.text}.$carpetArea2c';
          _carpetArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea;
          _scarpetArea += (_carpetArea * fac).round() / fac;
          _sbuiltupArea += (_builtupArea * fac).round() / fac;
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      } else if (dropdownValue == 'Balcony') {
        setState(() {
          int carpetArea2c = (int.parse(breadth2.text)* 83.5).floor();
          _carpetArea1 = length.text;
          _carpetArea2 = '${breadth.text}.$carpetArea2c';
          _carpetArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea + _carpetArea;
          _scarpetArea += (_carpetArea * fac).round() / fac;
          _sbuiltupArea += (_builtupArea * fac).round() / fac;
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      } else {
        setState(() {
          isCarpetSuperBuildupArea = true;
          _carpetArea = 0;
          int carpetArea2c = (int.parse(breadth2.text)* 83.5).floor();
          _carpetArea1 = length.text;
          _carpetArea2 = '${breadth.text}.$carpetArea2c';
          _builtupArea = _carpetArea + ((_carpetArea * 20) / 100);
          _superBuiltupArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      }
    } else if (length2.text.isNotEmpty &&
        (breadth2.text == '0' || breadth2.text.isEmpty)) {
      if (dropdownValue == 'Bedroom' ||
          dropdownValue == 'Living' ||
          dropdownValue == 'Dining' ||
          dropdownValue == 'Kitchen' ||
          dropdownValue == 'Bathroom' ||
          dropdownValue == 'Select Type') {
        setState(() {
          int carpetArea1c = (int.parse(length2.text)* 83.5).floor();
          _carpetArea1 = '${length.text}.$carpetArea1c';
          _carpetArea2 = breadth.text;
          _carpetArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea;
          _scarpetArea += (_carpetArea * fac).round() / fac;
          _sbuiltupArea += (_builtupArea * fac).round() / fac;
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      } else if (dropdownValue == 'Balcony') {
        setState(() {
          int carpetArea1c = (int.parse(length2.text)* 83.5).floor();
          _carpetArea1 = '${length.text}.$carpetArea1c';
          _carpetArea2 = breadth.text;
          _carpetArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea + _carpetArea;
          _scarpetArea += (_carpetArea * fac).round() / fac;
          _sbuiltupArea += (_builtupArea * fac).round() / fac;
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      } else {
        setState(() {
          isCarpetSuperBuildupArea = true;
          _carpetArea = 0;
          int carpetArea1c = (int.parse(length2.text)* 83.5).floor();
          _carpetArea1 = '${length.text}.$carpetArea1c';
          _carpetArea2 = breadth.text;
          _builtupArea = _carpetArea + ((_carpetArea * 20) / 100);
          _superBuiltupArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      }

    } else {
      isCarpetSuperBuildupArea = false;
      if (dropdownValue == 'Bedroom' ||
          dropdownValue == 'Living' ||
          dropdownValue == 'Dining' ||
          dropdownValue == 'Kitchen' ||
          dropdownValue == 'Bathroom' ||
          dropdownValue == 'Select Type') {
        setState(() {
          int carpetArea1c = (int.parse(length2.text) * 83.5).floor();
          int carpetArea2c = (int.parse(breadth2.text) * 83.5).floor();
          _carpetArea1 = '${length.text}.$carpetArea1c';
          _carpetArea2 = '${breadth.text}.$carpetArea2c';
          _carpetArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea;
          _scarpetArea = ((_scarpetArea +  _carpetArea) * fac).round() /
          fac;
          _sbuiltupArea = ((_sbuiltupArea +  _builtupArea) * fac).round() /
          fac;
          _ssuperBuiltupArea = ((_ssuperBuiltupArea + _superBuiltupArea) * fac).round() /
          fac;
        });
      } else if (dropdownValue == 'Balcony') {
        setState(() {
          int carpetArea1c = (int.parse(length2.text)* 83.5).floor();
          int carpetArea2c = (int.parse(breadth2.text)* 83.5).floor();
          _carpetArea1 = '${length.text}.$carpetArea1c';
          _carpetArea2 = '${breadth.text}.$carpetArea2c';
          _carpetArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _builtupArea =
              ((_carpetArea + ((_carpetArea * 20) / 100)) * fac).round() /
                  fac.toDouble();
          _superBuiltupArea = _builtupArea + _carpetArea;
          _scarpetArea += (_carpetArea * fac).round() / fac;
          _sbuiltupArea += (_builtupArea * fac).round() / fac;
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      } else {
        setState(() {
          isCarpetSuperBuildupArea = true;
          _carpetArea = 0;
          int carpetArea1c = (int.parse(length2.text)* 83.5).floor();
          int carpetArea2c = (int.parse(breadth2.text)* 83.5).floor();
          _carpetArea1 = '${length.text}.$carpetArea1c';
          _carpetArea2 = '${breadth.text}.$carpetArea2c';
          _builtupArea = _carpetArea + ((_carpetArea * 20) / 100);
          _superBuiltupArea =
              (double.parse(_carpetArea1!) * double.parse(_carpetArea2!) * fac)
                      .round() /
                  fac.toDouble();
          _ssuperBuiltupArea += (_superBuiltupArea * fac).round() / fac;
        });
      }
    }
    length2.text != '0' ? length2 = length2 : length2.clear();
    breadth2.text != '0' ? breadth2 = breadth2 : breadth2.clear();
  }

  Widget getCarperArea() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: getHeading('Carpet Area', Alignment.center, false),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 6.0),
              child: Text('${_scarpetArea.toStringAsFixed(2)} ft²',
                  style: TextStyle(
                      fontSize: fonts17,
                      color: Colors.red,
                      fontWeight: FontWeight.bold)),
            )
          ],
        ),
      ),
    );
  }

  Widget getAreas() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            child: Card(
              elevation: 4,
              child: Column(
                children: [
                  Center(
                    child: getHeading('Build-up Area', Alignment.center, false),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 3.0, bottom: 6.0),
                    child: Text('${_sbuiltupArea.toStringAsFixed(2)} ft²',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: fonts17,
                            color: Colors.red,
                            fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
              child: Card(
            elevation: 4,
            child: Column(
              children: [
                Center(
                  child: getHeading(
                      'Super Build-up Area', Alignment.center, false),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3.0, bottom: 6.0),
                  child: Text('${_ssuperBuiltupArea.toStringAsFixed(2)} ft²',
                      style: TextStyle(
                          fontSize: fonts17,
                          color: Colors.red,
                          fontWeight: FontWeight.bold)),
                )
              ],
            ),
          )),
        ],
      ),
    );
  }

  Widget getCal() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            getHeading('Area of Room / Passage', Alignment.centerLeft, true),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    '$index' != '0' ? Dividers() : Container(),
                    Padding(
                      padding: const EdgeInsets.only(top: 7.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(
                              myList[index]['area'],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: fonts17),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 8),
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          getAns('(', fonts15, FontWeight.w900, Colors.black),
                          getAns(double.parse(myList[index]['carpetArea1']).toStringAsFixed(2), fonts15,
                              FontWeight.w700, Colors.black),
                          getAns(
                              ' feet ', fonts13, FontWeight.w500, Colors.black),
                          getAns('X ', fonts15, FontWeight.w700, Colors.black),
                          getAns('${double.parse(myList[index]['carpetArea2']).toStringAsFixed(2)} ', fonts15,
                              FontWeight.w700, Colors.black),
                          getAns(
                              'feet', fonts13, FontWeight.w500, Colors.black),
                          getAns(')', fonts15, FontWeight.w900, Colors.black),
                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                getAns(
                                    '${myList[index]['isCarpetSuperBuildupArea'] ? myList[index]['superBuiltupArea'].toStringAsFixed(2) : myList[index]['carpetArea'].toStringAsFixed(2)} ',
                                    fonts15,
                                    FontWeight.w800,
                                    Colors.blue),
                                getAns('ft²', fonts13, FontWeight.w700,
                                    Colors.lightBlue),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0, 0, 5, 5),
                                  child: IconButton(
                                    icon: const Icon(
                                      Icons.remove_circle_outline_outlined,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      {
                                        _scarpetArea = ((_scarpetArea -
                                            myList[index]['carpetArea']) * 100).round() /
                                            100;
                                        _sbuiltupArea = ((_sbuiltupArea -
                                            myList[index]['builtupArea']) * 100).round() /
                                            100;
                                        _ssuperBuiltupArea = ((_ssuperBuiltupArea -
                                            myList[index]['superBuiltupArea']) * 100).round() /
                                      100;
                                        myList.remove(myList[index]);
                                        if (myList.length == 0)
                                          isDataVisible = false;
                                        setState(() {});
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
              itemCount: myList.length,
            ),
          ],
        ),
      ),
    );
  }

  Widget getAns(value, size, weight, color) {
    return Text(
      value,
      style: TextStyle(fontWeight: weight, fontSize: size, color: color),
    );
  }

  Widget getHeading(title, alignment, isRed) {
    return Container(
      padding: isRed ? const EdgeInsets.all(10) : const EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: isRed ? AppColors.primaryColor : Colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(5),
          topRight: Radius.circular(5),
        ),
      ),
      alignment: alignment,
      // height: 50,
      child: Text(
        title,
        style: TextStyle(
            fontSize: fonts15,
            color: isRed ? Colors.white : Colors.black,
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
