import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/08_h_flooring_cal_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class FlooringCalculationPage extends StatefulWidget {
  const FlooringCalculationPage({super.key});

  @override
  State<FlooringCalculationPage> createState() =>
      _FlooringCalculationPageState();
}

class _FlooringCalculationPageState extends State<FlooringCalculationPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var floorLength = TextEditingController();
  var floorLength2 = TextEditingController();
  var floorWidth = TextEditingController();
  var floorWidth2 = TextEditingController();
  var tileLength = TextEditingController(text: '2');
  var tileWidth = TextEditingController(text: '2');

  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();

  String? length2Label;
  String? lengthLabel;
  String? width2Label;
  String? widthLabel;
  String? tileWidthLabel;
  String? tileLengthLabel;

  late double finalAreaFeet = 0;
  late double finalAreaMeter = 0;
  late double finalTiles = 0;
  late double finalCement = 0;
  late double finalSand = 0;

  bool isChangeValue = true;
  List<ChartData> chartData = [];

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    chartData = [
      ChartData('Cement\n$finalCement Bags', finalCement * 50, Colors.blue),
      ChartData('Sand\n$finalSand Ton', finalSand * 1000, Colors.red),
    ];
    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String width2Label =
        buttons.isButtonClick2! ? 'Width (cm)' : 'Width (inch)';
    String widthLabel =
        buttons.isButtonClick1! ? 'Width (feet)' : 'Width (meter)';
    String tileWidthLabel =
        buttons.isButtonClick2! ? 'Width (feet)' : 'Width (feet)';
    String tileLengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (feet)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/flooringCalculator.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Flooring Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryFlooringCalculationPage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          floorLength.clear();
                          floorLength2.clear();
                          floorWidth.clear();
                          floorWidth2.clear();
                          tileLength = TextEditingController(text: '2');
                          tileWidth = TextEditingController(text: '2');

                          floorLength = TextEditingController(text: value[0]);
                          floorLength2 = TextEditingController(text: value[1]);
                          floorWidth = TextEditingController(text: value[2]);
                          floorWidth2 = TextEditingController(text: value[3]);
                          tileLength = TextEditingController(text: value[4]);
                          tileWidth = TextEditingController(text: value[5]);
                          buttons.isButtonClick1 =
                              value[6] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[6] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: lengthLabel,
                                      controller: floorLength,
                                      errMessage: 'Enter $lengthLabel',
                                      formkey: formKey1),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                    label: length2Label,
                                    controller: floorLength2,
                                    errMessage: 'Enter $length2Label',
                                    formkey: formKey2,
                                    validate: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: widthLabel,
                                      controller: floorWidth,
                                      errMessage: 'Enter $widthLabel',
                                      formkey: formKey3),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: width2Label,
                                      controller: floorWidth2,
                                      errMessage: 'Enter $width2Label',
                                      formkey: formKey4,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Container(
                              padding: const EdgeInsets.only(
                                  left: 22, bottom: 3, top: 7),
                              alignment: Alignment.centerLeft,
                              child: const Text(
                                'Tile Dimension',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: tileLengthLabel,
                                      controller: tileLength,
                                      errMessage: 'Enter $tileLengthLabel',
                                      formkey: formKey5),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: tileWidthLabel,
                                      controller: tileWidth,
                                      errMessage: 'Enter $tileWidthLabel',
                                      formkey: formKey6),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey1.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey4.currentState!.validate() &&
                                            formKey5.currentState!.validate() &&
                                            formKey6.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['Length'] = floorLength.text;
                                          history['Length2'] =
                                              floorLength2.text == ''
                                                  ? 0
                                                  : floorLength2.text;
                                          history['Width'] = floorWidth.text;
                                          history['Width2'] =
                                              floorWidth2.text == ''
                                                  ? 0
                                                  : floorWidth2.text;
                                          history['DimensionLength'] =
                                              tileLength.text;
                                          history['DimensionWidth'] =
                                              tileWidth.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'Flooring_Cal', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        floorLength.clear();
                                        floorLength2.clear();
                                        floorWidth.clear();
                                        floorWidth2.clear();
                                        tileLength.clear();
                                        tileWidth.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) totalArea(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    floorLength2 = floorLength2.text.isEmpty
        ? TextEditingController(text: '0')
        : floorLength2;
    floorWidth2 = floorWidth2.text.isEmpty
        ? TextEditingController(text: '0')
        : floorWidth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    late double _lengthFinally = double.parse(floorLength.text);
    late double _lengthFinal = double.parse(floorLength2.text);
    ((_lengthFinally + (_lengthFinal / 100)) * fac).round() / fac;
    late double _tileLength = double.parse(tileLength.text);
    late double _tileWidth = double.parse(tileWidth.text);
    late double _tileArea = ((_tileLength * _tileWidth) * fac).round() / fac;
    late double _volume = ((finalAreaMeter * 0.07) * fac).round() / fac;
    double length = double.parse('${floorLength.text}.${floorLength2.text}');
    double width = double.parse('${floorWidth.text}.${floorWidth2.text}');
    if (buttons.isButtonClick1!) {
      setState(() {
        finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
        finalAreaFeet = ((length * width) * fac).round() / fac;
        finalTiles = (finalAreaFeet / _tileArea).round().toDouble();
        finalCement = (((_volume * 1) / 7) / 0.035).ceil().round().toDouble();
        finalSand =
            ((((((_volume) * 6) / 7) * 1550) / 1000) * fac).round() / fac;
      });
    } else {
      setState(() {
        finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
        finalAreaMeter = ((length * width) * fac).round() / fac;
        finalTiles = (finalAreaFeet / _tileArea).round().toDouble();
        finalCement = (((_volume * 1) / 7) / 0.035).ceil().round().toDouble();
        finalSand =
            ((((((_volume) * 6) / 7) * 1550) / 1000) * fac).round() / fac;
      });
    }
    floorLength2.text != '0'
        ? floorLength2 = floorLength2
        : floorLength2.clear();
    floorWidth2.text != '0' ? floorWidth2 = floorWidth2 : floorWidth2.clear();
  }

  Widget totalArea() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Total Area',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts17)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding: const EdgeInsets.only(top: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Container(
                      height: 30,
                      padding: const EdgeInsets.only(right: 70, top: 5),
                      decoration:
                          const BoxDecoration(color: AppColors.primaryColor),
                      child: Text('Material',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: fonts17,
                              color: Colors.white)),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 30,
                      padding: const EdgeInsets.only(left: 70, top: 5),
                      decoration:
                          const BoxDecoration(color: AppColors.primaryColor),
                      child: Text('Quantity',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: fonts17,
                              color: Colors.white)),
                    ),
                  )
                ],
              ),
            ),
            GetMaterial('assets/icons/tiles.png', 'Tiles', '$finalTiles', 'No.',
                isDivider: true),
            GetMaterial(
                'assets/icons/cement.png', 'Cement', '$finalCement', 'Bags',
                isDivider: true),
            GetMaterial('assets/icons/sand.png', 'Sand', '$finalSand', 'Ton',
                isDivider: false),
            finalCement != 0
                ? SfCircularChart(
                    margin: EdgeInsets.zero,
                    series: <CircularSeries>[
                      PieSeries<ChartData, String>(
                        dataSource: chartData,
                        radius: '80',
                        pointColorMapper: (ChartData data, _) => data.color,
                        xValueMapper: (ChartData data, _) => data.x,
                        yValueMapper: (ChartData data, _) => data.y,
                        dataLabelMapper: (ChartData data, _) => data.x,
                        dataLabelSettings: const DataLabelSettings(
                          isVisible: true,
                          labelPosition: ChartDataLabelPosition.outside,
                        ),
                        enableTooltip: true,
                      )
                    ],
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
