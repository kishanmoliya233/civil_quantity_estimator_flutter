import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/13_h_paint_work_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class PaintWorkPage extends StatefulWidget {
  const PaintWorkPage({super.key});

  @override
  State<PaintWorkPage> createState() => _PaintWorkPageState();
}

class _PaintWorkPageState extends State<PaintWorkPage> {
  bool isDataVisible = false;
  bool isError = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var carpetArea = TextEditingController();
  var doorHeight = TextEditingController();
  var doorHeight2 = TextEditingController();
  var noOfDoorsPaint = TextEditingController();
  var windowWidth = TextEditingController();
  var windowWidth2 = TextEditingController();
  var noOfWindowsPaint = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();

  late double finalAreaFeet = 0;
  late double finalAreaMeter = 0;
  late double _paint = 0;
  late double _primer = 0;
  late double _putty = 0;

  String? doorHeightLabel;
  String? carperAreaLabel;
  String? windowHeightLabel;
  String? doorHeight2Label;
  String? windowHeight2Label;

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String doorHeightLabel =
        buttons.isButtonClick2! ? 'Door Width (meter)' : 'Door Width (feet)';
    String carperAreaLabel = buttons.isButtonClick1!
        ? 'Carpet Area (Sq. meter)'
        : 'Carpet Area (Sq. feet)';
    String windowHeightLabel = buttons.isButtonClick2!
        ? 'Window Width (meter)'
        : 'Window Width (feet)';
    String doorHeight2Label =
        buttons.isButtonClick1! ? 'Door Height (meter)' : 'Door Height (feet)';
    String windowHeight2Label = buttons.isButtonClick1!
        ? 'Window Height (meter)'
        : 'Window Height (feet)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/paintWork.png',
                  width: 40,
                  height: 40,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Paint Work Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryPaintWorkPage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          carpetArea.clear();
                          doorHeight.clear();
                          doorHeight2.clear();
                          noOfDoorsPaint.clear();
                          windowWidth.clear();
                          windowWidth2.clear();
                          noOfWindowsPaint.clear();

                          carpetArea = TextEditingController(text: value[0]);
                          doorHeight = TextEditingController(text: value[1]);
                          doorHeight2 = TextEditingController(text: value[2]);
                          noOfDoorsPaint =
                              TextEditingController(text: value[3]);
                          windowWidth = TextEditingController(text: value[4]);
                          windowWidth2 = TextEditingController(text: value[5]);
                          noOfWindowsPaint =
                              TextEditingController(text: value[6]);
                          buttons.isButtonClick1 =
                              value[7] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[7] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          isError ? getError() : Container(),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: CustomInput(
                              label: carperAreaLabel,
                              controller: carpetArea,
                              errMessage: 'Enter $carperAreaLabel',
                              formkey: formKey,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: doorHeightLabel,
                                    controller: doorHeight,
                                    errMessage: 'Enter $doorHeightLabel',
                                    formkey: formKey1,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                    label: doorHeight2Label,
                                    controller: doorHeight2,
                                    errMessage: 'Enter $doorHeight2Label',
                                    formkey: formKey2,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: CustomInput(
                              label: 'No. of Doors',
                              controller: noOfDoorsPaint,
                              errMessage: 'Enter No. of Doors',
                              formkey: formKey3,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: windowHeightLabel,
                                    controller: windowWidth,
                                    errMessage: 'Enter $windowHeightLabel',
                                    formkey: formKey4,
                                    // validate: _validate,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                    label: windowHeight2Label,
                                    controller: windowWidth2,
                                    errMessage: 'Enter $windowHeight2Label',
                                    formkey: formKey5,
                                    // validate: _validate,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: CustomInput(
                              label: 'No. of Windows',
                              controller: noOfWindowsPaint,
                              errMessage: 'Enter No. of Windows',
                              formkey: formKey6,
                              // validate: _validate,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey.currentState!.validate() &&
                                            formKey1.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey4.currentState!.validate() &&
                                            formKey5.currentState!.validate() &&
                                            formKey6.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['CarpetArea'] =
                                              carpetArea.text;
                                          history['DoorWidth'] =
                                              doorHeight.text;
                                          history['DoorHeight'] =
                                              doorHeight2.text;
                                          history['NoOfDoors'] =
                                              noOfDoorsPaint.text;
                                          history['WindowWidth'] =
                                              windowWidth.text;
                                          history['WindowHeight'] =
                                              windowWidth2.text;
                                          history['NoOfWindows'] =
                                              noOfWindowsPaint.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'Paint_Work', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        carpetArea.clear();
                                        doorHeight.clear();
                                        doorHeight2.clear();
                                        noOfDoorsPaint.clear();
                                        windowWidth.clear();
                                        windowWidth2.clear();
                                        noOfWindowsPaint.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getPaintArea(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    int decimals = 2;
    final num fac = pow(10, decimals);
    late double _carpetArea =
        ((double.parse(carpetArea.text)) * fac).round() / fac;
    late double _width = ((double.parse(windowWidth.text)) * fac).round() / fac;
    late double _width1 = ((double.parse(doorHeight.text)) * fac).round() / fac;
    late double _height =
        ((double.parse(windowWidth2.text)) * fac).round() / fac;
    late double _height1 =
        ((double.parse(doorHeight2.text)) * fac).round() / fac;
    late double _door =
        ((double.parse(noOfWindowsPaint.text)) * fac).round() / fac;
    late double _window =
        ((double.parse(noOfDoorsPaint.text)) * fac).round() / fac;
    late double _paintArea = _carpetArea * 3.5;
    late double _windowArea = ((_width * _height * _door) * fac).round() / fac;
    late double _doorArea =
        ((_width1 * _height1 * _window) * fac).round() / fac;
    if (_windowArea + _doorArea > _carpetArea) {
      isError = true;
    } else {
      isError = false;
    }
    if (buttons.isButtonClick1!) {
      setState(() {
        finalAreaFeet =
            ((_paintArea - _doorArea - _windowArea) * fac).round() / fac;
        finalAreaMeter = ((((((_paintArea) - _doorArea) - _windowArea)) *
                        (0.3048 * 0.3048)) *
                    fac)
                .round() /
            fac;
        _paint = ((finalAreaFeet / 100) * fac).round() / fac;
        _primer = ((finalAreaFeet / 100) * fac).round() / fac;
        _putty = ((finalAreaFeet / 40) * fac).round() / fac;
      });
    } else {
      setState(() {
        finalAreaFeet =
            (((_paintArea - _doorArea - _windowArea) * (3.28084 * 3.28084)) *
                        fac)
                    .round() /
                fac;
        finalAreaMeter =
            ((((_paintArea) - _doorArea) - _windowArea) * fac).round() / fac;
        _paint = ((finalAreaFeet / 100) * fac).round() / fac;
        _primer = ((finalAreaFeet / 100) * fac).round() / fac;
        _putty = ((finalAreaFeet / 40) * fac).round() / fac;
      });
    }
  }

  Widget getPaintArea() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Total Paint Area',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: fonts17)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 11),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(right: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: Text('Material',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.white)),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(left: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: Text('Quantity',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.white)),
                  ),
                )
              ],
            ),
            GetMaterial('assets/icons/paint.png', 'Paint', '$_paint', 'Liters',
                isDivider: true),
            GetMaterial(
                'assets/icons/primer.png', 'Primer', '$_primer', 'Liters',
                isDivider: true),
            GetMaterial('assets/icons/putty.png', 'Putty', '$_putty', 'Kgs',
                isDivider: false),
          ],
        ),
      ),
    );
  }

  Widget getError() {
    return Container(
      child: Center(
        child: Column(
          children: [
            Text(
              'Please Enter Doors And Windows Area',
              style: TextStyle(color: Colors.red, fontSize: fonts13),
            ),
            Text(
              'Less Than Carpet Area',
              style: TextStyle(color: Colors.red, fontSize: fonts13),
            ),
          ],
        ),
      ),
      margin: const EdgeInsets.only(bottom: 12),
    );
  }
}
