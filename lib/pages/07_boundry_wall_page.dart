import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/07_h_boundry_wall_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class BoundryWallPage extends StatefulWidget {
  const BoundryWallPage({super.key});

  @override
  State<BoundryWallPage> createState() => _BoundryWallPageState();
}

class _BoundryWallPageState extends State<BoundryWallPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var areaLength = TextEditingController();
  var areaLength2 = TextEditingController();
  var areaHeight = TextEditingController();
  var areaHeight2 = TextEditingController();
  var barLength = TextEditingController();
  var barLength2 = TextEditingController();
  var barHeight = TextEditingController();
  var barHeight2 = TextEditingController();

  late double finalHorizontalBar = 0;
  late double finalVerticalPost = 0;
  late double finalHorizontalBar2 = 0;
  late double finalVerticalPost2 = 0;

  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey8 = GlobalKey<FormState>();

  String? areaLength2Label;
  String? areaLengthLabel1;
  String? areaHeight2Label;
  String? areaHeightLabel;
  String? barLength2Label;
  String? barLengthLabel;
  String? barHeight2Label;
  String? barHeightLabel;

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String areaLength2Label = buttons.isButtonClick2!
        ? 'Length of Area (cm)'
        : 'Length of Area (inch)';
    String areaLengthLabel1 = buttons.isButtonClick1!
        ? 'Length of Area (feet)'
        : 'Length of Area (meter)';
    String areaHeight2Label = buttons.isButtonClick2!
        ? 'Height of Area (cm)'
        : 'Height of Area (inch)';
    String areaHeightLabel = buttons.isButtonClick1!
        ? 'Height of Area (feet)'
        : 'Height of Area (meter)';
    String barLength2Label =
        buttons.isButtonClick2! ? 'Length of Bar (cm)' : 'Length of Bar (inch)';
    String barLengthLabel = buttons.isButtonClick1!
        ? 'Length of Bar (feet)'
        : 'Length of Bar (meter)';
    String barHeight2Label =
        buttons.isButtonClick2! ? 'Height of Bar (cm)' : 'Height of Bar (inch)';
    String barHeightLabel = buttons.isButtonClick1!
        ? 'Height of Bar (feet)'
        : 'Height of Bar (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/boundryWall.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Boundry Wall Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryBoundryWallPage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          areaLength.clear();
                          areaLength2.clear();
                          areaHeight.clear();
                          areaHeight2.clear();
                          barLength.clear();
                          barLength2.clear();
                          barHeight.clear();
                          barHeight2.clear();

                          areaLength = TextEditingController(text: value[0]);
                          areaLength2 = TextEditingController(text: value[1]);
                          areaHeight = TextEditingController(text: value[2]);
                          areaHeight2 = TextEditingController(text: value[3]);
                          barLength = TextEditingController(text: value[4]);
                          barLength2 = TextEditingController(text: value[5]);
                          barHeight = TextEditingController(text: value[6]);
                          barHeight2 = TextEditingController(text: value[7]);
                          buttons.isButtonClick1 =
                              value[8] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[8] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Container(
                              padding: const EdgeInsets.only(
                                  left: 10, bottom: 3, top: 10),
                              alignment: Alignment.center,
                              child: const Text(
                                'Area to be Covered By Precast Wall',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: areaLengthLabel1,
                                      controller: areaLength,
                                      errMessage: 'Enter $areaLengthLabel1',
                                      formkey: formKey1),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: areaLength2Label,
                                      controller: areaLength2,
                                      errMessage: 'Enter $areaLength2Label',
                                      formkey: formKey2,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: areaHeightLabel,
                                      controller: areaHeight,
                                      errMessage: 'Enter $areaHeightLabel',
                                      formkey: formKey3),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: areaHeight2Label,
                                      controller: areaHeight2,
                                      errMessage: 'Enter $areaHeight2Label',
                                      formkey: formKey4,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Container(
                              padding: const EdgeInsets.only(
                                  left: 10, bottom: 3, top: 10),
                              alignment: Alignment.center,
                              child: const Text(
                                'Dimension of Horizontal Bar',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: barLengthLabel,
                                      controller: barLength,
                                      errMessage: 'Enter $barLengthLabel',
                                      formkey: formKey5),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: barLength2Label,
                                      controller: barLength2,
                                      errMessage: 'Enter $barLength2Label',
                                      formkey: formKey6,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: barHeightLabel,
                                      controller: barHeight,
                                      errMessage: 'Enter $barHeightLabel',
                                      formkey: formKey7),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: barHeight2Label,
                                      controller: barHeight2,
                                      errMessage: 'Enter $barHeight2Label',
                                      formkey: formKey8,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey1.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey4.currentState!.validate() &&
                                            formKey5.currentState!.validate() &&
                                            formKey6.currentState!.validate() &&
                                            formKey7.currentState!.validate() &&
                                            formKey8.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['LengthArea'] =
                                              areaLength.text;
                                          history['LengthArea2'] =
                                              areaLength2.text == ''
                                                  ? 0
                                                  : areaLength2.text;
                                          history['HeightArea'] =
                                              areaHeight.text;
                                          history['HeightArea2'] =
                                              areaHeight2.text == ''
                                                  ? 0
                                                  : areaHeight2.text;
                                          history['LengthBar'] = barLength.text;
                                          history['LengthBar2'] =
                                              barLength2.text == ''
                                                  ? 0
                                                  : barLength2.text;
                                          history['HeightBar'] = barHeight.text;
                                          history['HeightBar2'] =
                                              barHeight2.text == ''
                                                  ? 0
                                                  : barHeight2.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'Boundry_Wall', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        areaLength.clear();
                                        areaLength2.clear();
                                        areaHeight.clear();
                                        areaHeight2.clear();
                                        barLength.clear();
                                        barLength2.clear();
                                        barHeight.clear();
                                        barHeight2.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getAns(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        )
    );
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    areaLength2.text.isEmpty
        ? areaLength2 = TextEditingController(text: '0')
        : areaLength2;
    areaHeight2.text.isEmpty
        ? areaHeight2 = TextEditingController(text: '0')
        : areaHeight2;
    barLength2.text.isEmpty
        ? barLength2 = TextEditingController(text: '0')
        : barLength2;
    barHeight2.text.isEmpty
        ? barHeight2 = TextEditingController(text: '0')
        : barHeight2;
    double laValue = double.parse('${areaLength.text}.${areaLength2.text}');
    double haValue = double.parse('${areaHeight.text}.${areaHeight2.text}');
    double lbValue = double.parse('${barLength.text}.${barLength2.text}');
    double hbValue = double.parse('${barHeight.text}.${barHeight2.text}');
    double hb = laValue - (laValue * 0.50 / lbValue);
    double hb2 = laValue - (laValue * 0.15 / lbValue);
    if (buttons.isButtonClick1!) {
      setState(() {
        finalHorizontalBar =
            ((laValue * haValue) / (lbValue * hbValue)).ceilToDouble();
        finalVerticalPost = ((laValue / lbValue) + 1).ceil().round().toDouble();
        finalHorizontalBar2 =
            ((hb * haValue) / (lbValue * hbValue)).ceilToDouble();
        finalVerticalPost2 =
            ((laValue / lbValue) + 1).ceil().round().toDouble();
      });
    } else {
      setState(() {
        finalHorizontalBar =
            ((laValue * haValue) / (lbValue * hbValue)).ceilToDouble();
        finalVerticalPost = ((laValue / lbValue) + 1).ceil().round().toDouble();
        finalHorizontalBar2 =
            ((hb2 * haValue) / (lbValue * hbValue)).ceilToDouble();
        finalVerticalPost2 =
            ((laValue / lbValue) + 1).ceil().round().toDouble();
      });
    }
    areaLength2.text != '0' ? areaLength2 = areaLength2 : areaLength2.clear();
    areaHeight2.text != '0' ? areaHeight2 = areaHeight2 : areaHeight2.clear();
    barLength2.text != '0' ? barLength2 = barLength2 : barLength2.clear();
    barHeight2.text != '0' ? barHeight2 = barHeight2 : barHeight2.clear();
  }

  Widget getAns() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Container(
                padding: const EdgeInsets.only(left: 5, bottom: 3, top: 10),
                alignment: Alignment.center,
                child: const Text(
                  'Quantity of Horizontal bar and Vertical bar',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                )),
            Container(
                padding: const EdgeInsets.only(left: 5, bottom: 3, top: 10),
                alignment: Alignment.center,
                child: const Text(
                  'Note:  Space  is  kept  between  horizontal  bars',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black54),
                )),
            getHeading(),
            getParts('1', 'Horizontal Bar', '$finalHorizontalBar'),
            Dividers(),
            getParts('2', 'Vertical Post', '$finalVerticalPost'),
            Dividers(),
            Container(
                padding: const EdgeInsets.only(left: 5, bottom: 3, top: 10),
                alignment: Alignment.center,
                child: const Text(
                  'Note:  No  Space  is  kept  between  horizontal  bars',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black54),
                )),
            getHeading(),
            getParts('1', 'Horizontal Bar', '$finalHorizontalBar2'),
            Dividers(),
            getParts('2', 'Vertical Post', '$finalVerticalPost2'),
          ],
        ),
      ),
    );
  }

  Widget getHeading() {
    return Container(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              height: 30,
              padding: const EdgeInsets.only(right: 70, top: 5),
              decoration: const BoxDecoration(color: AppColors.primaryColor),
              child: const Text('Material',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      color: Colors.white)),
            ),
          ),
          Expanded(
            child: Container(
              height: 30,
              padding: const EdgeInsets.only(left: 70, top: 5),
              decoration: const BoxDecoration(color: AppColors.primaryColor),
              child: const Text('Quantity',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      color: Colors.white)),
            ),
          )
        ],
      ),
    );
  }

  Widget getParts(no, direction, ans) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Container(
              margin: const EdgeInsets.all(10),
              child: Center(
                child: Text(no,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts15)),
              ),
            ),
            Center(
              child: Text(direction,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: fonts15)),
            ),
          ],
        ),
        Row(
          children: [
            Center(
              child: Text(ans,
                  style: TextStyle(
                      fontWeight: FontWeight.w900, fontSize: fonts15)),
            ),
            Center(
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Text('No.', style: TextStyle(fontSize: fonts15)),
              ),
            ),
          ],
        )
      ],
    );
  }
}
