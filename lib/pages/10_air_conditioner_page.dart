import 'dart:math';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/10_h_air_conditioner_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class AirConditionerPage extends StatefulWidget {
  const AirConditionerPage({super.key});

  @override
  State<AirConditionerPage> createState() => _AirConditionerPageState();
}

class _AirConditionerPageState extends State<AirConditionerPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var length = TextEditingController();
  var length2 = TextEditingController();
  var breadth = TextEditingController();
  var breadth2 = TextEditingController();
  var height = TextEditingController();
  var height2 = TextEditingController();
  var noOfPerson = TextEditingController();
  var temperature = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey8 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey9 = GlobalKey<FormState>();
  late double finalSizeTon = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: Row(
          children: [
            Image.asset(
              'assets/icons/airConditioner.png',
              width: 40,
              height: 40,
              color: Colors.white,
            ),
            Expanded(
              child: Text(
                ' Air Conditioner Calculator',
                style: TextStyle(fontSize: fonts17),
              ),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: ((context) {
                      return HistoryAirConditionerPage();
                    }),
                  ),
                ).then(
                  (value) {
                    if (value != null) {
                      isDataVisible = false;
                      length.clear();
                      length2.clear();
                      breadth.clear();
                      breadth2.clear();
                      height.clear();
                      height2.clear();
                      noOfPerson.clear();
                      temperature.clear();

                      length = TextEditingController(text: value[0]);
                      length2 = TextEditingController(text: value[1]);
                      breadth = TextEditingController(text: value[2]);
                      breadth2 = TextEditingController(text: value[3]);
                      height = TextEditingController(text: value[4]);
                      height2 = TextEditingController(text: value[5]);
                      noOfPerson = TextEditingController(text: value[6]);
                      temperature = TextEditingController(text: value[7]);
                      _calculateEstimate();
                    }
                  },
                );
              },
              child: const Icon(Icons.history),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Screenshot(
          controller: screenshotController,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: Card(
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(7),
                          bottomRight: Radius.circular(7))),
                  elevation: 4,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                label: 'Length (feet)',
                                controller: length,
                                errMessage: 'Enter Length in feet',
                                formkey: formKey,
                              ),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: CustomInput(
                                  label: 'Length (inch)',
                                  controller: length2,
                                  errMessage: 'Enter Length in inch',
                                  formkey: formKey1,
                                  validate: true),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                label: 'Breadth (feet)',
                                controller: breadth,
                                errMessage: 'Enter Breadth in feet',
                                formkey: formKey2,
                              ),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: CustomInput(
                                  label: 'Breadth (inch)',
                                  controller: breadth2,
                                  errMessage: 'Enter Breadth in inch',
                                  formkey: formKey3,
                                  validate: true),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                label: 'Height (feet)',
                                controller: height,
                                errMessage: 'Enter Height in feet',
                              ),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: CustomInput(
                                  label: 'Height (inch)',
                                  controller: height2,
                                  errMessage: 'Enter Height in inch',
                                  formkey: formKey4,
                                  validate: true),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: CustomInput(
                          label: 'No of Person',
                          controller: noOfPerson,
                          errMessage: 'Enter No of Person',
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CustomInput(
                          label: 'Max Temperature in Celsius',
                          controller: temperature,
                          errMessage: 'Enter Max Temperature in Celsius',
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                FocusManager.instance.primaryFocus?.unfocus();
                                setState(() {
                                  if (formKey.currentState!.validate() &&
                                      formKey1.currentState!.validate() &&
                                      formKey3.currentState!.validate() &&
                                      formKey4.currentState!.validate() &&
                                      formKey2.currentState!.validate()) {
                                    _calculateEstimate();

                                    //add data in database
                                    Map<String, dynamic> history = {};
                                    history['Length'] = length.text;
                                    history['Length2'] =
                                        length2.text == '' ? 0 : length2.text;
                                    history['Breadth'] = breadth.text;
                                    history['Breadth2'] =
                                        breadth2.text == '' ? 0 : breadth2.text;
                                    history['Height'] =
                                        height.text == '' ? 0 : height.text;
                                    history['Height2'] =
                                        height2.text == '' ? 0 : height2.text;
                                    history['NoOfPerson'] =
                                        noOfPerson.text == ''
                                            ? 0
                                            : noOfPerson.text;
                                    history['MaxTemp'] = temperature.text == ''
                                        ? 0
                                        : temperature.text;
                                    MyDatabase()
                                        .insertData('Air_Conditioner', history);
                                  }
                                });
                              },
                              child: Container(
                                decoration: const BoxDecoration(
                                  color: AppColors.primaryColor,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(5)),
                                ),
                                alignment: Alignment.center,
                                margin: const EdgeInsets.only(right: 2.5),
                                padding: const EdgeInsets.all(8),
                                child: Text(
                                  'Calculate',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: fonts15),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  isDataVisible = false;
                                  length.clear();
                                  length2.clear();
                                  breadth.clear();
                                  breadth2.clear();
                                  height.clear();
                                  height2.clear();
                                  noOfPerson.clear();
                                  temperature.clear();
                                });
                              },
                              child: Container(
                                decoration: const BoxDecoration(
                                    color: AppColors.primaryColor,
                                    borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(5))),
                                alignment: Alignment.center,
                                margin: const EdgeInsets.only(left: 2.5),
                                padding: const EdgeInsets.all(8),
                                child: Text(
                                  'Reset',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: fonts15),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              if (isDataVisible) getAirConditionerSize(),
            ],
          ),
        ),
      ),
      floatingActionButton: isDataVisible
          ? FloatingActionButton(
              onPressed: () {
                ScreenShort().captureScreenShort(screenshotController);
              },
              backgroundColor: AppColors.primaryColor,
              child: const Icon(Icons.share),
            )
          : Container(),
    );
  }

  void _calculateEstimate() {
    isDataVisible = true;
    length2.text.isEmpty ? length2 = TextEditingController(text: '0') : length2;
    breadth2.text.isEmpty
        ? breadth2 = TextEditingController(text: '0')
        : breadth2;
    height.text.isEmpty ? height = TextEditingController(text: '0') : height;
    height2.text.isEmpty ? height2 = TextEditingController(text: '0') : height2;
    noOfPerson.text.isEmpty
        ? noOfPerson = TextEditingController(text: '0')
        : noOfPerson;
    temperature.text.isEmpty
        ? temperature = TextEditingController(text: '0')
        : temperature;
    int decimals = 2;
    final num fac = pow(10, decimals);
    late double _lengthFinally = double.parse(length.text);
    late double _breadthFinally = double.parse(breadth.text);
    late double _heightFinally = double.parse(height.text);
    late double _lengthFinal = double.parse(length2.text);
    late double _length =
        ((_lengthFinally + (_lengthFinal / 12)) * fac).round() / fac;
    late double _breadthFinal = double.parse(breadth2.text);
    late double _breadth =
        ((_breadthFinally + (_breadthFinal / 12)) * fac).round() / fac;
    late int _noOfExtraPerson = int.parse(noOfPerson.text) - 3;
    late double _person = double.parse(noOfPerson.text);
    late double _heightFinal = double.parse(height2.text);
    late double _height =
        ((_heightFinally + (_heightFinal / 12)) * fac).round() / fac;
    late double _tempFinal = double.parse(temperature.text);
    late double _temp = (_tempFinal * fac).round() / fac;
    if (_height == 0 && _person == 0 && _temp == 0) {
      setState(() {
        finalSizeTon = (((sqrt(_length * _breadth)) / 10) * fac).round() / fac;
      });
    } else if (_height != 0 && _person == 0 && _temp == 0) {
      setState(() {
        finalSizeTon =
            (((_length * _breadth * _height) / 1000) * fac).round() / fac;
      });
    } else {
      setState(() {
        // height
        if (_height <= 8) {
          setState(() {
            _height = 0;
          });
        } else {
          setState(() {
            _height = ((((double.parse(height.text) +
                                    (double.parse(height2.text) / 12)) -
                                8) *
                            0.1) *
                        fac)
                    .round() /
                fac;
          });
        }
        // person
        if (_person <= 3) {
          setState(() {
            _person = 0.3;
          });
        } else {
          setState(() {
            _person = (0.3 + (0.07 * _noOfExtraPerson));
          });
        }
        // temperature
        if (_temp > 45) {
          setState(() {
            _temp = 0.5;
          });
        } else if ((_temp > 41) && (_temp <= 45)) {
          setState(() {
            _temp = 0.4;
          });
        } else if ((_temp > 36) && (_temp <= 40)) {
          setState(() {
            _temp = 0.3;
          });
        } else {
          setState(() {
            _temp = 0.2;
          });
        }
        finalSizeTon =
            ((((_length * _breadth * 20) / 12000) + _temp + _person + _height) *
                        fac)
                    .round() /
                fac;
      });
    }
    length2.text != '0' ? length2 = length2 : length2.clear();
    breadth2.text != '0' ? breadth2 = breadth2 : breadth2.clear();
    height.text != '0' ? height = height : height.clear();
    height2.text != '0' ? height2 = height2 : height2.clear();
    noOfPerson.text != '0' ? noOfPerson = noOfPerson : noOfPerson.clear();
    temperature.text != '0' ? temperature = temperature : temperature.clear();
  }

  Widget getAirConditionerSize() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Size of Air Conditioner',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black)),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Text(
                  '$finalSizeTon Tons',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts17,
                      color: Colors.red),
                ),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 5, bottom: 10),
                child: Text(
                  'Note: All calculations are estimates based on the information you provide. It depends on several factors like city of use, no of people in a room,appliances in a room, etc',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                      fontSize: fonts15),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
