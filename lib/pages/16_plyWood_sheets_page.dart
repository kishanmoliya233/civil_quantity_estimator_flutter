import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/16_h_plyWood_sheets_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class PlyWoodSheets extends StatefulWidget {
  const PlyWoodSheets({super.key});

  @override
  State<PlyWoodSheets> createState() => _PlyWoodSheetsState();
}

class _PlyWoodSheetsState extends State<PlyWoodSheets> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var roomLength = TextEditingController();
  var roomLength2 = TextEditingController();
  var roomWidth = TextEditingController();
  var roomWidth2 = TextEditingController();
  var plywoodLength = TextEditingController(text: '4');
  var plywoodLength2 = TextEditingController();
  var plywoodWidth = TextEditingController(text: '8');
  var plywoodWidth2 = TextEditingController();

  late double finalPlyWoodSheets = 0;
  late double finalAreaMeter = 0;
  late double finalCoverFeet = 0;
  late double finalCoverMeter = 0;
  late double finalAreaFeet = 0;
  double rlength = 0;
  double rwidth = 0;
  double plength = 0;
  double pwidth = 0;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey8 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey9 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey10 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey11 = GlobalKey<FormState>();

  String? roomLength2Label;
  String? roomLengthLabel;
  String? roomWidth2Label;
  String? roomWidthLabel;
  String? plyWoodLength2Label;
  String? plyWoodLengthLabel;
  String? plyWoodWidth2Label;
  String? plyWoodWidthLabel;

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String roomLength2Label =
        buttons.isButtonClick2! ? 'Room Length (cm)' : 'Room Length (inch)';
    String roomLengthLabel =
        buttons.isButtonClick1! ? 'Room Length (feet)' : 'Room Length (meter)';
    String roomWidth2Label =
        buttons.isButtonClick2! ? 'Room Width (cm)' : 'Room Width (inch)';
    String roomWidthLabel =
        buttons.isButtonClick1! ? 'Room Width (feet)' : 'Room Width (meter)';
    String plyWoodLength2Label = buttons.isButtonClick2!
        ? 'Plywood Length (cm)'
        : 'Plywood Length (inch)';
    String plyWoodLengthLabel = buttons.isButtonClick1!
        ? 'Plywood Length (feet)'
        : 'Plywood Length (meter)';
    String plyWoodWidth2Label =
        buttons.isButtonClick2! ? 'Plywood Width (cm)' : 'Plywood Width (inch)';
    String plyWoodWidthLabel = buttons.isButtonClick1!
        ? 'Plywood Width (feet)'
        : 'Plywood Width (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/plyWoodSheets.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' plyWood Sheets Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryPlyWoodSheetsPage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          roomLength.clear();
                          roomLength2.clear();
                          roomWidth.clear();
                          roomWidth2.clear();
                          plywoodLength = TextEditingController(text: '4');
                          plywoodLength2.clear();
                          plywoodWidth = TextEditingController(text: '8');
                          plywoodWidth2.clear();

                          roomLength = TextEditingController(text: value[0]);
                          roomLength2 = TextEditingController(text: value[1]);
                          roomWidth = TextEditingController(text: value[2]);
                          roomWidth2 = TextEditingController(text: value[3]);
                          plywoodLength = TextEditingController(text: value[4]);
                          plywoodLength2 =
                              TextEditingController(text: value[5]);
                          plywoodWidth = TextEditingController(text: value[6]);
                          plywoodWidth2 = TextEditingController(text: value[7]);
                          buttons.isButtonClick1 =
                              value[8] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[8] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: roomLengthLabel,
                                    controller: roomLength,
                                    errMessage: 'Enter $roomLengthLabel',
                                    formkey: formKey,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: roomLength2Label,
                                      controller: roomLength2,
                                      errMessage: 'Enter $roomLength2Label',
                                      formkey: formKey1,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: roomWidthLabel,
                                    controller: roomWidth,
                                    errMessage: 'Enter $roomWidthLabel',
                                    formkey: formKey2,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: roomWidth2Label,
                                      controller: roomWidth2,
                                      errMessage: 'Enter $roomWidth2Label',
                                      formkey: formKey3,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: plyWoodLengthLabel,
                                    controller: plywoodLength,
                                    errMessage: 'Enter $plyWoodLengthLabel',
                                    formkey: formKey4,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: plyWoodLength2Label,
                                      controller: plywoodLength2,
                                      errMessage: 'Enter $plyWoodLength2Label',
                                      formkey: formKey5,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: plyWoodWidthLabel,
                                    controller: plywoodWidth,
                                    errMessage: 'Enter $plyWoodWidthLabel',
                                    formkey: formKey6,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: plyWoodWidth2Label,
                                      controller: plywoodWidth2,
                                      errMessage: 'Enter $plyWoodWidth2Label',
                                      formkey: formKey7,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey1.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey4.currentState!.validate() &&
                                            formKey5.currentState!.validate() &&
                                            formKey7.currentState!.validate() &&
                                            formKey6.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['RoomLength'] =
                                              roomLength.text;
                                          history['RoomLength2'] =
                                              roomLength2.text == ''
                                                  ? 0
                                                  : roomLength2.text;
                                          history['RoomWidth'] = roomWidth.text;
                                          history['RoomWidth2'] =
                                              roomWidth2.text == ''
                                                  ? 0
                                                  : roomWidth2.text;
                                          history['PlyWoodLength'] =
                                              plywoodLength.text;
                                          history['PlyWoodLength2'] =
                                              plywoodLength2.text == ''
                                                  ? 0
                                                  : plywoodLength2.text;
                                          history['PlyWoodWidth'] =
                                              plywoodWidth.text;
                                          history['PlyWoodWidth2'] =
                                              plywoodWidth2.text == ''
                                                  ? 0
                                                  : plywoodWidth2.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'PlyWood_Sheets', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        roomLength.clear();
                                        roomLength2.clear();
                                        roomWidth.clear();
                                        roomWidth2.clear();
                                        plywoodLength.clear();
                                        plywoodLength2.clear();
                                        plywoodWidth.clear();
                                        plywoodWidth2.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getVolume(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    roomLength2.text.isEmpty
        ? roomLength2 = TextEditingController(text: '0')
        : roomLength2;
    roomWidth2.text.isEmpty
        ? roomWidth2 = TextEditingController(text: '0')
        : roomWidth2;
    plywoodLength2.text.isEmpty
        ? plywoodLength2 = TextEditingController(text: '0')
        : plywoodLength2;
    plywoodWidth2.text.isEmpty
        ? plywoodWidth2 = TextEditingController(text: '0')
        : plywoodWidth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    rlength = double.parse('${roomLength.text}.${roomLength2.text}');
    rwidth = double.parse('${roomWidth.text}.${roomWidth2.text}');
    plength = double.parse('${plywoodLength.text}.${plywoodLength2.text}');
    pwidth = double.parse('${plywoodWidth.text}.${plywoodWidth2.text}');
    if (buttons.isButtonClick1!) {
      setState(() {
        finalAreaMeter = (((rlength * rwidth) / 10.764) * fac).round() / fac;
        finalCoverFeet = ((rlength * rwidth) * fac).round() / fac;
        finalCoverMeter = (((plength * pwidth) / 10.764) * fac).round() / fac;
        finalAreaFeet = ((plength * pwidth) * fac).round() / fac;
        finalPlyWoodSheets =
            ((finalCoverFeet / finalAreaFeet) * fac).round() / fac;
      });
    } else {
      setState(() {
        finalCoverFeet = (((rlength * rwidth) * 10.764) * fac).round() / fac;
        finalAreaMeter = ((rlength * rwidth) * fac).round() / fac;
        finalAreaFeet = (((plength * pwidth) * 10.764) * fac).round() / fac;
        finalCoverMeter = ((plength * pwidth) * fac).round() / fac;
        finalPlyWoodSheets =
            ((finalCoverFeet / finalAreaFeet) * fac).round() / fac;
      });
    }
    roomLength2.text != '0' ? roomLength2 = roomLength2 : roomLength2.clear();
    roomWidth2.text != '0' ? roomWidth2 = roomWidth2 : roomWidth2.clear();
    plywoodLength2.text != '0'
        ? plywoodLength2 = plywoodLength2
        : plywoodLength2.clear();
    plywoodWidth2.text != '0'
        ? plywoodWidth2 = plywoodWidth2
        : plywoodWidth2.clear();
  }

  Widget getVolume() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Plywood Sheets Required',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: fonts15)),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  '$finalPlyWoodSheets',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts17,
                      color: Colors.red),
                ),
              ),
            ),
            Dividers(),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 2),
                child: Text('Room Area',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: fonts15,
                        color: Colors.black)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts17),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalCoverFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Dividers(),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 2),
                child: Text('Plywood Cover',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: fonts15,
                        color: Colors.black)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalCoverMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts20),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
