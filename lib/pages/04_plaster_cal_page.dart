import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/04_h_plaster_cal_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class PlasterCalculationPage extends StatefulWidget {
  const PlasterCalculationPage({super.key});

  @override
  State<PlasterCalculationPage> createState() => _PlasterCalculationPageState();
}

class _PlasterCalculationPageState extends State<PlasterCalculationPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var plasterLength = TextEditingController();
  var plasterLength2 = TextEditingController();
  var plasterWidth = TextEditingController();
  var plasterWidth2 = TextEditingController();

  String? length2Label;
  String? lengthLabel;
  String? width2Label;
  String? widthLabel;

  String dropdownValue = '12 MM';
  String dropdownValue2 = 'C.M 1:3';

  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();

  late double finalAreaFeet = 0;
  late double finalAreaMeter = 0;
  late double _cementBag = 0;
  late double _sandTon = 0;

  bool isChangeValue = true;
  List<ChartData> chartData = [];
  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    chartData = [
      ChartData('Cement\n$_cementBag Bags', _cementBag * 50, Colors.blue),
      ChartData('Sand\n$_sandTon Ton', _sandTon * 1000, Colors.red),
    ];

    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String width2Label = buttons.isButtonClick2! ? 'Width (cm)' : 'Width (inch)';
    String widthLabel =
        buttons.isButtonClick1! ? 'Width (feet)' : 'Width (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/plasterCalculator.png',
                  width:45,
                  height: 45,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Plaster Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryPlasterCalculationPage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          plasterLength.clear();
                          plasterLength2.clear();
                          plasterWidth.clear();
                          plasterWidth2.clear();
                          dropdownValue2 = 'C.M 1:3';
                          dropdownValue = '12 MM';

                          dropdownValue = value[0];
                          plasterLength = TextEditingController(text: value[1]);
                          plasterLength2 = TextEditingController(text: value[2]);
                          plasterWidth = TextEditingController(text: value[3]);
                          plasterWidth2 = TextEditingController(text: value[4]);
                          dropdownValue2 = value[5];
                          buttons.isButtonClick1 =
                              value[6] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[6] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8,0,8,5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: InputDecorator(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: 'Plaster Type',
                                  labelStyle: TextStyle(fontSize: fonts15)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  value: dropdownValue,
                                  isExpanded: true,
                                  isDense: true,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: fonts15),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValue = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    '12 MM',
                                    '15 MM',
                                    '18 MM',
                                    '20 MM'
                                  ].map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  buttonHeight: 15,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: lengthLabel,
                                    controller: plasterLength,
                                    errMessage: 'Enter $lengthLabel',
                                    formkey: formKey1,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                    label: length2Label,
                                    controller: plasterLength2,
                                    errMessage: 'Enter $length2Label',
                                    formkey: formKey2,
                                    validate: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: widthLabel,
                                    controller: plasterWidth,
                                    errMessage: 'Enter $widthLabel',
                                    formkey: formKey3,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                    label: width2Label,
                                    controller: plasterWidth2,
                                    errMessage: 'Enter $width2Label',
                                    formkey: formKey4,
                                    validate: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: InputDecorator(
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(),
                                  labelText: 'Grade of footing',
                                  labelStyle: TextStyle(fontSize: fonts15)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  value: dropdownValue2,
                                  isDense: true,
                                  isExpanded: true,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: fonts15),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValue2 = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    'C.M 1:3',
                                    'C.M 1:4',
                                    'C.M 1:5',
                                    'C.M 1:6'
                                  ].map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  buttonHeight: 15,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus?.unfocus();
                                      setState(() {
                                        if (formKey1.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey4.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['PlasterType'] = dropdownValue;
                                          history['Length'] = plasterLength.text;
                                          history['Length2'] =
                                              plasterLength2.text == '' ? 0 : plasterLength2.text;
                                          history['Width'] = plasterWidth.text;
                                          history['Width2'] =
                                              plasterWidth2.text == '' ? 0 : plasterWidth2.text;
                                          history['GradeOfFooting'] =
                                              dropdownValue2;
                                          history['Unit'] = buttons.isButtonClick1;
                                          MyDatabase()
                                              .insertData('Plaster_Cal', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        plasterLength.clear();
                                        plasterLength2.clear();
                                        plasterWidth.clear();
                                        plasterWidth2.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getAreaOfPlaster(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
            onPressed: () {
              ScreenShort().captureScreenShort(screenshotController);
            },
            backgroundColor: AppColors.primaryColor,
            child: const Icon(Icons.share),
          )
              : Container(),
        )
    );
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    plasterLength2 = plasterLength2.text.isEmpty
        ? plasterLength2 = TextEditingController(text: '0')
        : plasterLength2;
    plasterWidth2 = plasterWidth2.text.isEmpty
        ? plasterWidth2 = TextEditingController(text: '0')
        : plasterWidth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    late double _volume =
        (((((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) +
                        (((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume1 =
        (((((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) +
                        (((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume2 =
        (((((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) +
                        (((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume3 =
        (((((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) +
                        (((finalAreaMeter * 0.012) + ((finalAreaMeter * 0.012) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume4 =
        (((((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) +
                        (((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume5 =
        (((((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) +
                        (((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume6 =
        (((((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) +
                        (((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume7 =
        (((((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) +
                        (((finalAreaMeter * 0.015) + ((finalAreaMeter * 0.015) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume8 =
        (((((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) +
                        (((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume9 =
        (((((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) +
                        (((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume10 =
        (((((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) +
                        (((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume11 =
        (((((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) +
                        (((finalAreaMeter * 0.018) + ((finalAreaMeter * 0.018) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume12 =
        (((((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) +
                        (((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume13 =
        (((((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) +
                        (((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume14 =
        (((((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) +
                        (((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _volume15 =
        (((((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) +
                        (((finalAreaMeter * 0.020) + ((finalAreaMeter * 0.020) * 0.3)) *
                            0.25)) *
                    fac)
                .round()) /
            fac;
    late double _cementVolume = _volume * (1 / 4);
    late double _sandVolume = _volume * (3 / 4);
    late double _cementVolume1 = _volume1 * (1 / 5);
    late double _sandVolume1 = _volume1 * (4 / 5);
    late double _cementVolume2 = _volume2 * (1 / 6);
    late double _sandVolume2 = _volume2 * (5 / 6);
    late double _cementVolume3 = _volume3 * (1 / 7);
    late double _sandVolume3 = _volume3 * (6 / 7);
    late double _cementVolume4 = _volume4 * (1 / 4);
    late double _sandVolume4 = _volume4 * (3 / 4);
    late double _cementVolume5 = _volume5 * (1 / 5);
    late double _sandVolume5 = _volume5 * (4 / 5);
    late double _cementVolume6 = _volume6 * (1 / 6);
    late double _sandVolume6 = _volume6 * (5 / 6);
    late double _cementVolume7 = _volume7 * (1 / 7);
    late double _sandVolume7 = _volume7 * (6 / 7);
    late double _cementVolume8 = _volume8 * (1 / 4);
    late double _sandVolume8 = _volume8 * (3 / 4);
    late double _cementVolume9 = _volume9 * (1 / 5);
    late double _sandVolume9 = _volume9 * (4 / 5);
    late double _cementVolume10 = _volume10 * (1 / 6);
    late double _sandVolume10 = _volume10 * (5 / 6);
    late double _cementVolume11 = _volume11 * (1 / 7);
    late double _sandVolume11 = _volume11 * (6 / 7);
    late double _cementVolume12 = _volume12 * (1 / 4);
    late double _sandVolume12 = _volume12 * (3 / 4);
    late double _cementVolume13 = _volume13 * (1 / 5);
    late double _sandVolume13 = _volume13 * (4 / 5);
    late double _cementVolume14 = _volume14 * (1 / 6);
    late double _sandVolume14 = _volume14 * (5 / 6);
    late double _cementVolume15 = _volume15 * (1 / 7);
    late double _sandVolume15 = _volume15 * (6 / 7);
    double length = double.parse('${plasterLength.text}.${plasterLength2.text}');
    double width = double.parse('${plasterWidth.text}.${plasterWidth2.text}');
    if (dropdownValue == '12 MM' && dropdownValue2 == 'C.M 1:3') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '12 MM' && dropdownValue2 == 'C.M 1:4') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume1 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume1 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).ceil().round() / fac;
          _cementBag = (_cementVolume1 / 0.035).round().toDouble();
          _sandTon = ((_sandVolume1 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '12 MM' && dropdownValue2 == 'C.M 1:5') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume2 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume2 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume2 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume2 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '12 MM' && dropdownValue2 == 'C.M 1:6') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume3 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume3 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume3 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume3 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '15 MM' && dropdownValue2 == 'C.M 1:3') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume4 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume4 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume4 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume4 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '15 MM' && dropdownValue2 == 'C.M 1:4') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume5 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume5 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume5 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume5 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '15 MM' && dropdownValue2 == 'C.M 1:5') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume6 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume6 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume6 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume6 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '15 MM' && dropdownValue2 == 'C.M 1:6') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume7 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume7 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume7 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume7 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '18 MM' && dropdownValue2 == 'C.M 1:3') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume8 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume8 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume8 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume8 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '18 MM' && dropdownValue2 == 'C.M 1:4') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume9 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume9 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume9 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume9 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '18 MM' && dropdownValue2 == 'C.M 1:5') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume10 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume10 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume10 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume10 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '18 MM' && dropdownValue2 == 'C.M 1:6') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume11 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume11 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume11 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume11 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '20 MM' && dropdownValue2 == 'C.M 1:3') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume12 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume12 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume12 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume12 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '20 MM' && dropdownValue2 == 'C.M 1:4') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume13 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume13 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume13 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume13 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue == '20 MM' && dropdownValue2 == 'C.M 1:5') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume14 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume14 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume14 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume14 * 1550 / 1000) * fac).round() / fac;
        });
      }
    } else {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalAreaMeter = (((length * width) / 10.7639) * fac).round() / fac;
          finalAreaFeet = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume15 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume15 * 1550 / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalAreaFeet = (((length * width) * 10.7639) * fac).round() / fac;
          finalAreaMeter = ((length * width) * fac).round() / fac;
          _cementBag = (_cementVolume15 / 0.035).ceil().round().toDouble();
          _sandTon = ((_sandVolume15 * 1550 / 1000) * fac).round() / fac;
        });
      }
    }
    plasterLength2.text != '0' ? plasterLength2 = plasterLength2 : plasterLength2.clear();
    plasterWidth2.text != '0' ? plasterWidth2 = plasterWidth2 : plasterWidth2.clear();
  }

  Widget getAreaOfPlaster() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 2),
                child: Text('Total Area of Plaster',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalAreaFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '2',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts15,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(right: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: Text('Material',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.white)),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(left: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: Text('Quantity',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: fonts17,
                            color: Colors.white)),
                  ),
                )
              ],
            ),
            GetMaterial(
              'assets/icons/cement.png',
              'Cement',
              '$_cementBag',
              'Bags',
              isDivider: true,
            ),
            GetMaterial(
              'assets/icons/sand.png',
              'Sand',
              '$_sandTon',
              'Ton',
              isDivider: false,
            ),
            _cementBag != 0 ? SfCircularChart(
              margin: EdgeInsets.zero,
              series: <CircularSeries>[
                PieSeries<ChartData, String>(
                  dataSource: chartData,
                  radius: '80',
                  pointColorMapper: (ChartData data, _) => data.color,
                  xValueMapper: (ChartData data, _) => data.x,
                  yValueMapper: (ChartData data, _) => data.y,
                  dataLabelMapper: (ChartData data, _) => data.x,
                  dataLabelSettings: const DataLabelSettings(
                    isVisible: true,
                    labelPosition: ChartDataLabelPosition.outside,
                  ),
                  enableTooltip: true,
                )
              ],
            ) : Container(),
          ],
        ),
      ),
    );
  }
}
