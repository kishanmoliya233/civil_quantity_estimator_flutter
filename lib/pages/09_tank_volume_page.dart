import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/09_h_tank_volume_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class TankVolumePage extends StatefulWidget {
  const TankVolumePage({super.key});

  @override
  State<TankVolumePage> createState() => _TankVolumePageState();
}

class _TankVolumePageState extends State<TankVolumePage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  String? length2Label;
  String? lengthLabel;
  String? width2Label;
  String? widthLabel;
  String? depthLabel;
  String? depth2Label;

  var tankLength = TextEditingController();
  var tankLength2 = TextEditingController();
  var tankWidth = TextEditingController();
  var tankWidth2 = TextEditingController();
  var tankDepth = TextEditingController();
  var tankDepth2 = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey8 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey9 = GlobalKey<FormState>();
  late double finalVolumeFeet = 0;
  late double finalVolumeMeter = 0;
  late double finalWaterLiters = 0;

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String width2Label =
        buttons.isButtonClick2! ? 'Width (cm)' : 'Width (inch)';
    String widthLabel =
        buttons.isButtonClick1! ? 'Width (feet)' : 'Width (meter)';
    String depth2Label =
        buttons.isButtonClick2! ? 'Depth (cm)' : 'Depth (inch)';
    String depthLabel =
        buttons.isButtonClick1! ? 'Depth (feet)' : 'Depth (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/tankVolume.png',
                  width: 45,
                  height: 45,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    'Tank Volume Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryTankVolumePage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          tankLength.clear();
                          tankLength2.clear();
                          tankWidth.clear();
                          tankWidth2.clear();
                          tankDepth.clear();
                          tankDepth2.clear();

                          tankLength = TextEditingController(text: value[0]);
                          tankLength2 = TextEditingController(text: value[1]);
                          tankWidth = TextEditingController(text: value[2]);
                          tankWidth2 = TextEditingController(text: value[3]);
                          tankDepth = TextEditingController(text: value[4]);
                          tankDepth2 = TextEditingController(text: value[5]);
                          buttons.isButtonClick1 =
                              value[6] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[6] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: lengthLabel,
                                    controller: tankLength,
                                    errMessage: 'Enter $lengthLabel',
                                    formkey: formKey,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: length2Label,
                                      controller: tankLength2,
                                      errMessage: 'Enter $length2Label',
                                      formkey: formKey1,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: widthLabel,
                                    controller: tankWidth,
                                    errMessage: 'Enter $widthLabel',
                                    formkey: formKey2,
                                    // validate: _validate,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: width2Label,
                                      controller: tankWidth2,
                                      errMessage: 'Enter $width2Label',
                                      formkey: formKey3,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: depthLabel,
                                    controller: tankDepth,
                                    errMessage: 'Enter $depthLabel',
                                    formkey: formKey4,
                                    // validate: _validate,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: depth2Label,
                                      controller: tankDepth2,
                                      errMessage: 'Enter $depth2Label',
                                      formkey: formKey5,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey.currentState!.validate() &&
                                            formKey1.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey5.currentState!.validate() &&
                                            formKey4.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['Length'] = tankLength.text;
                                          history['Length2'] =
                                              tankLength2.text == ''
                                                  ? 0
                                                  : tankLength2.text;
                                          history['Width'] = tankWidth.text;
                                          history['Widht2'] =
                                              tankWidth2.text == ''
                                                  ? 0
                                                  : tankWidth2.text;
                                          history['Depth'] = tankDepth.text;
                                          history['Depth2'] =
                                              tankDepth2.text == ''
                                                  ? 0
                                                  : tankDepth2.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'Tank_Volume', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        tankLength.clear();
                                        tankLength2.clear();
                                        tankWidth.clear();
                                        tankWidth2.clear();
                                        tankDepth.clear();
                                        tankDepth2.clear();

                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getWaterCapicity(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    tankLength2 = tankLength2.text.isEmpty
        ? TextEditingController(text: '0')
        : tankLength2;
    tankWidth2 =
        tankWidth2.text.isEmpty ? TextEditingController(text: '0') : tankWidth2;
    tankDepth2 =
        tankDepth2.text.isEmpty ? TextEditingController(text: '0') : tankDepth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    double length = double.parse('${tankLength.text}.${tankLength2.text}');
    double width = double.parse('${tankWidth.text}.${tankWidth2.text}');
    double depth = double.parse('${tankDepth.text}.${tankDepth2.text}');
    if (buttons.isButtonClick1!) {
      setState(() {
        finalVolumeMeter =
            (((length * width * depth) / 35.3147) * fac).round() / fac;
        finalVolumeFeet = (((length * width * depth)) * fac).round() / fac;
        finalWaterLiters =
            (((length * width * depth) / 35.3147 * 1000) * fac).round() / fac;
      });
    } else {
      setState(() {
        finalVolumeFeet =
            (((length * width * depth) * 35.3147) * fac).round() / fac;
        finalVolumeMeter = (((length * width * depth)) * fac).round() / fac;
        finalWaterLiters =
            (((length * width * depth) * 1000) * fac).round() / fac;
      });
    }
    tankLength2.text != '0' ? tankLength2 = tankLength2 : tankLength2.clear();
    tankWidth2.text != '0' ? tankWidth2 = tankWidth2 : tankWidth2.clear();
    tankDepth2.text != '0' ? tankDepth2 = tankDepth2 : tankDepth2.clear();
  }

  Widget getWaterCapicity() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Capacity of Water-Sump/Tank',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts17)),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '$finalWaterLiters',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: fonts17,
                          color: Colors.red),
                    ),
                    Text('  Liters',
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: fonts15,
                            color: Colors.red)),
                  ],
                ),
              ),
            ),
            Dividers(),
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Volume',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts17)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalVolumeMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalVolumeFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
