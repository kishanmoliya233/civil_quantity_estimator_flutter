import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/18_h_round_column_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class RoundColumnPage extends StatefulWidget {
  const RoundColumnPage({super.key});

  @override
  State<RoundColumnPage> createState() => _RoundColumnPageState();
}

class _RoundColumnPageState extends State<RoundColumnPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  String dropdownValue1 = 'M20 (1:1.5:3)';

  var roundDiameter1 = TextEditingController();
  var roundDiameter2 = TextEditingController();
  var roundHeight = TextEditingController();
  var roundHeight2 = TextEditingController();
  var noOfRoundColumns = TextEditingController(text: '1');

  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();

  String? diameter2Label;
  String? diameterLabel;
  String? height2Label;
  String? heightLabel;

  late double finalVolumeFeet = 0;
  late double finalVolumeMeter = 0;
  late double _cementBag = 0;
  late double _sandTon = 0;
  late double _aggTon = 0;

  bool isChangeValue = true;
  List<ChartData> chartData = [];
  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    chartData = [
      ChartData('Cement\n$_cementBag Bags', _cementBag * 50, Colors.blue),
      ChartData('Sand\n$_sandTon Ton', _sandTon * 1000, Colors.red),
      ChartData(
          'Aggregate\n$_aggTon Ton', _aggTon * 1000, Colors.yellow),
    ];
    String diameter2Label =
        buttons.isButtonClick2! ? 'Diameter (cm)' : 'Diameter (inch)';
    String diameterLabel =
        buttons.isButtonClick1! ? 'Diameter (feet)' : 'Diameter (meter)';
    String height2Label =
        buttons.isButtonClick2! ? 'Height (cm)' : 'Height (inch)';
    String heightLabel =
        buttons.isButtonClick1! ? 'Height (feet)' : 'Height (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/roundColumn.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Round Column Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryRoundColumePage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          roundDiameter1.clear();
                          roundDiameter2.clear();
                          roundHeight.clear();
                          roundHeight2.clear();
                          noOfRoundColumns = TextEditingController(text: '1');
                          dropdownValue1 = 'M20 (1:1.5:3)';

                          dropdownValue1 = value[0];
                          roundDiameter1 =
                              TextEditingController(text: value[1]);
                          roundDiameter2 =
                              TextEditingController(text: value[2]);
                          roundHeight = TextEditingController(text: value[3]);
                          roundHeight2 = TextEditingController(text: value[4]);
                          noOfRoundColumns =
                              TextEditingController(text: value[5]);
                          buttons.isButtonClick1 =
                              value[6] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[6] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: InputDecorator(
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Grade of Concrete'),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  value: dropdownValue1,
                                  isExpanded: true,
                                  isDense: true,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: fonts17),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValue1 = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    'M20 (1:1.5:3)',
                                    'M15 (1:2:4)',
                                    'M10 (1:3:6)',
                                    'M7.5 (1:4:8)'
                                  ].map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: diameterLabel,
                                      controller: roundDiameter1,
                                      errMessage: 'Enter $diameterLabel',
                                      formkey: formKey1),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: diameter2Label,
                                      controller: roundDiameter2,
                                      errMessage: 'Enter $diameter2Label',
                                      formkey: formKey2,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: heightLabel,
                                      controller: roundHeight,
                                      errMessage: 'Enter $heightLabel',
                                      formkey: formKey3),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: height2Label,
                                      controller: roundHeight2,
                                      errMessage: 'Enter $height2Label',
                                      formkey: formKey4,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                const Expanded(
                                  child: Text("No. of Columns :",
                                      textAlign: TextAlign.center),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: 'No. of Columns',
                                      controller: noOfRoundColumns,
                                      errMessage: 'Enter No. of Columns',
                                      formkey: formKey5),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey1.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey4.currentState!.validate() &&
                                            formKey5.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['GradeOfConcrete'] =
                                              dropdownValue1;
                                          history['Diameter'] =
                                              roundDiameter1.text;
                                          history['Diameter2'] =
                                              roundDiameter2.text == ''
                                                  ? 0
                                                  : roundDiameter2.text;
                                          history['Height'] = roundHeight.text;
                                          history['Height2'] =
                                              roundHeight2.text == ''
                                                  ? 0
                                                  : roundHeight2.text;
                                          history['NoOfColumn'] =
                                              noOfRoundColumns.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'Round_Column', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        roundDiameter1.clear();
                                        roundDiameter2.clear();
                                        roundHeight.clear();
                                        roundHeight2.clear();
                                        noOfRoundColumns.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getVolumeOfRoundColumn(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    roundDiameter2.text.isEmpty
        ? roundDiameter2 = TextEditingController(text: '0')
        : roundDiameter2;
    roundHeight2.text.isEmpty
        ? roundHeight2 = TextEditingController(text: '0')
        : roundHeight2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    late double _wetVolumeFinal =
        ((finalVolumeMeter * 1.524) * fac).round() / fac;
    late double _cementVolume = _wetVolumeFinal * (1 / 5.5);
    late double _sandVolume = _wetVolumeFinal * (1.5 / 5.5);
    late double _aggVolume = _wetVolumeFinal * (3 / 5.5);
    late double _cementVolume1 = _wetVolumeFinal * (1 / 7);
    late double _sandVolume1 = _wetVolumeFinal * (2 / 7);
    late double _aggVolume1 = _wetVolumeFinal * (4 / 7);
    late double _cementVolume2 = _wetVolumeFinal * (1 / 10);
    late double _sandVolume2 = _wetVolumeFinal * (3 / 10);
    late double _aggVolume2 = _wetVolumeFinal * (6 / 10);
    late double _cementVolume3 = _wetVolumeFinal * (1 / 13);
    late double _sandVolume3 = _wetVolumeFinal * (4 / 13);
    late double _aggVolume3 = _wetVolumeFinal * (8 / 13);
    double diameter =
        (double.parse('${roundDiameter1.text}.${roundDiameter2.text}') / 2);
    double height = double.parse('${roundHeight.text}.${roundHeight2.text}');
    double column = double.parse(noOfRoundColumns.text);
    if (dropdownValue1 == 'M20 (1:1.5:3)') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalVolumeFeet =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeMeter = ((finalVolumeFeet / 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalVolumeMeter =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeFeet = ((finalVolumeMeter * 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue1 == 'M15 (1:2:4)') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalVolumeFeet =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeMeter = ((finalVolumeFeet / 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume1 / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume1 * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume1 * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalVolumeMeter =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeFeet = ((finalVolumeMeter * 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume1 / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume1 * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume1 * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    } else if (dropdownValue1 == 'M10 (1:3:6)') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalVolumeFeet =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeMeter = ((finalVolumeFeet / 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume2 / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume2 * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume2 * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalVolumeMeter =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeFeet = ((finalVolumeMeter * 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume2 / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume2 * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume2 * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    } else {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalVolumeFeet =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeMeter = ((finalVolumeFeet / 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume3 / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume3 * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume3 * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      } else {
        setState(() {
          finalVolumeMeter =
              (pi * diameter * diameter * height * column * fac).round() /
                  fac;
          finalVolumeFeet = ((finalVolumeMeter * 35.3147) * fac).round() / fac;
          _cementBag = (_cementVolume3 / 0.035).ceil().round().toDouble();
          late double _sandBag = ((_sandVolume3 * 1550) * fac).round() / fac;
          _sandTon = ((_sandBag / 1000) * fac).round() / fac;
          late double _aggBag = ((_aggVolume3 * 1350) * fac).round() / fac;
          _aggTon = ((_aggBag / 1000) * fac).round() / fac;
        });
      }
    }
    roundDiameter2.text != '0'
        ? roundDiameter2 = roundDiameter2
        : roundDiameter2.clear();
    roundHeight2.text != '0'
        ? roundHeight2 = roundHeight2
        : roundHeight2.clear();
  }

  Widget getVolumeOfRoundColumn() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 2),
                child: Text('Total Value of Round Column',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.black)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalVolumeMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalVolumeFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(right: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: const Text('Material',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            color: Colors.white)),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(left: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: const Text('Quantity',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            color: Colors.white)),
                  ),
                )
              ],
            ),
            GetMaterial(
                'assets/icons/cement.png', 'Cement', '$_cementBag', 'Bags',
                isDivider: true),
            GetMaterial('assets/icons/sand.png', 'Sand', '$_sandTon', 'Ton',
                isDivider: true),
            GetMaterial(
                'assets/icons/aggregate.png', 'Aggregate', '$_aggTon', 'Ton',
                isDivider: false),
            _cementBag != 0 ? SfCircularChart(
              margin: EdgeInsets.zero,
              series: <CircularSeries>[
                PieSeries<ChartData, String>(
                  dataSource: chartData,
                  radius: '80',
                  pointColorMapper: (ChartData data, _) => data.color,
                  xValueMapper: (ChartData data, _) => data.x,
                  yValueMapper: (ChartData data, _) => data.y,
                  dataLabelMapper: (ChartData data, _) => data.x,
                  dataLabelSettings: const DataLabelSettings(
                    isVisible: true,
                    labelPosition: ChartDataLabelPosition.outside,
                  ),
                  enableTooltip: true,
                )
              ],
            ) : Container(),
          ],
        ),
      ),
    );
  }
}
