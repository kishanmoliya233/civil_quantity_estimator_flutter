import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/15_h_wood_farming_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class WoodFarmingPage extends StatefulWidget {
  const WoodFarmingPage({super.key});

  @override
  State<WoodFarmingPage> createState() => _WoodFarmingPageState();
}

class _WoodFarmingPageState extends State<WoodFarmingPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var woodLength = TextEditingController();
  var woodLength2 = TextEditingController();
  var woodDepth = TextEditingController();
  var woodDepth2 = TextEditingController();
  var woodThickness = TextEditingController();
  var woodThickness2 = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey8 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey9 = GlobalKey<FormState>();
  late double finalVolumeFeet = 0;
  late double finalVolumeMeter = 0;
  double length = 0;
  double depth = 0;
  double thickness = 0;

  String? length2Label;
  String? lengthLabel;
  String? depth2Label;
  String? depthLabel;
  String? thickness2Label;
  String? thicknessLabel;

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String depth2Label =
        buttons.isButtonClick2! ? 'Depth (cm)' : 'Depth (inch)';
    String depthLabel =
        buttons.isButtonClick1! ? 'Depth (feet)' : 'Depth (meter)';
    String thickness2Label =
        buttons.isButtonClick2! ? 'Thickness (cm)' : 'Thickness (inch)';
    String thicknessLabel =
        buttons.isButtonClick1! ? 'Thickness (feet)' : 'Thickness (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/woodFarming.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Wood Farming Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryWoodFarmingPage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          woodLength.clear();
                          woodLength2.clear();
                          woodDepth.clear();
                          woodDepth2.clear();
                          woodThickness.clear();
                          woodThickness2.clear();

                          woodLength = TextEditingController(text: value[0]);
                          woodLength2 = TextEditingController(text: value[1]);
                          woodDepth = TextEditingController(text: value[2]);
                          woodDepth2 = TextEditingController(text: value[3]);
                          woodThickness = TextEditingController(text: value[4]);
                          woodThickness2 =
                              TextEditingController(text: value[5]);
                          buttons.isButtonClick1 =
                              value[6] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[6] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: lengthLabel,
                                    controller: woodLength,
                                    errMessage: 'Enter $lengthLabel',
                                    formkey: formKey,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: length2Label,
                                      controller: woodLength2,
                                      errMessage: 'Enter $length2Label',
                                      formkey: formKey1,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: depthLabel,
                                    controller: woodDepth,
                                    errMessage: 'Enter $depthLabel',
                                    formkey: formKey2,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: depth2Label,
                                      controller: woodDepth2,
                                      errMessage: 'Enter $depth2Label',
                                      formkey: formKey3,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: thicknessLabel,
                                    controller: woodThickness,
                                    errMessage: 'Enter $thicknessLabel',
                                    formkey: formKey4,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: thickness2Label,
                                      controller: woodThickness2,
                                      errMessage: 'Enter $thickness2Label',
                                      formkey: formKey5,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey1.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey5.currentState!.validate() &&
                                            formKey4.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['Length'] = woodLength.text;
                                          history['Length2'] =
                                              woodLength2.text == ''
                                                  ? 0
                                                  : woodLength2.text;
                                          history['Depth'] = woodDepth.text;
                                          history['Depth2'] =
                                              woodDepth2.text == ''
                                                  ? 0
                                                  : woodDepth2.text;
                                          history['Thickness'] =
                                              woodThickness.text;
                                          history['Thickness2'] =
                                              woodThickness2.text == ''
                                                  ? 0
                                                  : woodThickness2.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'Wood_Farming', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        woodLength.clear();
                                        woodLength2.clear();
                                        woodDepth.clear();
                                        woodDepth2.clear();
                                        woodThickness.clear();
                                        woodThickness2.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getVolume(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    woodLength2.text.isEmpty
        ? woodLength2 = TextEditingController(text: '0')
        : woodLength2;
    woodDepth2.text.isEmpty
        ? woodDepth2 = TextEditingController(text: '0')
        : woodDepth2;
    woodThickness2.text.isEmpty
        ? woodThickness2 = TextEditingController(text: '0')
        : woodThickness2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    length = double.parse('${woodLength.text}.${woodLength2.text}');
    depth = double.parse('${woodDepth.text}.${woodDepth2.text}');
    thickness = double.parse('${woodThickness.text}.${woodThickness2.text}');
    if ((buttons.isButtonClick1!)) {
      setState(() {
        finalVolumeMeter =
            (((length * depth * thickness) / 35.3147) * fac).round() / fac;
        finalVolumeFeet = (((length * depth * thickness)) * fac).round() / fac;
      });
    } else {
      setState(() {
        finalVolumeFeet =
            (((length * depth * thickness) * 35.3147) * fac).round() / fac;
        finalVolumeMeter = (((length * depth * thickness)) * fac).round() / fac;
      });
    }
    woodLength2.text != '0' ? woodLength2 = woodLength2 : woodLength2.clear();
    woodDepth2.text != '0' ? woodDepth2 = woodDepth2 : woodDepth2.clear();
    woodThickness2.text != '0'
        ? woodThickness2 = woodThickness2
        : woodThickness2.clear();
  }

  Widget getVolume() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: Text('Total Volume of Wood Frame',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: fonts17)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalVolumeMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts15,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$finalVolumeFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
