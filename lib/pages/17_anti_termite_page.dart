import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/17_h_anti_termite_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/dividers.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class AntiTermitePage extends StatefulWidget {
  const AntiTermitePage({super.key});

  @override
  State<AntiTermitePage> createState() => _AntiTermitePageState();
}

class _AntiTermitePageState extends State<AntiTermitePage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var termiteLength = TextEditingController();
  var termiteLength2 = TextEditingController();
  var termiteWidth = TextEditingController();
  var termiteWidth2 = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();
  late double finalVolumeFeet = 0;
  late double finalVolumeMeter = 0;
  late double _quantity = 0;

  String? length2Label;
  String? lengthLabel;
  String? width2Label;
  String? widthLabel;

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String width2Label =
        buttons.isButtonClick2! ? 'Width (cm)' : 'Width (inch)';
    String widthLabel =
        buttons.isButtonClick1! ? 'Width (feet)' : 'Width (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/antiTermite.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Anti Termite Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryAntiTermitePagePage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          termiteLength.clear();
                          termiteLength2.clear();
                          termiteWidth.clear();
                          termiteWidth2.clear();

                          termiteLength = TextEditingController(text: value[0]);
                          termiteLength2 =
                              TextEditingController(text: value[1]);
                          termiteWidth = TextEditingController(text: value[2]);
                          termiteWidth2 = TextEditingController(text: value[3]);
                          buttons.isButtonClick1 =
                              value[4] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[4] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: lengthLabel,
                                    controller: termiteLength,
                                    errMessage: 'Enter $lengthLabel',
                                    formkey: formKey,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: length2Label,
                                      controller: termiteLength2,
                                      errMessage: 'Enter $length2Label',
                                      formkey: formKey1,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 7, 7, 3),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: widthLabel,
                                    controller: termiteWidth,
                                    errMessage: 'Enter $widthLabel',
                                    formkey: formKey2,
                                  ),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: width2Label,
                                      controller: termiteWidth2,
                                      errMessage: 'Enter $width2Label',
                                      formkey: formKey3,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey.currentState!.validate() &&
                                            formKey1.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey3.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['Length'] =
                                              termiteLength.text;
                                          history['Length2'] =
                                              termiteLength2.text == ''
                                                  ? 0
                                                  : termiteLength2.text;
                                          history['Width'] = termiteWidth.text;
                                          history['Width2'] =
                                              termiteWidth2.text == ''
                                                  ? 0
                                                  : termiteWidth2.text;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase().insertData(
                                              'Anti_Termite', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        termiteLength.clear();
                                        termiteLength2.clear();
                                        termiteWidth.clear();
                                        termiteWidth2.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getVolume(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    termiteLength2.text.isEmpty
        ? termiteLength2 = TextEditingController(text: '0')
        : termiteLength2;
    termiteWidth2.text.isEmpty
        ? termiteWidth2 = TextEditingController(text: '0')
        : termiteWidth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    double length = 0;
    double width = 0;
    if (buttons.isButtonClick2!) {
      length = (double.parse(termiteLength.text) * 100 +
              double.parse(termiteLength2.text)) *
          0.01;
      width = (double.parse(termiteWidth.text) * 100 +
              double.parse(termiteWidth2.text)) *
          0.01;
    } else {
      length = (double.parse(termiteLength.text) * 12 +
              double.parse(termiteLength2.text)) *
          0.0833333;
      width = (double.parse(termiteWidth.text) * 12 +
              double.parse(termiteWidth2.text)) *
          0.0833333;
    }
    if (buttons.isButtonClick1!) {
      setState(() {
        finalVolumeMeter = (((length * width) / 10.7639) * fac).round() / fac;
        finalVolumeFeet = ((length * width) * fac).round() / fac;
        _quantity = (((finalVolumeMeter * 30) * fac).round() / fac);
      });
    } else {
      setState(() {
        finalVolumeFeet = (length * width * 10.7639 * fac).round() / fac;
        finalVolumeMeter = (length * width * fac).round() / fac;
        _quantity = ((finalVolumeMeter * 30) * fac).round() / fac;
      });
    }
    setState(() {
      termiteLength2.text != '0'
          ? termiteLength2 = termiteLength2
          : termiteLength2.clear();
      termiteWidth2.text != '0'
          ? termiteWidth2 = termiteWidth2
          : termiteWidth2.clear();
    });
  }

  Widget getVolume() {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Card(
          elevation: 4,
          child: Column(
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 2),
                  child: Text('Quantity of Anti Termite',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: fonts17,
                          color: Colors.black)),
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$_quantity ml',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
              ),
              Dividers(),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 2),
                  child: Text('Total Area',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: fonts17,
                          color: Colors.black)),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Text(
                      '$finalVolumeMeter m',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: fonts17,
                          color: Colors.red),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Text(
                      '2',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: fonts13,
                        color: Colors.red,
                        fontFeatures: const <FontFeature>[
                          FontFeature.superscripts(),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Text(
                      ' or ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: fonts17,
                          color: Colors.black),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Text(
                      '$finalVolumeFeet ft',
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: fonts17,
                          color: Colors.red),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 13),
                    child: Text(
                      '2',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: fonts13,
                        color: Colors.red,
                        fontFeatures: const <FontFeature>[
                          FontFeature.superscripts(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
