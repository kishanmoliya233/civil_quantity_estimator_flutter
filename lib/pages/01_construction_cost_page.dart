import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/01_h_construction_cost_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class BuildUpCalculationPage extends StatefulWidget {
  const BuildUpCalculationPage({super.key});

  @override
  State<BuildUpCalculationPage> createState() => _BuildUpCalculationPageState();
}

class _BuildUpCalculationPageState extends State<BuildUpCalculationPage> {
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;
  var buildupController = TextEditingController();
  var costController = TextEditingController();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();

  bool isDataVisible = false;

  NumberFormat myFormat = NumberFormat.decimalPattern('en_us');

  List<_ChartData> chartData = [
    _ChartData('16.4 %\nCement', 16.4, Colors.green.shade300),
    _ChartData('22.8 %\nFitting', 12.3, Colors.yellowAccent.shade400),
    _ChartData('12.3 %\nSand', 22.8, Colors.cyan.shade300),
    _ChartData('16.5 %\nFinisher', 16.5, Colors.lightGreenAccent.shade400),
    _ChartData('24.6 %\nSteel', 24.6, Colors.blue.shade300),
    _ChartData('7.4 %\nAggregate', 7.4, Colors.red.shade400),
  ];

  late TooltipBehavior _tooltipBehavior;

  @override
  void initState() {
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  double _first = 0;
  double _second = 0;
  double _third = 0;
  double _forth = 0;
  double _fifth = 0;
  double _sixth = 0;
  double _volume = 0;
  double _cement = 0;
  double _cementBag = 0;
  double _sand = 0;
  double _sandBag = 0;
  double _aggregate = 0;
  double _aggregateBag = 0;
  double _steel = 0;
  double _steelBag = 0;
  double _finishers = 0;
  double _fitting = 0;
  double _paint = 0;
  double _bricksPack = 0;
  double _flooring = 0;
  List<_ChartSampleData> data1 = [];
  List<_ChartSampleOneData> data2 = [];

  void _calculation() {
    isDataVisible = true;
    int decimal = 2;
    final num fac = pow(10, decimal);
    final x = double.parse(buildupController.text) *
        double.parse(costController.text);
    final y = double.parse(buildupController.text);
    _first = (x * (21.9 / 100) * fac).round() / fac;
    _second = (x * (18.4 / 100) * fac).round() / fac;
    _third = (x * (11.1 / 100) * fac).round() / fac;
    _forth = (x * (16.9 / 100) * fac).round() / fac;
    _fifth = (x * (17.8 / 100) * fac).round() / fac;
    _sixth = (x * (13.9 / 100) * fac).round() / fac;
    _volume = (x * fac).round() / fac;
    _cement = (0.164 * x * fac).round() / fac;
    _cementBag = (0.4 * y * fac).round() / fac;
    _sand = (0.123 * x * fac).round() / fac;
    _sandBag = (0.816 * y * fac).round() / fac;
    _aggregate = (0.074 * x * fac).round() / fac;
    _aggregateBag = (0.608 * y * fac).round() / fac;
    _steel = (0.246 * x * fac).round() / fac;
    _steelBag = (4 * y * fac).round() / fac;
    _finishers = (16.5 * 0.01 * x * fac).round() / fac;
    _fitting = (0.228 * x * fac).round() / fac;
    _paint = (0.180 * y * fac).round() / fac;
    _bricksPack = (8 * y * fac).round() / fac;
    _flooring = (1.3 * y * fac).round() / fac;
  }


  ScreenshotController screenshotController = ScreenshotController();

  void _captureScreenShort() async {
    screenshotController
        .capture(delay: const Duration(milliseconds: 10))
        .then((capturedImage) async {
      if (capturedImage != null) {
        Uint8List pngint8 = capturedImage.buffer.asUint8List();

        await ImageGallerySaver.saveImage(Uint8List.fromList(pngint8),
            quality: 100, name: 'screenShot-${DateTime.now()}');

        final image =
            await ImagePicker().pickImage(source: (ImageSource.gallery));
        if (image == null) return;
        await Share.shareFiles([image.path]);
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  @override
  Widget build(BuildContext context) {
    buttons().getCurrencyType();
    data2 = [
      _ChartSampleOneData(1, _first, Colors.blue, '$_first'),
      _ChartSampleOneData(2, _second, Colors.red, '$_second'),
      _ChartSampleOneData(3, _third, Colors.yellowAccent, '$_third'),
      _ChartSampleOneData(4, _forth, Colors.cyanAccent, '$_forth'),
      _ChartSampleOneData(5, _fifth, Colors.deepPurple, '$_fifth'),
      _ChartSampleOneData(6, _sixth, Colors.purpleAccent, '$_sixth')
    ];
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        flexibleSpace: AppBarColor().getAppBarColor(),
        title: Row(
          children: [
            Image.asset(
              'assets/icons/ConstructionCost.png',
              width: 35,
              height: 35,
              color: Colors.white,
            ),
            Expanded(
              child: Text(
                ' Construction Cost Calculator',
                style: TextStyle(fontSize: fonts17),
              ),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: ((context) {
                      return HistoryBuildUpCalculationPage();
                    }),
                  ),
                ).then(
                  (value) {
                    setState(() {
                      if (value != null) {
                        isDataVisible = false;
                        buildupController.clear();
                        costController.clear();

                        buildupController =
                            TextEditingController(text: value[0]);
                        costController = TextEditingController(text: value[1]);
                        _calculation();
                      }
                    });
                  },
                );
              },
              child: const Icon(Icons.history),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(7.0),
          child: Screenshot(
            controller: screenshotController,
            child: Column(
              children: [
                Card(
                  elevation: 4,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(7,7,7,3),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                  label: 'Buildup Area (Square Feet)',
                                  controller: buildupController,
                                  errMessage: 'Enter Buildup Area',
                                  formkey: formKey1),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: CustomInput(
                                label: 'Cost Per Square Feet',
                                controller: costController,
                                errMessage: 'Enter Cost in feet',
                                formkey: formKey2,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  setState(() {
                                    if (formKey1.currentState!.validate() &&
                                        formKey2.currentState!.validate()) {
                                      _calculation();

                                      //add data in database
                                      Map<String, dynamic> history = {};
                                      history['BuiltUpArea'] =
                                          buildupController.text;
                                      history['Cost'] = costController.text;
                                      MyDatabase().insertData(
                                          'Construction_Cost', history);
                                    }
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: AppColors.primaryColor,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(5)),
                                  ),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(right: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Calculate',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isDataVisible = false;
                                    buildupController.clear();
                                    costController.clear();
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(5)
                                      ),
                                  ),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(left: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Reset',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                if (isDataVisible) getAmount(),
                if (isDataVisible) getVariousCost(),
                if (isDataVisible) getQuantity(),
                if (isDataVisible) getTime(),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: isDataVisible
          ? FloatingActionButton(
              onPressed: () {
                _captureScreenShort();
              },
              backgroundColor: AppColors.primaryColor,
              child: const Icon(Icons.share),
            )
          : Container(),
    );
  }

  //First Field
  Widget getAmount() {
    return Card(
      elevation: 4,
      child: Column(
        children: [
          Container(
            color: AppColors.primaryColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  ("Approximate amount of cost for given construction is"),
                  style: TextStyle(
                      fontSize: fonts17,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  (myFormat.format(_volume)),
                  style:
                      TextStyle(fontSize: fonts20, fontWeight: FontWeight.bold),
                ),
                Text(
                  '  ${buttons.currencyType}',
                  style: const TextStyle(fontSize: 17),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //Second Field
  Widget getVariousCost() {
    return Card(
      elevation: 4,
      child: Column(
        children: [
          Container(
            color: AppColors.primaryColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  ("Approximate cost on various work of materials as per thumb rule"),
                  style: TextStyle(
                      fontSize: fonts17,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          getCostField('Cement (16.4%)',
              equation: _cement, icon: 'assets/icons/cement.png'),
          const Divider(),
          getCostField('Sand (12.3%)',
              equation: _sand, icon: 'assets/icons/sand.png'),
          const Divider(),
          getCostField('Aggregate (7.4%)',
              equation: _aggregate, icon: 'assets/icons/aggregate.png'),
          const Divider(),
          getCostField('Steel (24.6%)',
              equation: _steel, icon: 'assets/icons/steel.png'),
          const Divider(),
          getCostField('Finishers (16.5%)(Paint(4.1%)+Tiles(8.0%)+Bricks(4.4%))',
              equation: _finishers, icon: 'assets/icons/finishers.png'),
          const Divider(),
          getCostField(
              'Fittings (22.8%)(Window (3.0%)+Doors (3.4%)+Plumbing (5.5%)+Electrical (6.8)+Sanitary (4.1%))',
              equation: _fitting,
              icon: 'assets/icons/fittings.png'),
          const Divider(),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: SfCircularChart(
                legend: Legend(
                  isVisible: true,
                  position: LegendPosition.bottom,
                  overflowMode: LegendItemOverflowMode.wrap,
                ),
                annotations: <CircularChartAnnotation>[
                  CircularChartAnnotation(
                    widget: const Text('Construction Cost ',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 13)),
                  )
                ],
                tooltipBehavior: _tooltipBehavior,
                series: <CircularSeries>[
                  DoughnutSeries<_ChartData, String>(
                    dataSource: chartData,
                    radius: '120%',
                    pointColorMapper: (_ChartData data, _) => data.color,
                    xValueMapper: (_ChartData data, _) => data.x,
                    yValueMapper: (_ChartData data, _) => data.y,
                    dataLabelMapper: (_ChartData data, _) => data.x,
                    dataLabelSettings: const DataLabelSettings(
                        isVisible: true, 
                        labelPosition: ChartDataLabelPosition.inside,
                        textStyle: TextStyle(color: Colors.black)),
                    enableTooltip: true,
                  )
                ]),
          )
        ],
      ),
    );
  }

  //Third Field
  Widget getQuantity() {
    return Card(
      elevation: 4,
      child: Column(
        children: [
          Container(
            color: AppColors.primaryColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  ("Quantity of material required for ${buildupController.text} ft²"),
                  style: TextStyle(
                      fontSize: fonts17,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          getQuantityField('Cement',
              quantity: 'Bags',
              equation: _cementBag,
              icon: 'assets/icons/cement.png'),
          const Divider(),
          getQuantityField('Sand',
              quantity: 'Tan',
              equation: _sandBag,
              icon: 'assets/icons/sand.png'),
          const Divider(),
          getQuantityField('Aggregate',
              quantity: 'Tan',
              equation: _aggregateBag,
              icon: 'assets/icons/aggregate.png'),
          const Divider(),
          getQuantityField('Steel',
              quantity: 'Kg',
              equation: _steelBag,
              icon: 'assets/icons/steel.png'),
          const Divider(),
          getQuantityField('Paint',
              quantity: 'Liters',
              equation: _paint,
              icon: 'assets/icons/paint.png'),
          const Divider(),
          getQuantityField('Bricks',
              quantity: 'Pcs',
              equation: _bricksPack,
              icon: 'assets/icons/finishers.png'),
          const Divider(),
          getQuantityField('Flooring',
              quantity: 'Ft²',
              equation: _flooring,
              icon: 'assets/icons/floor.png'),
        ],
      ),
    );
  }

  //Fourth Field
  Widget getTime() {
    return Card(
      elevation: 4,
      child: Column(
        children: [
          Container(
            color: AppColors.primaryColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  ("To complete this construction in 6 months timeline money required is as below"),
                  style: TextStyle(
                      fontSize: fonts17,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          getFourthField('1',
              position: 'st',
              month: 'Month(21.9%)',
              equation: _first,
              color: Colors.blue),
          const Divider(),
          getFourthField('2',
              position: 'nd',
              month: 'Month(18.4%)',
              equation: _second,
              color: Colors.red),
          const Divider(),
          getFourthField('3',
              position: 'rd',
              month: 'Month(11.1%)',
              equation: _third,
              color: Colors.yellowAccent),
          const Divider(),
          getFourthField('4',
              position: 'th',
              month: 'Month(16.9%)',
              equation: _forth,
              color: Colors.cyanAccent),
          const Divider(),
          getFourthField('5',
              position: 'th',
              month: 'Month(17.8%)',
              equation: _fifth,
              color: Colors.deepPurple),
          const Divider(),
          getFourthField('6',
              position: 'th',
              month: 'Month(13.9%)',
              equation: _sixth,
              color: Colors.purpleAccent),
          SfCartesianChart(
            primaryXAxis: NumericAxis(
              minimum: 0,
              maximum: 7,
              interval: 1,
              enableAutoIntervalOnZooming: false,
              opposedPosition: true,
            ),
            zoomPanBehavior: ZoomPanBehavior(
              enableDoubleTapZooming: true,
              enableMouseWheelZooming: true,
              enablePinching: true,
              enableSelectionZooming: true,
              maximumZoomLevel: 3,
              selectionRectBorderColor: Colors.red,
              selectionRectBorderWidth: 1,
              selectionRectColor: Colors.white,
            ),
            legend: Legend(
              isVisible: true,
              position: LegendPosition.bottom,
              isResponsive: true,
            ),
            primaryYAxis: NumericAxis(
                // minimum: 0,
                // maximum: 40,
                // interval: 10,
                //  anchorRangeToVisiblePoints: true,
                ),
            enableAxisAnimation: true,
            series: <ChartSeries>[
              ColumnSeries<_ChartSampleOneData, double>(
                dataLabelMapper: (_ChartSampleOneData data, _) => data.z,
                animationDelay: 2500,
                borderColor: Colors.black,
                isVisible: true,
                isVisibleInLegend: true,
                dataLabelSettings: const DataLabelSettings(
                  isVisible: true,
                ),
                dataSource: data2,
                xValueMapper: (_ChartSampleOneData data, _) => data.x,
                yValueMapper: (_ChartSampleOneData data, _) => data.y,
                pointColorMapper: (_ChartSampleOneData data, _) => data.color,
                name: 'Rs',
                selectionBehavior: SelectionBehavior(
                    enable: true,
                    selectedBorderColor: Colors.white,
                    selectedColor: Colors.white,
                    toggleSelection: true),
                color: const Color.fromRGBO(8, 142, 255, 1),
                xAxisName: 'primary x-Axis',
                yAxisName: 'primary y-Axis',
              ),
              LineSeries<_ChartSampleOneData, double>(
                isVisibleInLegend: false,
                animationDelay: 5000,
                selectionBehavior: SelectionBehavior(
                  enable: true,
                  selectedColor: Colors.white,
                ),
                dataLabelMapper: (_ChartSampleOneData data, _) => data.z,
                dataSource: data2,
                pointColorMapper: (_ChartSampleOneData data, _) => data.color,
                xValueMapper: (_ChartSampleOneData data, _) => data.x,
                yValueMapper: (_ChartSampleOneData data, _) => data.y,
                name: 'secondary y-Axis',
                yAxisName: 'secondary y-Axis',
              ),
            ],
            axes: [
              NumericAxis(
                name: 'secondary y-Axis',
                opposedPosition: true,
                minimum: 0,
                maximum: 40,
                isVisible: true,
              ),
            ],
          ),
        ],
      ),
    );
  }

  //Fourth Data print
  Widget getFourthField(num,
      {equation, position = '', month = '', color = Colors.black}) {
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 8),
          child: Icon(
            Icons.circle_rounded,
            size: 15,
            color: color,
          ),
        ),
        Expanded(
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  num,
                  style:
                      TextStyle(fontWeight: FontWeight.w900, fontSize: fonts15),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12),
                child: Text(
                  position,
                  style: TextStyle(
                    fontSize: fonts13,
                    fontFeatures: const <FontFeature>[
                      FontFeature.superscripts(),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Text(
                    month,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fonts15),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  myFormat.format(equation),
                  textAlign: TextAlign.end,
                  style:
                      TextStyle(fontSize: fonts15, fontWeight: FontWeight.w900),
                ),
                Text(
                  '  ${buttons.currencyType}',
                  style: TextStyle(fontSize: fonts13),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  //Main Field
  Widget getCostField(material, {quantity = '', equation, icon = ''}) {
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 8),
          child: Image.asset(
            icon,
            height: 25,
            width: 25,
          ),
        ),
        Expanded(
          flex: 6,
          child: Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Text(
              (material),
              style: TextStyle(fontSize: fonts15, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            alignment: Alignment.centerRight,
            child: Text(myFormat.format(equation),
              style:
                  TextStyle(fontSize: fonts15, fontWeight: FontWeight.w900),
            ),
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(right: 8,left: 7),
            child: Text(
              '${buttons.currencyType}',
              style: TextStyle(fontSize: fonts13),
            ),
          ),
        ),
      ],
    );
  }
  Widget getQuantityField(material, {quantity = '', equation, icon = ''}) {
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 8),
          child: Image.asset(
            icon,
            height: 25,
            width: 25,
          ),
        ),
        Expanded(
          flex: 6,
          child: Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Text(
              (material),
              style: TextStyle(fontSize: fonts15, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Expanded(
          flex: 5,
          child: Container(
            alignment: Alignment.centerRight,
            child: Text(myFormat.format(equation),
              style:
              TextStyle(fontSize: fonts15, fontWeight: FontWeight.w900),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                quantity,
                style: TextStyle(fontSize: fonts13),
              ),
            ),
          ),
        ),
      ],
    );
  }
}




class _ChartSampleOneData {
  _ChartSampleOneData(this.x, this.y, this.color, this.z);

  final Color color;
  final String z;
  final double x;
  final double y;
}

class _ChartData {
  _ChartData(this.x, this.y, this.color);

  final String x;
  final double y;
  final Color color;
}

class _ChartSampleData {
  _ChartSampleData(this.x, this.y, this.color, this.z);

  final Color color;
  final String z;
  final double x;
  final double y;
}
