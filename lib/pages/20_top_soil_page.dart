import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/20_h_top_soil_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class TopSoilPage extends StatefulWidget {
  const TopSoilPage({super.key});

  @override
  State<TopSoilPage> createState() => _TopSoilPageState();
}

class _TopSoilPageState extends State<TopSoilPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var soilLength = TextEditingController();
  var soilLength2 = TextEditingController();
  var soilWidth = TextEditingController();
  var soilWidth2 = TextEditingController();
  var soilDepth = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();

  late double finalVolumeMeter = 0;
  late double finalVolumeFeet = 0;

  String? length2Label;
  String? lengthLabel;
  String? width2Label;
  String? widthLabel;
  String? depthLabel;

  String dropdownValue = 'cm';

  bool isChangeValue = true;

  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    String length2Label =
        buttons.isButtonClick2! ? 'Length (cm)' : 'Length (inch)';
    String lengthLabel =
        buttons.isButtonClick1! ? 'Length (feet)' : 'Length (meter)';
    String width2Label =
        buttons.isButtonClick2! ? 'Width (cm)' : 'Width (inch)';
    String widthLabel =
        buttons.isButtonClick1! ? 'Width (feet)' : 'Width (meter)';
    String depthLabel =
        buttons.isButtonClick1! ? 'Depth (feet)' : 'Depth (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/topSoil.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Top Soil Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryTopSoilPage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          soilLength.clear();
                          soilLength2.clear();
                          soilWidth.clear();
                          soilWidth2.clear();
                          soilDepth.clear();
                          dropdownValue = 'cm';

                          soilLength = TextEditingController(text: value[0]);
                          soilLength2 = TextEditingController(text: value[1]);
                          soilWidth = TextEditingController(text: value[2]);
                          soilWidth2 = TextEditingController(text: value[3]);
                          soilDepth = TextEditingController(text: value[4]);
                          dropdownValue = value[5];
                          buttons.isButtonClick1 =
                              value[6] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[6] == '1' ? false : true;
                          _calculateVolume();
                        }
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: lengthLabel,
                                      controller: soilLength,
                                      errMessage: 'Enter $lengthLabel',
                                      formkey: formKey),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: length2Label,
                                      controller: soilLength2,
                                      errMessage: 'Enter $length2Label',
                                      formkey: formKey1,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: widthLabel,
                                      controller: soilWidth,
                                      errMessage: 'Enter $widthLabel',
                                      formkey: formKey2),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: width2Label,
                                      controller: soilWidth2,
                                      errMessage: 'Enter $width2Label',
                                      formkey: formKey3,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: depthLabel,
                                      controller: soilDepth,
                                      errMessage: 'Enter $depthLabel',
                                      formkey: formKey4),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: InputDecorator(
                                    decoration: const InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Inch/CM'),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton2(
                                        value: dropdownValue,
                                        isExpanded: true,
                                        isDense: true,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            dropdownValue = newValue!;
                                          });
                                        },
                                        items: <String>['cm', 'inch']
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style: const TextStyle(
                                                fontSize: 15,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          );
                                        }).toList(),
                                        buttonHeight: 15,
                                        itemHeight: 40,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 2.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      setState(() {
                                        if (formKey.currentState!.validate() &&
                                            formKey2.currentState!.validate() &&
                                            formKey1.currentState!.validate() &&
                                            formKey3.currentState!.validate() &&
                                            formKey4.currentState!.validate()) {
                                          _calculateVolume();

                                          //add data in database
                                          Map<String, dynamic> history = {};
                                          history['Length'] = soilLength.text;
                                          history['Length2'] =
                                              soilLength2.text == ''
                                                  ? 0
                                                  : soilLength2.text;
                                          history['Width'] = soilWidth.text;
                                          history['Width2'] =
                                              soilWidth2.text == ''
                                                  ? 0
                                                  : soilWidth2.text;
                                          history['Depth'] = soilDepth.text;
                                          history['InchCm'] = dropdownValue;
                                          history['Unit'] =
                                              buttons.isButtonClick1;
                                          MyDatabase()
                                              .insertData('Top_Soil', history);
                                        }
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(5)),
                                      ),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(right: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Calculate',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        isDataVisible = false;
                                        soilLength.clear();
                                        soilLength2.clear();
                                        soilWidth.clear();
                                        soilWidth2.clear();
                                        soilDepth.clear();
                                      });
                                    },
                                    child: Container(
                                      decoration: const BoxDecoration(
                                          color: AppColors.primaryColor,
                                          borderRadius: BorderRadius.only(
                                              bottomRight: Radius.circular(5))),
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.only(left: 2.5),
                                      padding: const EdgeInsets.all(8),
                                      child: Text(
                                        'Reset',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fonts15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getTopSoilVolume(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    soilLength2.text.isEmpty
        ? soilLength2 = TextEditingController(text: '0')
        : soilLength2;
    soilWidth2.text.isEmpty
        ? soilWidth2 = TextEditingController(text: '0')
        : soilWidth2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    double length = double.parse('${soilLength.text}.${soilLength2.text}');
    double width = double.parse('${soilWidth.text}.${soilWidth2.text}');
    double depth = double.parse(soilDepth.text);
    if (dropdownValue == 'cm') {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalVolumeFeet =
              ((length * width * (depth * 0.00093) * fac)).round() / fac;
          finalVolumeMeter =
              (length * width * (depth * 0.00093) * 35.3147 * fac).round() /
                  fac;
        });
      } else {
        setState(() {
          finalVolumeFeet =
              (((length * width * (depth * 0.01)) * fac)).round() / fac;
          finalVolumeMeter =
              (((length * width * (depth * 0.01)) * 35.3147) * fac).round() /
                  fac;
        });
      }
    } else {
      if (buttons.isButtonClick1!) {
        setState(() {
          finalVolumeFeet =
              ((length * width * (depth * 0.00236) * fac)).round() / fac;
          finalVolumeMeter =
              (length * width * (depth * 0.00236) * 35.3147 * fac).round() /
                  fac;
        });
      } else {
        setState(() {
          finalVolumeFeet =
              ((length * width * (depth * 0.0254) * fac)).round() / fac;
          finalVolumeMeter =
              ((length * width * (depth * 0.0254)) * 35.3147 * fac).round() /
                  fac;
        });
      }
    }
    soilLength2.text != '0' ? soilLength2 = soilLength2 : soilLength2.clear();
    soilWidth2.text != '0' ? soilWidth2 = soilWidth2 : soilWidth2.clear();
  }

  Widget getTopSoilVolume() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Card(
        elevation: 4,
        child: Column(children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 2),
              child: Text('Total Volume of Top Soil',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts17,
                      color: Colors.black)),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  '$finalVolumeFeet m',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts17,
                      color: Colors.red),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                  '3',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: fonts13,
                    color: Colors.red,
                    fontFeatures: const <FontFeature>[
                      FontFeature.superscripts(),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  ' or ',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts17,
                      color: Colors.black),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  '$finalVolumeMeter ft',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: fonts17,
                      color: Colors.red),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 13),
                child: Text(
                  '3',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: fonts13,
                    color: Colors.red,
                    fontFeatures: const <FontFeature>[
                      FontFeature.superscripts(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
