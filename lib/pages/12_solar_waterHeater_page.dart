import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/12_h_solar_waterHeater_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';

class SolarWaterHeaterPage extends StatefulWidget {
  const SolarWaterHeaterPage({super.key});

  @override
  State<SolarWaterHeaterPage> createState() => _SolarWaterHeaterPageState();
}

class _SolarWaterHeaterPageState extends State<SolarWaterHeaterPage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var noOfPerson = TextEditingController();

  String dropdownValue = 'Yes';
  late double _capacityOfHeater = 0;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: Row(
          children: [
            Image.asset(
              'assets/icons/solarWaterHeater.png',
              width: 35,
              height: 35,
              color: Colors.white,
            ),
            Expanded(
              child: Text(
                ' Solar Water Heater Calculator',
                style: TextStyle(fontSize: fonts17),
              ),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: ((context) {
                      return HistorySolarWaterHeaterPage();
                    }),
                  ),
                ).then(
                  (value) {
                    if (value != null) {
                      isDataVisible = false;
                      noOfPerson.clear();
                      dropdownValue = 'Yes';

                      noOfPerson = TextEditingController(text: value[0]);
                      dropdownValue = value[1];
                      _calculateVolume();
                    }
                  },
                );
              },
              child: const Icon(Icons.history),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Screenshot(
          controller: screenshotController,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 4,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                  label: 'No of Persons in Family',
                                  controller: noOfPerson,
                                  errMessage: 'Enter No of Persons in Family',
                                  formkey: formKey),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: InputDecorator(
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Heater with Pressure Pump'),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton2(
                              value: dropdownValue,
                              isExpanded: true,
                              isDense: true,
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 15),
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownValue = newValue!;
                                });
                              },
                              items: <String>[
                                'Yes',
                                'No',
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              buttonHeight: 14,
                              itemHeight: 40,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  setState(() {
                                    if (formKey.currentState!.validate()) {
                                      _calculateVolume();

                                      //add data in database
                                      Map<String, dynamic> history = {};
                                      history['NoOfPerson'] = noOfPerson.text;
                                      history['IsHeater'] = dropdownValue;
                                      MyDatabase().insertData(
                                          'Solar_WaterHeater', history);
                                    }
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: AppColors.primaryColor,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(5)),
                                  ),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(right: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Calculate',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isDataVisible = false;
                                    noOfPerson.clear();
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(5))),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(left: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Reset',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (isDataVisible) getHeaterCapacity(),
            ],
          ),
        ),
      ),
      floatingActionButton: isDataVisible
          ? FloatingActionButton(
              onPressed: () {
                ScreenShort().captureScreenShort(screenshotController);
              },
              backgroundColor: AppColors.primaryColor,
              child: const Icon(Icons.share),
            )
          : Container(),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    if (dropdownValue == 'Yes') {
      setState(() {
        _capacityOfHeater =
            (double.parse(noOfPerson.text) * 80).round().toDouble();
      });
    } else {
      setState(() {
        _capacityOfHeater =
            (double.parse(noOfPerson.text) * 50).round().toDouble();
      });
    }
  }

  Widget getHeaterCapacity() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 5),
                child: const Text('Capacity of Solar Water Heater',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  '$_capacityOfHeater Liters',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: fonts17,
                      color: Colors.red),
                ),
              ),
            ),
            const Center(
              child: Padding(
                padding: EdgeInsets.only(left: 15, right: 5, bottom: 10),
                child: Text(
                    'Note: The thumb rule in deciding capacity is that a person requires 30-50 litres of water per day',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black54)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
