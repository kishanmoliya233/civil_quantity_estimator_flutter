import 'dart:math';
import 'dart:ui';

import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/22_h_concrete_pipe_page.dart';
import 'package:civil_quantity_estimator/home_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ConcretePipePage extends StatefulWidget {
  const ConcretePipePage({super.key});

  @override
  State<ConcretePipePage> createState() => _ConcretePipePageState();
}

class _ConcretePipePageState extends State<ConcretePipePage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  String dropdownValue1 = 'M20 (1:1.5:3)';

  var innerDiameter = TextEditingController();
  var innerDiameter2 = TextEditingController();
  var outerDiameter = TextEditingController();
  var outerDiameter2 = TextEditingController();
  var height = TextEditingController();
  var height2 = TextEditingController();
  var noOfPipes = TextEditingController(text: '1');
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();

  late double finalConcreteAreaFeet = 0;
  late double finalConcreteAreaMeter = 0;
  late double _cementBag = 0;
  late double _sandTon = 0;
  late double _aggTon = 0;

  String? innerDiameter2Label;
  String? innerDiameterLabel;
  String? outerDiameter2Label;
  String? outerDiameterLabel;
  String? height2Label;
  String? heightLabel;

  bool isChangeValue = true;
  List<ChartData> chartData = [];
  @override
  Widget build(BuildContext context) {
    isChangeValue ? buttons().getData() : Container();
    isChangeValue = false;
    chartData = [
      ChartData('Cement\n$_cementBag Bags', _cementBag * 50, Colors.blue),
      ChartData('Sand\n$_sandTon Ton', _sandTon * 1000, Colors.red),
      ChartData(
          'Aggregate\n$_aggTon Ton', _aggTon * 1000, Colors.yellow),
    ];
    String innerDiameter2Label = buttons.isButtonClick2!
        ? 'Inner Diameter (cm)'
        : 'Inner Diameter (inch)';
    String innerDiameterLabel = buttons.isButtonClick1!
        ? 'Inner Diameter (feet)'
        : 'Inner Diameter (meter)';
    String outerDiameter2Label = buttons.isButtonClick2!
        ? 'Outer Diameter (cm)'
        : 'Outer Diameter (inch)';
    String outerDiameterLabel = buttons.isButtonClick1!
        ? 'Outer Diameter (feet)'
        : 'Outer Diameter (meter)';
    String height2Label =
        buttons.isButtonClick2! ? 'Height (cm)' : 'Height (inch)';
    String heightLabel =
        buttons.isButtonClick1! ? 'Height (feet)' : 'Height (meter)';
    return WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: ((context) {
                return const Home_page();
              }),
            ),
          );
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: AppBarColor().getAppBarColor(),
            titleSpacing: 0,
            title: Row(
              children: [
                Image.asset(
                  'assets/icons/concretePipe.png',
                  width: 35,
                  height: 35,
                  color: Colors.white,
                ),
                Expanded(
                  child: Text(
                    ' Concrete Pipe Calculator',
                    style: TextStyle(fontSize: fonts17),
                  ),
                ),
              ],
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15))),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: ((context) {
                          return HistoryConcretePipePage();
                        }),
                      ),
                    ).then(
                      (value) {
                        if (value != null) {
                          isDataVisible = false;
                          innerDiameter.clear();
                          innerDiameter2.clear();
                          outerDiameter.clear();
                          outerDiameter2.clear();
                          height.clear();
                          height2.clear();
                          noOfPipes = TextEditingController(text: '1');
                          dropdownValue1 = 'M20 (1:1.5:3)';

                          dropdownValue1 = value[0];
                          innerDiameter = TextEditingController(text: value[1]);
                          innerDiameter2 =
                              TextEditingController(text: value[2]);
                          outerDiameter = TextEditingController(text: value[3]);
                          outerDiameter2 =
                              TextEditingController(text: value[4]);
                          height = TextEditingController(text: value[5]);
                          height2 = TextEditingController(text: value[6]);
                          noOfPipes = TextEditingController(text: value[7]);
                          buttons.isButtonClick1 =
                              value[8] == '1' ? true : false;
                          buttons.isButtonClick2 =
                              value[8] == '1' ? false : true;
                          _calculateVolume();
                        }
                        setState(() {});
                      },
                    );
                  },
                  child: const Icon(Icons.history),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Screenshot(
              controller: screenshotController,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Form(child: getVolumeType1()),
                        ),
                        Expanded(
                          child: Form(
                            child: getVolumeType2(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 5),
                    child: Card(
                      elevation: 4,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InputDecorator(
                              decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Grade of Concrete'),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  value: dropdownValue1,
                                  isExpanded: true,
                                  isDense: true,
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 15),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValue1 = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    'M20 (1:1.5:3)',
                                    'M15 (1:2:4)',
                                    'M10 (1:3:6)',
                                    'M7.5 (1:4:8)'
                                  ].map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  buttonHeight: 17,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: innerDiameterLabel,
                                      controller: innerDiameter,
                                      errMessage: 'Enter $innerDiameterLabel',
                                      formkey: formKey),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: innerDiameter2Label,
                                      controller: innerDiameter2,
                                      errMessage: 'Enter $innerDiameter2Label',
                                      formkey: formKey1,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: outerDiameterLabel,
                                      controller: outerDiameter,
                                      errMessage: 'Enter $outerDiameterLabel',
                                      formkey: formKey2),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: outerDiameter2Label,
                                      controller: outerDiameter2,
                                      errMessage: 'Enter $outerDiameter2Label',
                                      formkey: formKey3,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                      label: heightLabel,
                                      controller: height,
                                      errMessage: 'Enter $heightLabel',
                                      formkey: formKey4),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomInput(
                                      label: height2Label,
                                      controller: height2,
                                      errMessage: 'Enter $height2Label',
                                      formkey: formKey5,
                                      validate: true),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                            child: Row(
                              children: [
                                Expanded(
                                  child: CustomInput(
                                    label: 'No. of Pipes',
                                    controller: noOfPipes,
                                    errMessage: 'Enter Number of Pipes',
                                    formkey: formKey6,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                    setState(() {
                                      if (formKey1.currentState!.validate() &&
                                          formKey3.currentState!.validate() &&
                                          formKey2.currentState!.validate() &&
                                          formKey4.currentState!.validate() &&
                                          formKey6.currentState!.validate() &&
                                          formKey5.currentState!.validate()) {
                                        _calculateVolume();

                                        //add data in database
                                        Map<String, dynamic> history = {};
                                        history['GradeOfConcrete'] =
                                            dropdownValue1;
                                        history['InnerDiameter'] =
                                            innerDiameter.text;
                                        history['InnerDiameter2'] =
                                            innerDiameter2.text == ''
                                                ? 0
                                                : innerDiameter2.text;
                                        history['OuterDiameter'] =
                                            outerDiameter.text;
                                        history['OuterDiameter2'] =
                                            outerDiameter2.text == ''
                                                ? 0
                                                : outerDiameter2.text;
                                        history['Height'] = height.text;
                                        history['Height2'] = height2.text == ''
                                            ? 0
                                            : height2.text;
                                        history['NoOfPipes'] = noOfPipes.text;
                                        history['Unit'] =
                                            buttons.isButtonClick1;
                                        MyDatabase().insertData(
                                            'Concrete_Pipe', history);
                                      }
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(5)),
                                    ),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(right: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Calculate',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      isDataVisible = false;
                                      innerDiameter.clear();
                                      innerDiameter2.clear();
                                      outerDiameter.clear();
                                      outerDiameter2.clear();
                                      height.clear();
                                      height2.clear();
                                      noOfPipes.clear();
                                    });
                                  },
                                  child: Container(
                                    decoration: const BoxDecoration(
                                        color: AppColors.primaryColor,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(5))),
                                    alignment: Alignment.center,
                                    margin: const EdgeInsets.only(left: 2.5),
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      'Reset',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fonts15),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (isDataVisible) getAreaOfPipes(),
                ],
              ),
            ),
          ),
          floatingActionButton: isDataVisible
              ? FloatingActionButton(
                  onPressed: () {
                    ScreenShort().captureScreenShort(screenshotController);
                  },
                  backgroundColor: AppColors.primaryColor,
                  child: const Icon(Icons.share),
                )
              : Container(),
        ));
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    innerDiameter2.text.isEmpty
        ? innerDiameter2 = TextEditingController(text: '0')
        : innerDiameter2;
    outerDiameter2.text.isEmpty
        ? outerDiameter2 = TextEditingController(text: '0')
        : outerDiameter2;
    height2.text.isEmpty ? height2 = TextEditingController(text: '0') : height2;
    int decimals = 2;
    final num fac = pow(10, decimals);
    double? cement1;
    double? cement2;
    double? sand1;
    double? sand2;
    double? aggregate1;
    double? aggregate2;
    if (dropdownValue1 == 'M20 (1:1.5:3)') {
      cement1 = 1;
      cement2 = 5.5;
      sand1 = 1.5;
      sand2 = 5.5;
      aggregate1 = 3;
      aggregate2 = 5.5;
    } else if (dropdownValue1 == 'M15 (1:2:4)') {
      cement1 = 1;
      cement2 = 7;
      sand1 = 1.5;
      sand2 = 7;
      aggregate1 = 3;
      aggregate2 = 7;
    } else if (dropdownValue1 == 'M10 (1:3:6)') {
      cement1 = 1;
      cement2 = 10;
      sand1 = 3;
      sand2 = 10;
      aggregate1 = 6;
      aggregate2 = 10;
    } else {
      cement1 = 1;
      cement2 = 13;
      sand1 = 4;
      sand2 = 13;
      aggregate1 = 8;
      aggregate2 = 13;
    }
    double innerArea = ((pi *
        (double.parse('${innerDiameter.text}.${(innerDiameter2.text)}')) /
        2 *
        (double.parse('${(innerDiameter.text)}.${(innerDiameter2.text)}')) /
        2));
    double outerArea = ((pi *
        (double.parse('${(outerDiameter.text)}.${(outerDiameter2.text)}')) /
        2 *
        (double.parse('${(outerDiameter.text)}.${(outerDiameter2.text)}')) /
        2));
    double area = outerArea - innerArea;
    double volumeft = buttons.isButtonClick1!
        ? (area *
                    (double.parse('${(height.text)}.${(height2.text)}')) *
                    (double.parse(noOfPipes.text)) *
                    fac)
                .round() /
            fac
        : ((area *
                        (double.parse('${(height.text)}.${(height2.text)}')) *
                        (double.parse(noOfPipes.text))) *
                    35.3147 *
                    fac)
                .round() /
            fac;
    double volumem = buttons.isButtonClick1!
        ? ((volumeft / 35.3147 * fac).round() / fac)
        : ((volumeft / 35.3147) * fac).round() / fac;
    double dryVolume = volumem * 1.524 ;
    double cement = (((dryVolume * cement1) / cement2) / 0.035).ceilToDouble();
    double sand =
        (((((dryVolume * sand1) / sand2) * 1550) / 1000) * 100).round() /
    100;
    double aggregate =
        (((((dryVolume * aggregate1) / aggregate2) * 1350) / 1000) * 100).round() /
            100;
    finalConcreteAreaMeter = volumem;
    finalConcreteAreaFeet = volumeft;
    _cementBag = cement;
    _sandTon = sand;
    _aggTon = aggregate;
    innerDiameter2.text != '0'
        ? innerDiameter2 = innerDiameter2
        : innerDiameter2.clear();
    outerDiameter2.text != '0'
        ? outerDiameter2 = outerDiameter2
        : outerDiameter2.clear();
    height2.text != '0' ? height2 = height2 : height2.clear();
  }

  Widget getAreaOfPipes() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 2),
                child: Text('Total Area of Concrete Pipes',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.black)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0, top: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '$finalConcreteAreaMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: Text(
                      '3',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: fonts13,
                        color: Colors.red,
                        fontFeatures: const <FontFeature>[
                          FontFeature.superscripts(),
                        ],
                      ),
                    ),
                  ),
                  Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                  Text(
                    '$finalConcreteAreaFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 7.0),
                    child: Text(
                      '3',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: fonts13,
                        color: Colors.red,
                        fontFeatures: const <FontFeature>[
                          FontFeature.superscripts(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    color: AppColors.primaryColor,
                    padding: const EdgeInsets.only(right: 70, top: 10, bottom: 10),
                    child: const Text('Material',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.white)),
                  ),
                ),
                Expanded(
                  child: Container(
                    color: AppColors.primaryColor,
                    padding: const EdgeInsets.only(left: 70, top: 10, bottom: 10),
                    child: const Text('Quantity',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.white)),
                  ),
                )
              ],
            ),
            GetMaterial(
                'assets/icons/cement.png', 'Cement', '$_cementBag', 'Bags',
                isDivider: true),
            GetMaterial('assets/icons/sand.png', 'Sand', '$_sandTon', 'Ton',
                isDivider: true),
            GetMaterial(
                'assets/icons/aggregate.png', 'Aggregate', '$_aggTon', 'Ton',
                isDivider: false),
            _cementBag != 0 ? SfCircularChart(
              margin: EdgeInsets.zero,
              series: <CircularSeries>[
                PieSeries<ChartData, String>(
                  dataSource: chartData,
                  radius: '80',
                  pointColorMapper: (ChartData data, _) => data.color,
                  xValueMapper: (ChartData data, _) => data.x,
                  yValueMapper: (ChartData data, _) => data.y,
                  dataLabelMapper: (ChartData data, _) => data.x,
                  dataLabelSettings: const DataLabelSettings(
                    isVisible: true,
                    labelPosition: ChartDataLabelPosition.outside,
                  ),
                  enableTooltip: true,
                )
              ],
            ) : Container(),
          ],
        ),
      ),
    );
  }
}
