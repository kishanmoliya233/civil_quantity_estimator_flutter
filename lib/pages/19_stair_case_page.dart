import 'dart:math';
import 'dart:ui';
import 'package:civil_quantity_estimator/DataBase/my_database.dart';
import 'package:civil_quantity_estimator/dataBase_Pages/19_h_stair_case_page.dart';
import 'package:civil_quantity_estimator/utility/colorsFonts.dart';
import 'package:civil_quantity_estimator/widgets/customInput.dart';
import 'package:civil_quantity_estimator/widgets/material.dart';
import 'package:civil_quantity_estimator/widgets/screenShort.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StairCasePage extends StatefulWidget {
  const StairCasePage({super.key});

  @override
  State<StairCasePage> createState() => _StairCasePageState();
}

class _StairCasePageState extends State<StairCasePage> {
  bool isDataVisible = false;
  ScreenshotController screenshotController = ScreenshotController();
  double fonts13 = AppFonts.font13;
  double fonts15 = AppFonts.font15;
  double fonts17 = AppFonts.font17;
  double fonts20 = AppFonts.font20;

  var stairRiser = TextEditingController(text: '0');
  var stairRiser2 = TextEditingController(text: '6');
  var stairTread = TextEditingController(text: '1');
  var stairTread2 = TextEditingController(text: '0');
  var stairWidth = TextEditingController();
  var stairHeight = TextEditingController();
  var stairThickness = TextEditingController(text: '4');

  final GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey3 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey4 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey5 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey6 = GlobalKey<FormState>();
  final GlobalKey<FormState> formKey7 = GlobalKey<FormState>();

  String dropdownValue = 'M20 (1:1.5:3)';

  late double stairCaseVolumeFeet = 0;
  late double stairCaseVolumeMeter = 0;
  late double _cementBag = 0;
  late double _sandTon = 0;
  late double _aggTon = 0;
  List<ChartData> chartData = [];
  @override
  Widget build(BuildContext context) {
    chartData = [
      ChartData('Cement\n$_cementBag Bags', _cementBag * 50, Colors.blue),
      ChartData('Sand\n$_sandTon Ton', _sandTon * 1000, Colors.red),
      ChartData(
          'Aggregate\n$_aggTon Ton', _aggTon * 1000, Colors.yellow),
    ];
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: AppBarColor().getAppBarColor(),
        titleSpacing: 0,
        title: Row(
          children: [
            Image.asset(
              'assets/icons/stairCase.png',
              width: 35,
              height: 35,
              color: Colors.white,
            ),
            Expanded(
              child: Text(
                ' Stair Case Calculator',
                style: TextStyle(fontSize: fonts17),
              ),
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: ((context) {
                      return HistoryStairCasePage();
                    }),
                  ),
                ).then(
                  (value) {
                    if (value != null) {
                      isDataVisible = false;
                      stairRiser = TextEditingController(text: '0');
                      stairRiser2 = TextEditingController(text: '6');
                      stairTread = TextEditingController(text: '1');
                      stairTread2 = TextEditingController(text: '0');
                      stairWidth.clear();
                      stairHeight.clear();
                      stairThickness = TextEditingController(text: '4');
                      dropdownValue = 'M20 (1:1.5:3)';

                      stairRiser = TextEditingController(text: value[0]);
                      stairRiser2 = TextEditingController(text: value[1]);
                      stairTread = TextEditingController(text: value[2]);
                      stairTread2 = TextEditingController(text: value[3]);
                      stairWidth = TextEditingController(text: value[4]);
                      stairHeight = TextEditingController(text: value[5]);
                      stairThickness = TextEditingController(text: value[6]);
                      dropdownValue = value[7];
                      _calculateVolume();
                    }
                  },
                );
              },
              child: const Icon(Icons.history),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Screenshot(
          controller: screenshotController,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 4,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                  label: 'Riser (feet)',
                                  controller: stairRiser,
                                  errMessage: 'Enter Riser in feet ',
                                  formkey: formKey1),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: CustomInput(
                                  label: 'Riser (inch)',
                                  controller: stairRiser2,
                                  errMessage: 'Enter Riser in inch',
                                  formkey: formKey2,validate: true,),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                  label: 'Tread (feet)',
                                  controller: stairTread,
                                  errMessage: 'Enter Tread in feet',
                                  formkey: formKey3),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: CustomInput(
                                  label: 'Tread (inch)',
                                  controller: stairTread2,
                                  errMessage: 'Enter Tread in inch',
                                  formkey: formKey4,validate: true,),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                  label: 'Width of Stair (feet)',
                                  controller: stairWidth,
                                  errMessage: 'Enter Width of Stair',
                                  formkey: formKey5),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                  label: 'Height of Stair (feet)',
                                  controller: stairHeight,
                                  errMessage: 'Enter Height of Stair',
                                  formkey: formKey6),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: Row(
                          children: [
                            Expanded(
                              child: CustomInput(
                                  label: 'Thickness of Waist Slab (inch)',
                                  controller: stairThickness,
                                  errMessage: 'Enter Thickness of Waist Slab',
                                  formkey: formKey7,
                              validate: true,),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                        child: InputDecorator(
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Grade of Concrete'),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton2(
                              value: dropdownValue,
                              isExpanded: true,
                              isDense: true,
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 15),
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownValue = newValue!;
                                });
                              },
                              items: <String>[
                                'M20 (1:1.5:3)',
                                'M15 (1:2:4)',
                                'M10 (1:3:6)',
                                'M7.5 (1:4:8)'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              buttonHeight: 17,
                              itemHeight: 40,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  FocusManager.instance.primaryFocus?.unfocus();
                                  setState(() {
                                    if (formKey1.currentState!.validate() &&
                                        formKey2.currentState!.validate() &&
                                        formKey3.currentState!.validate() &&
                                        formKey4.currentState!.validate() &&
                                        formKey5.currentState!.validate() &&
                                        formKey6.currentState!.validate() &&
                                        formKey7.currentState!.validate()) {
                                      _calculateVolume();

                                      //add data in database
                                      Map<String, dynamic> history = {};
                                      history['Riser'] = stairRiser.text;
                                      history['Riser2'] = stairRiser2.text;
                                      history['Tread'] = stairTread.text;
                                      history['Tread2'] = stairTread2.text;
                                      history['StairWidth'] = stairWidth.text;
                                      history['StairHeight'] = stairHeight.text;
                                      history['ThicknessOfSlab'] =
                                          stairHeight.text;
                                      history['GradeOfConcrete'] =
                                          dropdownValue;
                                      MyDatabase()
                                          .insertData('Stair_Case', history);
                                    }
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                    color: AppColors.primaryColor,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(5)),
                                  ),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(right: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Calculate',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isDataVisible = false;
                                    stairRiser.clear();
                                    stairRiser2.clear();
                                    stairTread.clear();
                                    stairTread2.clear();
                                    stairWidth.clear();
                                    stairHeight.clear();
                                    stairThickness.clear();
                                  });
                                },
                                child: Container(
                                  decoration: const BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(5))),
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(left: 2.5),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    'Reset',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: fonts15),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (isDataVisible) getVolumeOfStaircase(),
            ],
          ),
        ),
      ),
      floatingActionButton: isDataVisible
          ? FloatingActionButton(
              onPressed: () {
                ScreenShort().captureScreenShort(screenshotController);
              },
              backgroundColor: AppColors.primaryColor,
              child: const Icon(Icons.share),
            )
          : Container(),
    );
  }

  Widget getVolumeType1() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick1! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick1 = true;
          buttons.isButtonClick2 = false;
        });
      },
      child: Text('Feet / Inch',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.black : Colors.white,
              fontWeight: FontWeight.bold)),
    );
  }

  Widget getVolumeType2() {
    return TextButton(
      style: ElevatedButton.styleFrom(
        backgroundColor:
            buttons.isButtonClick2! ? AppColors.primaryColor : Colors.white,
        shape: const RoundedRectangleBorder(
            side: BorderSide(color: Colors.black12),
            borderRadius: BorderRadius.horizontal(
              right: Radius.circular(5),
            )),
      ),
      onPressed: () {
        setState(() {
          buttons.isButtonClick2 = true;
          buttons.isButtonClick1 = false;
        });
      },
      child: Text('Meter / CM',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: buttons.isButtonClick2! ? Colors.white : Colors.black,
              fontWeight: FontWeight.bold)),
    );
  }

  void _calculateVolume() {
    isDataVisible = true;
    int decimals = 2;
    final num fac = pow(10, decimals);
    late double _wetVolumeFinal =
        ((stairCaseVolumeMeter * 1.524) * fac).round() / fac;
    late double _cementVolume = _wetVolumeFinal * (1 / 5.5);
    late double _sandVolume = _wetVolumeFinal * (1.5 / 5.5);
    late double _aggVolume = _wetVolumeFinal * (3 / 5.5);
    late double _cementVolume1 = _wetVolumeFinal * (1 / 7);
    late double _sandVolume1 = _wetVolumeFinal * (2 / 7);
    late double _aggVolume1 = _wetVolumeFinal * (4 / 7);
    late double _cementVolume2 = _wetVolumeFinal * (1 / 10);
    late double _sandVolume2 = _wetVolumeFinal * (3 / 10);
    late double _aggVolume2 = _wetVolumeFinal * (6 / 10);
    late double _cementVolume3 = _wetVolumeFinal * (1 / 13);
    late double _sandVolume3 = _wetVolumeFinal * (4 / 13);
    late double _aggVolume3 = _wetVolumeFinal * (8 / 13);
    late double _riserFinal = double.parse(stairRiser2.text);
    late double _riserFinally = double.parse(stairRiser.text);
    late double _treadFinally = double.parse(stairTread.text);
    late double _treadFinal = double.parse(stairTread2.text);
    late double _widthOfStair = double.parse(stairWidth.text);
    late double _heightOfStair = double.parse(stairHeight.text);
    late double _thicknessOfWaist = double.parse(stairThickness.text);
    late double _riser =
        ((_riserFinally + (_riserFinal / 12)) * fac).round() / fac;
    late double _tread =
        ((_treadFinally + (_treadFinal / 12)) * fac).round() / fac;
    late double _thickness = ((_thicknessOfWaist / 12) * fac).round() / fac;
    late double _slantHeight = ((sqrt((((_heightOfStair / _riser) / _tread) *
                        ((_heightOfStair / _riser) / _tread)) +
                    (_heightOfStair * _heightOfStair))) *
                fac)
            .round() /
        fac;
    late double _totalVolumeStep =
        (((0.5 * _riser * _tread * _widthOfStair) * (_heightOfStair / _riser)) *
                    fac)
                .round() /
            fac;
    late double _totalVolumeWaist =
        ((_widthOfStair * _thickness * _slantHeight) * fac).round() / fac;
    late double stairCaseVolumeFeet1 =
        ((_totalVolumeWaist + _totalVolumeStep) * fac).round() / fac;
    if (dropdownValue == 'M20 (1:1.5:3)') {
      setState(() {
        stairCaseVolumeMeter =
            (((stairCaseVolumeFeet1) / 35.3147) * fac).round() / fac;
        stairCaseVolumeFeet = ((stairCaseVolumeFeet1) * fac).round() / fac;
        _cementBag = (_cementVolume / 0.035).round().toDouble();
        late double _sandBag = ((_sandVolume * 1550) * fac).round() / fac;
        _sandTon = ((_sandBag / 1000) * fac).round() / fac;
        late double _aggBag = ((_aggVolume * 1350) * fac).round() / fac;
        _aggTon = ((_aggBag / 1000) * fac).round() / fac;
      });
    } else if (dropdownValue == 'M15 (1:2:4)') {
      setState(() {
        stairCaseVolumeMeter =
            (((stairCaseVolumeFeet1) / 35.3147) * fac).round() / fac;
        stairCaseVolumeFeet = ((stairCaseVolumeFeet1) * fac).round() / fac;
        _cementBag = (_cementVolume1 / 0.035).round().toDouble();
        late double _sandBag = ((_sandVolume1 * 1550) * fac).round() / fac;
        _sandTon = ((_sandBag / 1000) * fac).round() / fac;
        late double _aggBag = ((_aggVolume1 * 1350) * fac).round() / fac;
        _aggTon = ((_aggBag / 1000) * fac).round() / fac;
      });
    } else if (dropdownValue == 'M10 (1:3:6)') {
      setState(() {
        stairCaseVolumeMeter =
            (((stairCaseVolumeFeet1) / 35.3147) * fac).round() / fac;
        stairCaseVolumeFeet = ((stairCaseVolumeFeet1) * fac).round() / fac;
        _cementBag = (_cementVolume2 / 0.035).round().toDouble();
        late double _sandBag = ((_sandVolume2 * 1550) * fac).round() / fac;
        _sandTon = ((_sandBag / 1000) * fac).round() / fac;
        late double _aggBag = ((_aggVolume2 * 1350) * fac).round() / fac;
        _aggTon = ((_aggBag / 1000) * fac).round() / fac;
      });
    } else {
      setState(() {
        stairCaseVolumeMeter =
            (((stairCaseVolumeFeet1) / 35.3147) * fac).round() / fac;
        stairCaseVolumeFeet = ((stairCaseVolumeFeet1) * fac).round() / fac;
        _cementBag = (_cementVolume3 / 0.035).ceil().round().toDouble();
        late double _sandBag = ((_sandVolume3 * 1550) * fac).round() / fac;
        _sandTon = ((_sandBag / 1000) * fac).round() / fac;
        late double _aggBag = ((_aggVolume3 * 1350) * fac).round() / fac;
        _aggTon = ((_aggBag / 1000) * fac).round() / fac;
      });
    }
  }

  Widget getVolumeOfStaircase() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 2),
                child: Text('Total Volume of Staircase',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.black)),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$stairCaseVolumeMeter m',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    ' or ',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: fonts17,
                        color: Colors.black),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5),
                  child: Text(
                    '$stairCaseVolumeFeet ft',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: fonts17,
                        color: Colors.red),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 13),
                  child: Text(
                    '3',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: fonts13,
                      color: Colors.red,
                      fontFeatures: const <FontFeature>[
                        FontFeature.superscripts(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(right: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: const Text('Material',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            color: Colors.white)),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    padding: const EdgeInsets.only(left: 70, top: 10),
                    decoration: const BoxDecoration(color: AppColors.primaryColor),
                    child: const Text('Quantity',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            color: Colors.white)),
                  ),
                )
              ],
            ),
            GetMaterial(
                'assets/icons/cement.png', 'Cement', '$_cementBag', 'Bags',
                isDivider: true),
            GetMaterial('assets/icons/sand.png', 'Sand', '$_sandTon', 'Ton',
                isDivider: true),
            GetMaterial(
                'assets/icons/aggregate.png', 'Aggregate', '$_aggTon', 'Ton',
                isDivider: false),
            _cementBag != 0 ? SfCircularChart(
              margin: EdgeInsets.zero,
              series: <CircularSeries>[
                PieSeries<ChartData, String>(
                  dataSource: chartData,
                  radius: '80',
                  pointColorMapper: (ChartData data, _) => data.color,
                  xValueMapper: (ChartData data, _) => data.x,
                  yValueMapper: (ChartData data, _) => data.y,
                  dataLabelMapper: (ChartData data, _) => data.x,
                  dataLabelSettings: const DataLabelSettings(
                    isVisible: true,
                    labelPosition: ChartDataLabelPosition.outside,
                  ),
                  enableTooltip: true,
                )
              ],
            ) : Container(),
          ],
        ),
      ),
    );
  }
}
